﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces.Broadcasters;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Hubs
{
	//[Authorize]
	public class LoadsHub : Hub<ILoadsHub>
	{
		private readonly IMediator _mediator;

		public LoadsHub(IMediator mediator)
		{
			_mediator = mediator;
		}
	}
}
