﻿using AutoMapper;
using FluentValidation.AspNetCore;
using LoadProPlus.Application.Contexts.Loads;
using LoadProPlus.Application.Contexts.Trucks.Commands.CreateTruck;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.Application.Interfaces.Utils;
using LoadProPlus.Application.Utils.Helpers;
using LoadProPlus.Application.Utils.Tokens;
using LoadProPlus.Persistence;
using LoadProPlus.WebAPI.Broadcasters;
using LoadProPlus.WebAPI.Hubs;
using LoadProPlus.WebAPI.Utils.CurrentUser;
using LoadProPlus.WebAPI.Utils.Extensions;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Reflection;
using System.Text;
using System.Threading;

namespace LoadProPlus.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add DbContext
            services.AddDbContext<ILoadProPlusDbContext, LoadProPlusDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Add AutoMapper
            services.AddAutoMapper(Assembly.GetAssembly(typeof(LoadsMapperProfile)));

            // Add MediatR
            services.AddMediatR(typeof(CreateTruckCommand).GetTypeInfo().Assembly);

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(x => x.RegisterValidatorsFromAssemblyContaining<CreateTruckCommandValidator>());

            // Add Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "LoadProPlus", Version = "v1" });
            });

            // Add AppSettings
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

			var appSettings = appSettingsSection.Get<AppSettings>();

			// Add TokenGenerator
			services.AddSingleton<ITokenGenerator, TokenGenerator>();
			services.AddSingleton<IJwtHelpers, JwtHelper>();

			//Add Broadcasters
			services.AddTransient<ILoadsBroadcaster, LoadsBroadcaster>();
			services.AddTransient<ITrucksBroadcaster, TrucksBroadcaster>();

			var key = Encoding.ASCII.GetBytes(appSettings.Secret);

			// Add Bearer Authentication
			services.AddAuthentication(authentication =>
            {
                authentication.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authentication.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(authentication =>
            {
                authentication.RequireHttpsMetadata = false;
                authentication.SaveToken = true;
				authentication.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(key),
					ValidateIssuer = false,
					ValidateAudience = false,
					ClockSkew = TimeSpan.Zero
                };
            });

			// Add HttpContextAccessor
			services.AddHttpContextAccessor();
			
			// Add current user
			services.AddScoped<ICurrentUser, CurrentUser>();

			//services.AddCors(options =>
			//{
			//	options.AddPolicy("CorsPolicy",
			//		builder => builder
			//		.AllowAnyMethod()
			//		.AllowAnyHeader()
			//		.AllowCredentials()
			//		.AllowAnyOrigin()
			//		.WithOrigins("http://localhost:4200"));
			//});

			// Add SignalR
			services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseGlobalExceptionHandler();

            //app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseAuthentication();

			//app.UseCors("CorsPolicy");

			app.UseCors(builder =>
			{
				builder
				//.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader()
				.AllowCredentials()
				.WithOrigins("http://localhost:4200");
			});

			app.UseMvc();

            // Add SignalR routes
            app.UseSignalR(routes =>
            {
                routes.MapHub<TrucksHub>("/trucks");
                routes.MapHub<LoadsHub>("/loads");
            });
        }
    }
}
