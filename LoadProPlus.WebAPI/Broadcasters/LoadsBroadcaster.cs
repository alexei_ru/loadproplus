﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.WebAPI.Hubs;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Broadcasters
{
	public class LoadsBroadcaster : ILoadsBroadcaster
	{
		private readonly IHubContext<LoadsHub, ILoadsHub> _hubContext;

		public LoadsBroadcaster(IHubContext<LoadsHub, ILoadsHub> hubContext)
		{
			_hubContext = hubContext;
		}

		public async Task BroadcastLoad(LoadViewModel load)
		{
			await Task.Run(() => _hubContext.Clients.All.BroadcastLoad(load));
		}
	}
}
