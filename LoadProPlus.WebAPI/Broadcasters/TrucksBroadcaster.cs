﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.Application.Interfaces.Hubs;
using LoadProPlus.WebAPI.Hubs;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Broadcasters
{
	public class TrucksBroadcaster : ITrucksBroadcaster
	{
		private readonly IHubContext<TrucksHub, ITrucksHub> _hubContext;

		public TrucksBroadcaster(IHubContext<TrucksHub, ITrucksHub> hubContext)
		{
			_hubContext = hubContext;
		}

		public async Task BroadcastTruck(TruckViewModel truck)
		{
			await Task.Run(() => _hubContext.Clients.All.BroadcastTruck(truck));
		}
	}
}
