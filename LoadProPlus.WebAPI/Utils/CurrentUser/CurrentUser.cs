﻿using LoadProPlus.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace LoadProPlus.WebAPI.Utils.CurrentUser
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUser(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string Id
        {
            get => _httpContextAccessor
                .HttpContext
                .User
                .FindFirst(ClaimTypes.NameIdentifier)
                .Value;
        }
    }
}
