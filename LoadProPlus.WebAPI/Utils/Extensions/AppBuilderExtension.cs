﻿using LoadProPlus.Domain.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;

namespace LoadProPlus.WebAPI.Utils.Extensions
{
    public static class AppBuilderExtension
    {
        public static void UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    string errorMessage;
                    var exceptionHandler = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = exceptionHandler.Error;
                    var actionPath = context.Request.Path.HasValue ? context.Request.Path.Value : "unknown";

                    errorMessage = exception.Message;

                    if (exception.IsNotFoundException())
                        context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    else if (exception.IsBadArgumentException())
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    else
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        errorMessage = "Internal Server Error";
                    }

                    await context.Response.WriteAsync(errorMessage);
                });
            });
        }

        private static bool IsNotFoundException(this Exception ex)
        {
            return ex is EntityNotFoundException;
        }

        private static bool IsBadArgumentException(this Exception ex)
        {
            return ex is ArgumentException
                || ex is BadRequestException;
        }
    }
}
