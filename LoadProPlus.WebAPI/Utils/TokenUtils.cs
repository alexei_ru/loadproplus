﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Utils
{
	public static class TokenUtils
	{
		public static Task<string> GetAccessToken(this HttpContext context)
		{
			return context.GetTokenAsync("access_token");
		}
	}
}
