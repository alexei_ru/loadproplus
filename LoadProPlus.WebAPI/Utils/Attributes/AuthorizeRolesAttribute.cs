﻿using Microsoft.AspNetCore.Authorization;

namespace LoadProPlus.WebAPI.Utils.Attributes
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles) : base()
        {
            Roles = string.Join(',', roles);
        }
    }
}
