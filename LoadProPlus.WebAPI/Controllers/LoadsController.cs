﻿using System;
using System.Threading.Tasks;
using LoadProPlus.Application.Contexts.Loads.Commands.CreateLoad;
using LoadProPlus.Application.Contexts.Loads.Commands.DeleteLoad;
using LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoad;
using LoadProPlus.Application.Contexts.Loads.Queries.GetLoadDetails;
using LoadProPlus.Application.Contexts.Loads.Queries.GetLoadsByBrokerId;
using LoadProPlus.Application.Contexts.Loads.Queries.GetLoadStatuses;
using LoadProPlus.Application.Contexts.Loads.Queries.GetPagedAvailableLoads;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Pagination;
using LoadProPlus.WebAPI.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LoadProPlus.WebAPI.Controllers
{
	public class LoadsController : BaseController
	{
		[HttpPost]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> Create([FromBody] CreateLoadCommand command)
		{
            var createdLoad = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new { id = createdLoad.Id }, createdLoad);
		}

		[HttpPut("{id}")]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> Update(Guid id, [FromBody] UpdateLoadCommand command)
		{
            command.Id = id;
            await Mediator.Send(command);

			return NoContent();
		}

		[HttpDelete("{id}")]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> Delete(Guid id)
		{
			await Mediator.Send(new DeleteLoadCommand { Id = id });
			return NoContent();
		}

		[HttpGet("{id}")]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> GetById(Guid id)
		{
			return Ok(await Mediator.Send(new GetLoadDetailsQuery { Id = id }));
		}

		[HttpGet]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> GetLoadsPaged([FromQuery] PageViewModel request)
		{
			return Ok(await Mediator.Send(new GetLoadsByBrokerIdQuery
			{
				AccessToken = await HttpContext.GetAccessToken(),
				PageViewModel = request
			}));
		}

		[HttpGet("all")]
		[Authorize(Roles = Roles.Dispatcher)]
		public async Task<IActionResult> GetAllLoadsPaged([FromQuery]PageViewModel request)
		{
			return Ok(await Mediator.Send(new GetPagedAvailableLoadsQuery
			{
				PageViewModel = request
			}));
		}

		[HttpGet("statuses")]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> GetStatuses()
		{
			return Ok(await Mediator.Send(new GetLoadStatusesQuery()));
		}
	}
}