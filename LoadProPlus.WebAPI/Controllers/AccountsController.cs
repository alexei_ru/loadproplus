﻿using System.Threading.Tasks;
using LoadProPlus.Application.Contexts.Users.Commands.LogoutUser;
using LoadProPlus.Application.Contexts.Users.Commands.RefreshToken;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LoadProPlus.WebAPI.Controllers
{
    public class AccountsController : BaseController
	{
		[HttpPut("refresh-token")]
		public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenCommand request)
		{
			return Ok(await Mediator.Send(request));
		}

        // It must be HttpDelete method...
        [HttpPost("logout")]
        [Authorize]
        public async Task<IActionResult> Logout([FromBody] LogoutUserCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
	}
}
