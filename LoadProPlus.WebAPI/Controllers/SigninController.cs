﻿using LoadProPlus.Application.Contexts.Users.Commands.SigninCompany;
using LoadProPlus.Application.Contexts.Users.Commands.SigninEmployee;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    public class SigninController : BaseController
    {
        [HttpPost("company")]
        public async Task<IActionResult> SigninCompany([FromBody] SigninCompanyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("employee")]
        public async Task<IActionResult> SigninEmployee([FromBody] SigninEmployeeCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
