﻿using LoadProPlus.Application.Contexts.Employees.Queries.CheckUniqueEmployeeUsernameByCompanyId;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    public class EmployeesController : BaseController
    {
        [HttpGet("unique/username")]
        public async Task<IActionResult> CheckUniqueUsernameByCompanyId([FromQuery] CheckUniqueEmployeeUsernameByCompanyIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
