﻿using LoadProPlus.Application.Contexts.Users.Queries.CheckUniqueUserEmail;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    public class UsersController : BaseController
    {
        [HttpGet("unique/email")]
        public async Task<IActionResult> CheckUniqueEmail([FromQuery] CheckUniqueUserEmailQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
