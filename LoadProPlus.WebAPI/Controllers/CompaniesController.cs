﻿using LoadProPlus.Application.Contexts.Companies.Commands.UpdateCompany;
using LoadProPlus.Application.Contexts.Companies.Queries.CheckUniqueCompanyMcNumber;
using LoadProPlus.Application.Contexts.Companies.Queries.CheckUniqueCompanyUsername;
using LoadProPlus.Application.Contexts.Companies.Queries.GetCompanyById;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.WebAPI.Utils.Attributes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    public class CompaniesController : BaseController
    {
        [HttpGet("{id}")]
        [AuthorizeRoles(Roles.BrokerCompany, Roles.DispatcherCompany)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await Mediator.Send(new GetCompanyByIdQuery
            {
                Id = id
            }));
        }

        [HttpPut("{id}")]
        [AuthorizeRoles(Roles.BrokerCompany, Roles.DispatcherCompany)]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateCompanyCommand command)
        {
            command.Id = id;
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpGet("unique/mc-number")]
        public async Task<IActionResult> CheckUniqueMcNumber([FromQuery] CheckUniqueCompanyMcNumberQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("unique/username")]
        public async Task<IActionResult> CheckUniqueUsername([FromQuery] CheckUniqueCompanyUsernameQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
