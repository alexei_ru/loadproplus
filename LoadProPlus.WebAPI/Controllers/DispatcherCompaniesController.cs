﻿using LoadProPlus.Application.Contexts.DispatcherCompanies.Commands.SignupDispatcherCompany;
using LoadProPlus.Application.Contexts.DispatcherCompanies.Queries.GetAllPagedDispatcherCompanies;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Pagination;
using LoadProPlus.WebAPI.Utils.Attributes;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    [Route("api/dispatcher-companies")]
    public class DispatcherCompaniesController : BaseController
    {
        [HttpGet]
        [AuthorizeRoles(Roles.BrokerCompany, Roles.Broker)]
        public async Task<IActionResult> GetAll([FromQuery] PageViewModel pageViewModel)
        {
            return Ok(await Mediator.Send(new GetAllPagedDispatcherCompaniesQuery
            {
                PageViewModel = pageViewModel
            }));
        }

        [HttpPost]
        public async Task<IActionResult> SignupDispatcherCompany([FromBody] SignupDispatcherCompanyCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
