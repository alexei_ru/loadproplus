﻿using LoadProPlus.Application.Contexts.Dispatchers.Commands.DeleteDispatcher;
using LoadProPlus.Application.Contexts.Dispatchers.Commands.SignupDispatcher;
using LoadProPlus.Application.Contexts.Dispatchers.Commands.UpdateDispatcherCommand;
using LoadProPlus.Application.Contexts.Dispatchers.Queries.GetDispatcherByIdAndCompanyId;
using LoadProPlus.Application.Contexts.Dispatchers.Queries.GetPagedDispatchersByCompanyId;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    public class DispatchersController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = Roles.DispatcherCompany)]
        public async Task<IActionResult> GetAll([FromQuery] PageViewModel pageViewModel)
        {
            return Ok(await Mediator.Send(new GetPagedDispatchersByCompanyIdQuery
            {
                PageViewModel = pageViewModel
            }));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.DispatcherCompany)]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteDispatcherCommand
            {
                DispatcherId = id
            }));
        }

        [HttpGet("{id}")]
        [Authorize(Roles = Roles.DispatcherCompany)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await Mediator.Send(new GetDispatcherByIdAndCompanyIdQuery
            {
                DispatcherId = id
            }));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = Roles.DispatcherCompany)]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateDispatcherCommand command)
        {
            command.DispatcherId = id;
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = Roles.DispatcherCompany)]
        public async Task<IActionResult> Create([FromBody] SignupDispatcherCommand command)
        {
            var createdDispatcher = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new { id = createdDispatcher.Id }, createdDispatcher);
        }
    }
}
