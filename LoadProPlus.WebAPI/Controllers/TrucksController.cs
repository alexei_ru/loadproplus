﻿using LoadProPlus.Application.Contexts.Trucks.Commands.CreateTruck;
using LoadProPlus.Application.Contexts.Trucks.Commands.DeleteTruck;
using LoadProPlus.Application.Contexts.Trucks.Commands.UpdateTruck;
using LoadProPlus.Application.Contexts.Trucks.Queries.GetTruckByIdAndDispatcherId;
using LoadProPlus.Application.Contexts.Trucks.Queries.GetPagedTrucksByDispatcherId;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using LoadProPlus.Application.Contexts.Trucks.Queries.GetPagedAvailableTrucks;

namespace LoadProPlus.WebAPI.Controllers
{
	public class TrucksController : BaseController
	{
		[HttpPost]
		[Authorize(Roles = Roles.Dispatcher)]
		public async Task<IActionResult> Create([FromBody] CreateTruckCommand command)
		{
			var createdTruck = await Mediator.Send(command);
			return CreatedAtAction(nameof(GetById), new { id = createdTruck.Id }, createdTruck);
		}

		[HttpGet]
		[Authorize(Roles = Roles.Dispatcher)]
		public async Task<IActionResult> GetAllByDispatcher([FromQuery] PageViewModel pageViewModel)
		{
			return Ok(await Mediator.Send(new GetPagedTrucksByDispatcherIdQuery
			{
				PageViewModel = pageViewModel
			}));
		}

		[HttpGet("all")]
		[Authorize(Roles = Roles.Broker)]
		public async Task<IActionResult> GetAll([FromQuery] PageViewModel pageViewModel)
		{
			return Ok(await Mediator.Send(new GetPagedAvailableTrucksQuery
			{
				PageViewModel = pageViewModel
			}));
		}

		[HttpDelete("{id}")]
		[Authorize(Roles = Roles.Dispatcher)]
		public async Task<IActionResult> Delete(Guid id)
		{
			await Mediator.Send(new DeleteTruckCommand
			{
				TruckId = id
			});

			return NoContent();
		}

		[HttpGet("{id}")]
		[Authorize(Roles = Roles.Dispatcher)]
		public async Task<IActionResult> GetById(Guid id)
		{
			return Ok(await Mediator.Send(new GetTruckByIdAndDispatcherIdQuery
			{
				TruckId = id
			}));
		}

		[HttpPut("{id}")]
		[Authorize(Roles = Roles.Dispatcher)]
		public async Task<IActionResult> Update(Guid id, [FromBody] UpdateTruckCommand command)
		{
			command.TruckId = id;
			await Mediator.Send(command);

			return NoContent();
		}
	}
}
