﻿using LoadProPlus.Application.Contexts.BrokerCompanies.Commands.SignupBrokerCompany;
using LoadProPlus.Application.Contexts.BrokerCompanies.Queries.GetAllPagedBrokerCompanies;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Pagination;
using LoadProPlus.WebAPI.Utils.Attributes;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    [Route("api/broker-companies")]
    public class BrokerCompaniesController : BaseController
    {
        [HttpGet]
        [AuthorizeRoles(Roles.Dispatcher, Roles.DispatcherCompany)]
        public async Task<IActionResult> GetAll([FromQuery] PageViewModel pageViewModel)
        {
            return Ok(await Mediator.Send(new GetAllPagedBrokerCompaniesQuery
            {
                PageViewModel = pageViewModel
            }));
        }

        [HttpPost]
        public async Task<IActionResult> SignupBrokerCompany([FromBody] SignupBrokerCompanyCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
