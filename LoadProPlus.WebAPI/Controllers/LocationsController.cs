﻿using System;
using System.Threading.Tasks;
using LoadProPlus.Application.Contexts.Locations.Queries.GetCitiesByState;
using LoadProPlus.Application.Contexts.Locations.Queries.GetStates;
using Microsoft.AspNetCore.Mvc;

namespace LoadProPlus.WebAPI.Controllers
{
	public class LocationsController : BaseController
	{
		[HttpGet("states")]
		public async Task<IActionResult> GetAllStates()
		{
			return Ok(await Mediator.Send(new GetAllStatesQuery()));
		}

		[HttpGet("cities/{stateId}")]
		public async Task<IActionResult> GetAllCities(Guid stateId)
		{
			return Ok(await Mediator.Send(new GetCitiesByStateQuery
			{
				StateId = stateId
			}));
		}
	}
}
