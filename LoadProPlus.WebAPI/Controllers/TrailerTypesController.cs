﻿using LoadProPlus.Application.Contexts.TrailerTypes.Queries.GetTrailerTypes;
using LoadProPlus.Application.Utils.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
	[Route("api/trailer-types")]
	public class TrailerTypesController : BaseController
	{
		//[Authorize] - Didnt work. Hz why. To be investigated...
		[HttpGet]
		public async Task<IActionResult> GetTrailerTypes()
		{
			return Ok(await Mediator.Send(new GetTrailerTypesQuery()));
		}
	}
}
