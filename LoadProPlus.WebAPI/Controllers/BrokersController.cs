﻿using LoadProPlus.Application.Contexts.Brokers.Commands.DeleteBroker;
using LoadProPlus.Application.Contexts.Brokers.Commands.SignupBroker;
using LoadProPlus.Application.Contexts.Brokers.Commands.UpdateBroker;
using LoadProPlus.Application.Contexts.Brokers.Queries.GetBrokerByIdAndCompanyId;
using LoadProPlus.Application.Contexts.Brokers.Queries.GetPagedBrokersByCompanyId;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Pagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LoadProPlus.WebAPI.Controllers
{
    public class BrokersController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = Roles.BrokerCompany)]
        public async Task<IActionResult> GetAll([FromQuery] PageViewModel pageViewModel)
        {
            return Ok(await Mediator.Send(new GetPagedBrokersByCompanyIdQuery
            {
                PageViewModel = pageViewModel
            }));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.BrokerCompany)]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteBrokerCommand
            {
                BrokerId = id,
            }));
        }

        [HttpGet("{id}")]
        [Authorize(Roles = Roles.BrokerCompany)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await Mediator.Send(new GetBrokerByIdAndCompanyIdQuery
            {
                BrokerId = id
            }));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = Roles.BrokerCompany)]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateBrokerCommand command)
        {
            command.BrokerId = id;
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = Roles.BrokerCompany)]
        public async Task<IActionResult> Create([FromBody] SignupBrokerCommand command)
        {
            var createdBroker = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new { id = createdBroker.Id }, createdBroker);
        }
    }
}
