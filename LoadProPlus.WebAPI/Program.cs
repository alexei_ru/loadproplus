﻿using LoadProPlus.Application.Interfaces;
using LoadProPlus.Persistence;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LoadProPlus.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    var context = (LoadProPlusDbContext) scope.ServiceProvider.GetService<ILoadProPlusDbContext>();
                    context.Database.Migrate();
                    LoadProPlusInitializer.Initialize(context);
                }
                catch(Exception exception)
                {
                    // TO DO: Log this exception
                    throw new Exception("An error ocurred while migrating or initializing the database.", exception);
                }
            }

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
