import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { AppComponent } from './app.component';

// Custom modules
import { CoreModule } from '@core';
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { SigninModule } from '@signin';
import { SignupModule } from '@signup';
import { BrokerCompanyModule } from '@broker-company';
import { DispatcherCompanyModule } from '@dispatcher-company';
import { BrokerModule } from '@broker';
import { DispatcherModule } from '@dispatcher';
import { SharedModule } from '@shared';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AppMaterialModule,
    SharedModule,
    SigninModule,
    SignupModule,
    CoreModule,
    BrowserAnimationsModule,
    BrokerCompanyModule,
    DispatcherCompanyModule,
    BrokerModule,
    DispatcherModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
