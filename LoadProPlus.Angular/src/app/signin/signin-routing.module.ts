import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { NotAuthenticatedGuard } from '@core';

// Components
import {
  LoginComponent,
  LoginCompanyComponent,
  LoginEmployeeComponent
} from './pages';
import { SigninComponent } from './signin.component';

const routes: Routes = [
  {
    path: 'login',
    component: SigninComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
        canActivate: [NotAuthenticatedGuard]
      },
      {
        path: 'employee',
        component: LoginEmployeeComponent,
        canActivate: [NotAuthenticatedGuard]
      },
      {
        path: 'company',
        component: LoginCompanyComponent,
        canActivate: [NotAuthenticatedGuard]
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SigninRoutingModule {

}
