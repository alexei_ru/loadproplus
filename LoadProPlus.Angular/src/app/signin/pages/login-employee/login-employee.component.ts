import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, Employee, Roles } from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login-employee',
  templateUrl: './login-employee.component.html',
  styleUrls: ['./login-employee.component.scss'],
  providers: [NgOnDestroy]
})
export class LoginEmployeeComponent implements OnInit {

  formGroup: FormGroup;
  isLoading = false;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      mcNumber: new FormControl('', [Validators.required, Validators.maxLength(70)]),
      username: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(40)])
    });
  }

  onSubmit(): void {
    this.isLoading = true;
    const employee: Employee = this.formGroup.getRawValue();
    this.authenticationService.loginEmployee(employee).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.isLoading = false;
      this.snackBarService.openSuccess('Login successfully');
      this.navigateToPrincipalPage();
    }, () => {
      this.isLoading = false;
      this.snackBarService.openError('Login failed');
    });
  }

  get mcNumber(): AbstractControl {
    return this.formGroup.get('mcNumber');
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  navigateToPrincipalPage(): void {
    const userRole = this.authenticationService.role;
    switch (userRole) {
      case Roles.Broker : this.router.navigateByUrl('/broker'); break;
      case Roles.Dispatcher : this.router.navigateByUrl('/dispatcher'); break;
      default: this.router.navigateByUrl('/');
    }
  }
}
