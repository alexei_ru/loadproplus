import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, Company, Roles } from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login-company',
  templateUrl: './login-company.component.html',
  styleUrls: ['./login-company.component.scss'],
  providers: [NgOnDestroy]
})
export class LoginCompanyComponent implements OnInit {

  formGroup: FormGroup;
  isLoading = false;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private onDestroy$: NgOnDestroy,
    private snackBarSerivce: SnackBarService
  ) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      mcNumber: new FormControl('', [Validators.required, Validators.maxLength(70)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(40)])
    });
  }

  onSubmit(): void {
    this.isLoading = true;
    const company: Company = this.formGroup.getRawValue();
    this.authenticationService.loginCompany(company).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.isLoading = false;
      this.snackBarSerivce.openSuccess('Login successfully');
      this.navigateToPrincipalPage();
    }, () => {
      this.isLoading = false;
      this.snackBarSerivce.openError('Login failed');
    });
  }

  get mcNumber(): AbstractControl {
    return this.formGroup.get('mcNumber');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  navigateToPrincipalPage(): void {
    const userRole = this.authenticationService.role;
    switch (userRole) {
      case Roles.BrokerCompany : this.router.navigateByUrl('/broker-company'); break;
      case Roles.DispatcherCompany : this.router.navigateByUrl('/dispatcher-company'); break;
      default: this.router.navigateByUrl('/');
    }
  }
}
