import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Custom modules
import { SigninRoutingModule } from './signin-routing.module';
import { SigninMaterialModule } from './signin-material.module';
import { SharedModule } from '@shared';

// Components
import {
  LoginCompanyComponent,
  LoginEmployeeComponent,
  LoginComponent
} from './pages';
import { SigninComponent } from './signin.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    SigninRoutingModule,
    SigninMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    LoginCompanyComponent,
    LoginEmployeeComponent,
    LoginComponent,
    SigninComponent
  ]
})
export class SigninModule { }
