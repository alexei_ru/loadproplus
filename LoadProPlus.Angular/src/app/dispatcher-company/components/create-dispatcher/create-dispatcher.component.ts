import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import {
  DispatchersService,
  Dispatcher,
  EmployeesService,
  AuthenticationService,
  checkUniqueUsernameByCompanyIdValidator,
  UsersService,
  checkUniqueUserEmailValidator
} from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-create-dispatcher',
  templateUrl: './create-dispatcher.component.html',
  styleUrls: ['./create-dispatcher.component.scss'],
  providers: [NgOnDestroy]
})
export class CreateDispatcherComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CreateDispatcherComponent>,
    private dispatchersService: DispatchersService,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService,
    private employeesService: EmployeesService,
    private authenticationService: AuthenticationService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.maxLength(50)],
        checkUniqueUsernameByCompanyIdValidator(this.authenticationService.userId, this.employeesService)
      ),
      email: new FormControl('',
        [Validators.required, Validators.maxLength(254)],
        checkUniqueUserEmailValidator(this.usersService)
      ),
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      phone: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(40)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(40)])
    });
  }

  onSubmit(): void {
    const dispatcher: Dispatcher = this.formGroup.getRawValue();
    this.dispatchersService.create(dispatcher).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe((createdDispatcher: Dispatcher) => {
      this.dialogRef.close({
        result: createdDispatcher
      });
    }, () => {
      this.snackBarService.openError('Dispatcher creation failed');
    });
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get name(): AbstractControl {
    return this.formGroup.get('name');
  }

  get surname(): AbstractControl {
    return this.formGroup.get('surname');
  }

  get phone(): AbstractControl {
    return this.formGroup.get('phone');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

}
