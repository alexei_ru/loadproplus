import { EditDispatcherDialogData } from '../../models';
import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormControl,
  AbstractControl
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {
  DispatchersService,
  Dispatcher,
  EmployeesService,
  AuthenticationService,
  checkUniqueUsernameByCompanyIdValidator,
  UsersService,
  checkUniqueUserEmailValidator
} from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-edit-dispatcher',
  templateUrl: './edit-dispatcher.component.html',
  styleUrls: ['./edit-dispatcher.component.scss'],
  providers: [NgOnDestroy]
})
export class EditDispatcherComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditDispatcherComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditDispatcherDialogData,
    private dispatchersService: DispatchersService,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService,
    private employeesService: EmployeesService,
    private authenticationService: AuthenticationService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.dispatchersService.getById(this.data.id).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe((broker: Dispatcher) => {
      this.formGroup.patchValue({
        username: broker.username,
        email: broker.email,
        name: broker.name,
        surname: broker.surname,
        phone: broker.phone
      });
    });
  }

  initializeForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.maxLength(50)],
        checkUniqueUsernameByCompanyIdValidator(this.authenticationService.userId, this.employeesService, this.data.id)
      ),
      email: new FormControl('',
        [Validators.required, Validators.maxLength(254)],
        checkUniqueUserEmailValidator(this.usersService, this.data.id)
      ),
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      phone: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      password: new FormControl('', [Validators.maxLength(40)]),
      confirmPassword: new FormControl('', [Validators.maxLength(40)])
    });
  }

  onSubmit(): void {
    const dispatcher: Dispatcher = this.formGroup.getRawValue();
    this.dispatchersService.update(this.data.id, dispatcher).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.dialogRef.close({
        result: true
      });
    }, () => {
      this.snackBarService.openError('Dispatcher modification failed');
    });
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get name(): AbstractControl {
    return this.formGroup.get('name');
  }

  get surname(): AbstractControl {
    return this.formGroup.get('surname');
  }

  get phone(): AbstractControl {
    return this.formGroup.get('phone');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }
}
