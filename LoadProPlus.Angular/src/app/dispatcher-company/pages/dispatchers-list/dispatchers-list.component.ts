import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import {
  DataTableComponent,
  PageView,
  PagedResult,
  Dispatcher
} from '@core';
import { NgOnDestroy, SnackBarService, ConfirmationDialogComponent } from '@shared';
import { DispatchersService } from '@core';
import { CreateDispatcherComponent, EditDispatcherComponent } from '../../components';

@Component({
  selector: 'app-dispatchers-list',
  templateUrl: './dispatchers-list.component.html',
  styleUrls: ['./dispatchers-list.component.scss'],
  providers: [NgOnDestroy]
})
export class DispatchersListComponent extends DataTableComponent {

  displayedColumns: string[] = ['name', 'surname', 'email', 'phone', 'actions'];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    private dispatchersService: DispatchersService,
    private dialog: MatDialog,
    onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) {
    super(onDestroy$);
  }

  fetchData(vm: PageView): Observable<PagedResult<Dispatcher>> {
    return this.dispatchersService.getPaged(vm);
  }

  onClickEdit(id: string): void {
    const dialogRef = this.dialog.open(EditDispatcherComponent, {
      width: '350px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this.snackBarService.openSuccess('Dispatcher edited successfully');
        this.refresh.next();
      }
    });
  }

  onClickDelete(id: string): void {
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        message: 'Are you sure you want to delete this dispatcher?'
      }
    });

    this.confirmationDialog.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(result => {
      if (result) {
        this.dispatchersService.delete(id).subscribe(() => {
          this.snackBarService.openSuccess('Dispatcher deleted successfully');
          this.deleteItem(id, 'id');
        }, () => {
          this.snackBarService.openError('Dispatcher deletion failed');
        });
      } else {
        return;
      }
    });
  }

  onClickAdd(): void {
    const dialogRef = this.dialog.open(CreateDispatcherComponent, {
      width: '350px'
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this.snackBarService.openSuccess('Dispatcher created successfully');
        this.appendItem(response.result);
      }
    });
  }
}
