import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Custom modules
import {
  RoleGuard,
  BrokerCompaniesListComponent,
  LayoutComponent,
  CompanyProfileComponent
} from '@core';

// Components
import {
  DashboardComponent,
  DispatchersListComponent
} from './pages';

const routes: Routes = [
  {
    path: 'dispatcher-company',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'DispatcherCompany'
        }
      },
      {
        path: 'dispatchers-list',
        component: DispatchersListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'DispatcherCompany'
        }
      },
      {
        path: 'broker-companies',
        component: BrokerCompaniesListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'ViewBrokerCompanies'
        }
      },
      {
        path: 'profile',
        component: CompanyProfileComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'DispatcherCompany'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DispatcherCompanyRoutingModule {

}
