import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Custom modules
import { SharedModule } from '@shared';
import { DispatcherCompanyRoutingModule } from './dispatcher-company-routing.module';
import { DispatcherCompanyMaterialModule } from './dispatcher-company-material.module';

// Components
import {
  DashboardComponent,
  DispatchersListComponent
} from './pages';
import {
  EditDispatcherComponent,
  CreateDispatcherComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    SharedModule,
    DispatcherCompanyRoutingModule,
    DispatcherCompanyMaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    DashboardComponent,
    DispatchersListComponent,
    EditDispatcherComponent,
    CreateDispatcherComponent,
  ],
  entryComponents: [
    EditDispatcherComponent,
    CreateDispatcherComponent
  ]
})
export class DispatcherCompanyModule { }
