import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

// Custom modules
import { BrokerCompanyRoutingModule } from './broker-company-routing.module';
import { BrokerCompanyMaterialModule } from './broker-company-material.module';
import { SharedModule } from '@shared';

// Components
import { DashboardComponent, BrokersListComponent } from './pages';
import {
  EditBrokerComponent,
  CreateBrokerComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    BrokerCompanyMaterialModule,
    BrokerCompanyRoutingModule,
    FlexLayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    DashboardComponent,
    BrokersListComponent,
    EditBrokerComponent,
    CreateBrokerComponent
  ],
  entryComponents: [
    EditBrokerComponent,
    CreateBrokerComponent
  ]
})
export class BrokerCompanyModule { }
