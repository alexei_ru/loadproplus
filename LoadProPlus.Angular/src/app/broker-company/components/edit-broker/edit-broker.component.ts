import { EditBrokerDialogData } from '../../models';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import {
  BrokersService,
  Broker,
  EmployeesService,
  AuthenticationService,
  checkUniqueUsernameByCompanyIdValidator,
  UsersService,
  checkUniqueUserEmailValidator
} from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-edit-broker',
  templateUrl: './edit-broker.component.html',
  styleUrls: ['./edit-broker.component.scss'],
  providers: [NgOnDestroy]
})
export class EditBrokerComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditBrokerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditBrokerDialogData,
    private brokersService: BrokersService,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService,
    private employeesService: EmployeesService,
    private authenticationService: AuthenticationService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.initializeData();
  }

  initializeData(): void {
    this.brokersService.getById(this.data.id).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe((broker: Broker) => {
      this.formGroup.patchValue({
        username: broker.username,
        email: broker.email,
        name: broker.name,
        surname: broker.surname,
        phone: broker.phone
      });
    });
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.maxLength(50)],
        checkUniqueUsernameByCompanyIdValidator(this.authenticationService.userId, this.employeesService, this.data.id)
      ),
      email: new FormControl('',
        [Validators.required, Validators.maxLength(254)],
        checkUniqueUserEmailValidator(this.usersService, this.data.id)
      ),
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      phone: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      password: new FormControl('', [Validators.maxLength(40)]),
      confirmPassword: new FormControl('', [Validators.maxLength(40)])
    });
  }

  onSubmit(): void {
    const broker: Broker = this.formGroup.getRawValue();
    this.brokersService.update(this.data.id, broker).subscribe(() => {
      this.dialogRef.close({
        result: true
      });
    }, () => {
      this.snackBarService.openError('Broker modification failed');
    });
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get name(): AbstractControl {
    return this.formGroup.get('name');
  }

  get surname(): AbstractControl {
    return this.formGroup.get('surname');
  }

  get phone(): AbstractControl {
    return this.formGroup.get('phone');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

}
