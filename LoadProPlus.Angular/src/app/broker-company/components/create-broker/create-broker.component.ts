import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import {
  BrokersService,
  Broker,
  EmployeesService,
  checkUniqueUsernameByCompanyIdValidator,
  AuthenticationService,
  UsersService,
  checkUniqueUserEmailValidator
} from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-create-broker',
  templateUrl: './create-broker.component.html',
  styleUrls: ['./create-broker.component.scss'],
  providers: [NgOnDestroy]
})
export class CreateBrokerComponent implements OnInit {

  formGroup: FormGroup;
  userId: string;

  constructor(
    public dialogRef: MatDialogRef<CreateBrokerComponent>,
    private brokersService: BrokersService,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService,
    private employeesService: EmployeesService,
    private authenticationService: AuthenticationService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.maxLength(50)],
        checkUniqueUsernameByCompanyIdValidator(this.authenticationService.userId, this.employeesService)
      ),
      email: new FormControl('',
        [Validators.required, Validators.maxLength(254)],
        checkUniqueUserEmailValidator(this.usersService)
      ),
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      phone: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(40)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(40)])
    });
  }

  onSubmit(): void {
    const broker: Broker = this.formGroup.getRawValue();
    this.brokersService.create(broker).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe((createdBroker: Broker) => {
      this.dialogRef.close({
        result: createdBroker
      });
    }, () => {
      this.snackBarService.openError('Broker creation failed');
    });
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get name(): AbstractControl {
    return this.formGroup.get('name');
  }

  get surname(): AbstractControl {
    return this.formGroup.get('surname');
  }

  get phone(): AbstractControl {
    return this.formGroup.get('phone');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

}
