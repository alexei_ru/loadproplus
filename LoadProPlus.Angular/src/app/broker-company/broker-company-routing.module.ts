import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Custom modules
import {
  RoleGuard,
  DispatcherCompaniesListComponent,
  LayoutComponent,
  CompanyProfileComponent
} from '@core';

// Components
import {
  DashboardComponent,
  BrokersListComponent
} from './pages';

const routes: Routes = [
  {
    path: 'broker-company',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'BrokerCompany'
        }
      },
      {
        path: 'brokers-list',
        component: BrokersListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'BrokerCompany'
        }
      },
      {
        path: 'dispatcher-companies',
        component: DispatcherCompaniesListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'ViewDispatcherCompanies'
        }
      },
      {
        path: 'profile',
        component: CompanyProfileComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'BrokerCompany'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BrokerCompanyRoutingModule {

}
