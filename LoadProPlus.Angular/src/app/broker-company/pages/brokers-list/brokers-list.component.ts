import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { EditBrokerComponent, CreateBrokerComponent } from '../../components';
import {
  DataTableComponent,
  PageView,
  PagedResult,
  BrokersService,
  Broker
} from '@core';
import { NgOnDestroy, SnackBarService, ConfirmationDialogComponent } from '@shared';

@Component({
  selector: 'app-brokers-list',
  templateUrl: './brokers-list.component.html',
  styleUrls: ['./brokers-list.component.scss'],
  providers: [NgOnDestroy]
})
export class BrokersListComponent extends DataTableComponent {

  displayedColumns: string[] = ['name', 'surname', 'email', 'phone', 'actions'];

  constructor(
    private brokersService: BrokersService,
    private dialog: MatDialog,
    onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) {
    super(onDestroy$);
  }

  fetchData(vm: PageView): Observable<PagedResult<Broker>> {
    return this.brokersService.getPaged(vm);
  }

  onClickEdit(id: string): void {
    const dialogRef = this.dialog.open(EditBrokerComponent, {
      width: '350px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this.snackBarService.openSuccess('Broker edited successfully');
        this.refresh.next();
      }
    });
  }

  onClickDelete(id: string): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        message: 'Are you sure you want to delete this broker?'
      }
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(result => {
      if (result) {
        this.brokersService.delete(id).subscribe(() => {
          this.snackBarService.openSuccess('Broker deleted successfully');
          this.deleteItem(id, 'id');
        }, () => {
          this.snackBarService.openError('Broker deletion failed');
        });
      }
    });
  }

  onClickAdd(): void {
    const dialogRef = this.dialog.open(CreateBrokerComponent, {
      width: '350px'
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this.snackBarService.openSuccess('Broker created successfully');
        this.appendItem(response.result);
      }
    });
  }
}
