import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Custom modules
import { SharedMaterialModule } from './shared-material.module';

// Services
import {
  SnackBarService,
  SpinnerService
} from './services';

// Components
import {
  LoadingComponent,
  ConfirmationDialogComponent,
  SpinnerComponent
} from './components';

// Guards
import { CanDeactivateGuard } from './guards';

@NgModule({
  imports: [
    CommonModule,
    SharedMaterialModule
  ],
  declarations: [
    LoadingComponent,
    ConfirmationDialogComponent,
    SpinnerComponent
  ],
  exports: [
    LoadingComponent,
    SpinnerComponent
  ],
  providers: [
    SnackBarService,
    SpinnerService,
    CanDeactivateGuard
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class SharedModule {

}
