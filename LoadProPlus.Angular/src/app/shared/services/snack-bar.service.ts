import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable()
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {

  }

  private open(message: string, panelClass: string[]) {
    const config = new MatSnackBarConfig();
    config.duration = 5000;
    config.panelClass = panelClass;
    config.verticalPosition = 'bottom';
    config.horizontalPosition = 'end';

    this.snackBar.open(message, null, config);
  }

  openSuccess(message: string) {
    this.open(message, ['snack-bar-success']);
  }

  openWarning(message: string) {
    this.open(message, ['snack-bar-warning']);
  }

  openError(message: string) {
    this.open(message, ['snack-bar-error']);
  }
}
