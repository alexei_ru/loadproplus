import { NgModule } from '@angular/core';

import {
  MatProgressSpinnerModule,
  MatButtonModule,
  MatDialogModule
} from '@angular/material';

@NgModule({
  imports: [
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDialogModule
  ],
  exports: [
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDialogModule
  ]
})
export class SharedMaterialModule {

}
