import { AbstractControl } from '@angular/forms';

export class ValidatorHelper {
  static RequireAutocompleteObject(control: AbstractControl) {
    const selection: any = control.value;

    if (typeof selection === 'string') {
        return { incorrect: true };
    }

    return null;
  }
}
