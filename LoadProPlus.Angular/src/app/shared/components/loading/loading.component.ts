import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {

  @Input() diameter = 50;
  @Input() strokeWidth = 5;
  @Input() borderRadius = '0px';
  @Input() shadow = true;

}
