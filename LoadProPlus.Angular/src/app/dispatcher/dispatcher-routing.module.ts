import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Custom modules
import {
  RoleGuard,
  BrokerCompaniesListComponent,
  LayoutComponent
} from '@core';

// Components
import {
  DashboardComponent,
  TrucksListComponent
} from './pages';

const routes: Routes = [
  {
    path: 'dispatcher',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'Dispatcher'
        }
      },
      {
        path: 'trucks-list',
        component: TrucksListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'Dispatcher'
        }
      },
      {
        path: 'broker-companies',
        component: BrokerCompaniesListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'ViewBrokerCompanies'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DispatcherRoutingModule {

}
