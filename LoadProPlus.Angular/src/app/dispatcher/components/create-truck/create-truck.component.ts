import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import { startWith, map, takeUntil } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  TrucksService,
  Truck,
  TrailerTypesService,
  TrailerType
} from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';

@Component({
  selector: 'app-create-truck',
  templateUrl: './create-truck.component.html',
  styleUrls: ['./create-truck.component.scss'],
  providers: [NgOnDestroy]
})
export class CreateTruckComponent implements OnInit {

  formGroup: FormGroup;
  trailerTypes: TrailerType[];
  filteredTrailerTypes: Observable<TrailerType[]>;
  isDisplayedSecondaryDriverForm = false;

  constructor(
    public dialogRef: MatDialogRef<CreateTruckComponent>,
    private trailerTypesService: TrailerTypesService,
    private truckService: TrucksService,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.initializeTrailerTypes();
  }

  initializeTrailerTypes(): void {
    this.trailerTypesService.getAll().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe((trailerTypes: TrailerType[]) => {
      this.trailerTypes = trailerTypes;
      this.filterTrailerTypes();
    });
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      truckNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      trailerNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      trailerTypeId: new FormControl('', [Validators.required]),
      principalDriver: new FormGroup({
        firstName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
        email: new FormControl('', [Validators.required, Validators.maxLength(254)])
      })
    });
  }

  onSubmit(): void {
    const truck: Truck = this.formGroup.getRawValue();
    this.truckService.create(truck).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe((createdTruck: Truck) => {
      createdTruck.trailerType = this.getTrailerNameById(truck.trailerTypeId);
      this.dialogRef.close({
        result: createdTruck
      });
    }, () => {
      this.snackBarService.openError('Truck creation failed');
    });
  }

  getTrailerNameById(id: string): string {
    return this.trailerTypes.find(x => x.id === id).name;
  }

  get displayTrailerType() {
    return (id: string) => {
      if (id) {
        return this.getTrailerNameById(id);
      }
    };
  }

  showSecondaryDriverForm(): void {
    this.isDisplayedSecondaryDriverForm = true;
    this.formGroup.addControl(
      'secondaryDriver', new FormGroup({
        firstName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
        email: new FormControl('', [Validators.required, Validators.maxLength(254)])
      })
    );
  }

  hideSecondaryDriverForm(): void {
    this.isDisplayedSecondaryDriverForm = false;
    this.formGroup.removeControl('secondaryDriver');
  }

  filterTrailerTypes(): void {
    this.filteredTrailerTypes = this.trailerTypeId.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value)),
      takeUntil(this.onDestroy$)
    );
  }

  get truckNumber(): AbstractControl {
    return this.formGroup.get('truckNumber');
  }

  get trailerNumber(): AbstractControl {
    return this.formGroup.get('trailerNumber');
  }

  get trailerTypeId(): AbstractControl {
    return this.formGroup.get('trailerTypeId');
  }

  // Principal driver
  private get principalDriver(): AbstractControl {
    return this.formGroup.get('principalDriver');
  }

  get principalDriverFirstName(): AbstractControl {
    return this.principalDriver.get('firstName');
  }

  get principalDriverSurname(): AbstractControl {
    return this.principalDriver.get('surname');
  }

  get principalDriverPhoneNumber(): AbstractControl {
    return this.principalDriver.get('phoneNumber');
  }

  get principalDriverEmail(): AbstractControl {
    return this.principalDriver.get('email');
  }

  // Secondary driver
  private get secondaryDriver(): AbstractControl {
    return this.formGroup.get('secondaryDriver');
  }

  get secondaryDriverFirstName(): AbstractControl {
    return this.secondaryDriver.get('firstName');
  }

  get secondaryDriverSurname(): AbstractControl {
    return this.secondaryDriver.get('surname');
  }

  get secondaryDriverPhoneNumber(): AbstractControl {
    return this.secondaryDriver.get('phoneNumber');
  }

  get secondaryDriverEmail(): AbstractControl {
    return this.secondaryDriver.get('email');
  }

  private filter(value: any): TrailerType[] {
    let filterValue = '';
    if (typeof (value) === 'object') {
      filterValue = value.name.toLowerCase();
    } else {
      filterValue = value.toLowerCase();
    }
    return this.trailerTypes.filter(trailerType => trailerType.name.toLowerCase().includes(filterValue));
  }
}
