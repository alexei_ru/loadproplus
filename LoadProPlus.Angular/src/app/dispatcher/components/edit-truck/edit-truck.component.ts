import { EditTruckDialogData } from '../../models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith, map, takeUntil } from 'rxjs/operators';
import {
  TrucksService,
  Truck,
  TrailerType,
  TrailerTypesService
} from '@core';
import { NgOnDestroy, SnackBarService } from '@shared';

@Component({
  selector: 'app-edit-truck',
  templateUrl: './edit-truck.component.html',
  styleUrls: ['./edit-truck.component.scss'],
  providers: [NgOnDestroy]
})
export class EditTruckComponent implements OnInit {

  formGroup: FormGroup;
  trailerTypes: TrailerType[];
  filteredTrailerTypes: Observable<TrailerType[]>;
  isDisplayedSecondaryDriverForm = false;
  truck: Truck;

  constructor(
    public dialogRef: MatDialogRef<EditTruckComponent>,
    private trailerTypesService: TrailerTypesService,
    private trucksService: TrucksService,
    @Inject(MAT_DIALOG_DATA) public data: EditTruckDialogData,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.initializeData();
  }

  initializeData(): void {
    const trailerTypes$ = this.trailerTypesService.getAll();
    const truck$ = this.trucksService.getById(this.data.id);

    combineLatest([trailerTypes$, truck$])
      .pipe(
        takeUntil(this.onDestroy$)
      )
      .subscribe(([trailerTypes, truck]) => {
        this.trailerTypes = trailerTypes;
        this.truck = truck;
        this.formGroup.patchValue({
          truckNumber: truck.truckNumber,
          trailerNumber: truck.trailerNumber,
          trailerTypeId: truck.trailerTypeId,
          principalDriver: truck.principalDriver
        });

        if (truck.secondaryDriver) {
          this.isDisplayedSecondaryDriverForm = true;
          this.setValidatorsSecondaryDriver();
          this.secondaryDriver.setValue(truck.secondaryDriver);
        } else {
          this.formGroup.removeControl('secondaryDriver');
        }

        this.filterTrailerTypes();
      });
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      truckNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      trailerNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      trailerTypeId: new FormControl('', [Validators.required]),
      principalDriver: new FormGroup({
        firstName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(30)]),
        email: new FormControl('', [Validators.required, Validators.maxLength(254)])
      }),
      secondaryDriver: new FormGroup({
        firstName: new FormControl(''),
        surname: new FormControl(''),
        phoneNumber: new FormControl(''),
        email: new FormControl('')
      })
    });
  }

  setValidatorsSecondaryDriver(): void {
    this.secondaryDriverEmail.setValidators([Validators.required, Validators.maxLength(254)]);
    this.secondaryDriverFirstName.setValidators([Validators.required, Validators.maxLength(50)]);
    this.secondaryDriverPhoneNumber.setValidators([Validators.required, Validators.maxLength(30)]);
    this.secondaryDriverSurname.setValidators([Validators.required, Validators.maxLength(50)]);
    this.secondaryDriverEmail.updateValueAndValidity();
    this.secondaryDriverFirstName.updateValueAndValidity();
    this.secondaryDriverPhoneNumber.updateValueAndValidity();
    this.secondaryDriverSurname.updateValueAndValidity();
  }

  onSubmit(): void {
    const truck: Truck = this.formGroup.getRawValue();
    this.trucksService.update(this.data.id, truck).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.dialogRef.close({
        result: truck
      });
    }, () => {
      this.snackBarService.openError('Truck modification failed');
    });
  }

  getTrailerNameById(id: string): string {
    return this.trailerTypes.find(x => x.id === id).name;
  }

  get displayTrailerType() {
    return (id: string) => {
      if (id) {
        return this.getTrailerNameById(id);
      }
    };
  }

  showSecondaryDriverForm(): void {
    this.isDisplayedSecondaryDriverForm = true;
    this.formGroup.addControl(
      'secondaryDriver', new FormGroup({
        firstName: new FormControl(this.truck.secondaryDriver
          ? this.truck.secondaryDriver.firstName
          : ''),
        surname: new FormControl(
          this.truck.secondaryDriver
            ? this.truck.secondaryDriver.surname
            : ''),
        phoneNumber: new FormControl(
          this.truck.secondaryDriver
            ? this.truck.secondaryDriver.phoneNumber
            : ''),
        email: new FormControl(
          this.truck.secondaryDriver
            ? this.truck.secondaryDriver.email
            : '')
      })
    );
    this.setValidatorsSecondaryDriver();
  }

  hideSecondaryDriverForm(): void {
    this.isDisplayedSecondaryDriverForm = false;
    this.formGroup.removeControl('secondaryDriver');
  }

  filterTrailerTypes(): void {
    this.filteredTrailerTypes = this.trailerTypeId.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value)),
      takeUntil(this.onDestroy$)
    );
  }

  get truckNumber(): AbstractControl {
    return this.formGroup.get('truckNumber');
  }

  get trailerNumber(): AbstractControl {
    return this.formGroup.get('trailerNumber');
  }

  get trailerTypeId(): AbstractControl {
    return this.formGroup.get('trailerTypeId');
  }

  // Principal driver
  private get principalDriver(): AbstractControl {
    return this.formGroup.get('principalDriver');
  }

  get principalDriverFirstName(): AbstractControl {
    return this.principalDriver.get('firstName');
  }

  get principalDriverSurname(): AbstractControl {
    return this.principalDriver.get('surname');
  }

  get principalDriverPhoneNumber(): AbstractControl {
    return this.principalDriver.get('phoneNumber');
  }

  get principalDriverEmail(): AbstractControl {
    return this.principalDriver.get('email');
  }

  // Secondary driver
  private get secondaryDriver(): AbstractControl {
    return this.formGroup.get('secondaryDriver');
  }

  get secondaryDriverFirstName(): AbstractControl {
    return this.secondaryDriver.get('firstName');
  }

  get secondaryDriverSurname(): AbstractControl {
    return this.secondaryDriver.get('surname');
  }

  get secondaryDriverPhoneNumber(): AbstractControl {
    return this.secondaryDriver.get('phoneNumber');
  }

  get secondaryDriverEmail(): AbstractControl {
    return this.secondaryDriver.get('email');
  }

  private filter(value: any): TrailerType[] {
    let filterValue = '';
    if (typeof (value) === 'object') {
      filterValue = value.name.toLowerCase();
    } else {
      filterValue = value.toLowerCase();
    }
    return this.trailerTypes.filter(trailerType => trailerType.name.toLowerCase().includes(filterValue));
  }
}
