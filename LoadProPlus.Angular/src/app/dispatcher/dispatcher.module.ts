import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Custom modules
import { DispatcherRoutingModule } from './dispatcher-routing.module';
import { DispatcherMaterialModule } from './dispatcher-material.module';
import { SharedModule } from '@shared';

// Components
import {
  DashboardComponent,
  TrucksListComponent
} from './pages';

import {
  EditTruckComponent,
  CreateTruckComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    DispatcherRoutingModule,
    DispatcherMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    DashboardComponent,
    TrucksListComponent,
    EditTruckComponent,
    CreateTruckComponent
  ],
  entryComponents: [
    EditTruckComponent,
    CreateTruckComponent
  ]
})
export class DispatcherModule { }
