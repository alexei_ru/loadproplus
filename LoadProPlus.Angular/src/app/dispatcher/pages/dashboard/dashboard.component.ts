import { LoadsSignalRService } from './../../../core/services/loads-signalr.service';
import { LoadsService, Load } from '@core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import { DataTableComponent, PageView, PagedResult } from '@core';
import { Observable } from 'rxjs';
import { NgOnDestroy, SnackBarService, ConfirmationDialogComponent } from '@shared';

@Component({
  selector: 'app-rt-loads-list',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [NgOnDestroy]
})

export class DashboardComponent extends DataTableComponent {

  displayedColumns: string[] = [
    'isNew',
    'pickUpDateTime',
    'pickUpLocation',
    'deliveryDateTime',
    'deliveryLocation',
    'trailerType',
    'commodity',
    'weight',
    'customerRate',
    'loadStatus'];

  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    private _loadsService: LoadsService,
    onDestroy$: NgOnDestroy,
    private loadsSignalRService: LoadsSignalRService
  ) {
    super(onDestroy$);
    this.loadsSignalRService.startConnection();
    this.loadsSignalRService.loadBroadcast().subscribe((load) => {
      this.addUpdateLoad(load);
    });
  }

  addUpdateLoad(load: Load) {
    const existingLoad = this.dataSource.data.filter((f: Load) => f.id === load.id)[0];

    load.isNew = true;

    if (existingLoad) {
      this.dataSource.data.splice(this.dataSource.data.indexOf(existingLoad), 1);
    }

    this.prependItem(load);
  }

  fetchData(vm: PageView): Observable<PagedResult<any>> {
    return this._loadsService.getPagedAll(vm);
  }
}
