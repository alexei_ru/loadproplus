import { MatDialog, MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CreateTruckComponent, EditTruckComponent } from '../../components';
import {
  DataTableComponent,
  PageView,
  PagedResult,
  TrucksService,
  Truck
} from '@core';
import { NgOnDestroy, SnackBarService, ConfirmationDialogComponent } from '@shared';


@Component({
  selector: 'app-trucks-list',
  templateUrl: './trucks-list.component.html',
  styleUrls: ['./trucks-list.component.scss'],
  providers: [NgOnDestroy]
})
export class TrucksListComponent extends DataTableComponent {

  displayedColumns: string[] = ['truckNumber', 'trailerNumber', 'trailerType', 'principalDriver', 'secondaryDriver', 'actions'];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    private dialog: MatDialog,
    private trucksService: TrucksService,
    onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) {
    super(onDestroy$);
  }

  fetchData(vm: PageView): Observable<PagedResult<Truck>> {
    return this.trucksService.getPaged(vm);
  }

  onClickAdd(): void {
    const dialogRef = this.dialog.open(CreateTruckComponent, {
      width: '850px'
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this.snackBarService.openSuccess('Truck created successfully');
        this.appendItem(response.result);
      }
    });
  }

  onClickDelete(id: string): void {
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        message: 'Are you sure you want to delete this truck?'
      }
    });

    this.confirmationDialog.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(result => {
      if (result) {
        this.trucksService.delete(id).subscribe(() => {
          this.snackBarService.openSuccess('Truck deleted successfully');
          this.deleteItem(id, 'id');
        }, () => {
          this.snackBarService.openError('Truck deletion failed');
        });
      }
    });
  }

  onClickEdit(id: string): void {
    const dialogRef = this.dialog.open(EditTruckComponent, {
      width: '850px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this.snackBarService.openSuccess('Truck edited successfully');
        this.refresh.next();
      }
    });
  }
}
