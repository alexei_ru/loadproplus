import { AuthenticationService } from './../services';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authenticationService.userHasPermission(route.data.permission)) {
      return true;
    } else {
      this.router.navigateByUrl('/', { queryParams: { returnUrl: state.url }});
      return false;
    }
  }
}
