import { AuthenticationService } from './../services';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class NotAuthenticatedGuard implements CanActivate {

  constructor(private router: Router, private authenticationService: AuthenticationService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authenticationService.userIsAuthenticated) {
      this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
      return false;
    } else {
      return true;
    }
  }
}
