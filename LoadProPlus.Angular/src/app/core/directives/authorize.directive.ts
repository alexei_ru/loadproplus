import { Directive, OnInit, OnDestroy, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from '../services';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { takeWhile, filter, map } from 'rxjs/operators';

@Directive({
  selector: '[appAuthorize]'
})
export class AuthorizeDirective implements OnInit, OnDestroy {

  private hasView = false;

  private functionality = new BehaviorSubject<string>(null);
  private functionality$: Observable<string>;

  private isAlive = true;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authService: AuthenticationService
  ) {
    this.functionality$ = this.functionality.asObservable();
  }

  @Input() set appAuthorize(functionality: string) {
    this.functionality.next(functionality);
  }

  ngOnInit(): void {
    combineLatest([this.functionality$, this.authService.accessToken])
      .pipe(
        takeWhile(() => this.isAlive),
        filter(([functionality, _]) => functionality != null),
        map(([functionality, _]) => functionality)
      )
      .subscribe((functionality) => {
        this.toggleEmbeddedView(this.checkPermissions(functionality));
      });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  private checkPermissions(functionality: string): boolean {
    return this.authService.userHasPermission(functionality);
  }

  private createEmbeddedView() {
    this.viewContainer.createEmbeddedView(this.templateRef);
    this.hasView = true;
  }

  private clearEmbeddedView() {
    this.viewContainer.clear();
    this.hasView = false;
  }

  private toggleEmbeddedView(condition: boolean) {
    if (condition && !this.hasView) {
      this.createEmbeddedView();
    } else if (!condition && this.hasView) {
      this.clearEmbeddedView();
    }
  }

}
