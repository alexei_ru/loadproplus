import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidebarService } from '../../services';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(
    private sidebarService: SidebarService
  ) {
  }

  ngOnInit() {
    this.sidebarService.toggleButtonIsCalled$.subscribe(() => {
      this.sidenav.toggle();
    });
  }
}
