import { Component } from '@angular/core';
import { PageView, PagedResult } from '../../models';
import { Observable } from 'rxjs';
import { BrokerCompaniesService } from '../../services';
import { BrokerCompany } from '../../models';
import { DataTableComponent } from '../../components';
import { NgOnDestroy } from '@shared';

@Component({
  selector: 'app-broker-companies-list',
  templateUrl: './broker-companies-list.component.html',
  styleUrls: ['./broker-companies-list.component.scss'],
  providers: [NgOnDestroy]
})
export class BrokerCompaniesListComponent extends DataTableComponent {

  displayedColumns: string[] = ['mcNumber', 'companyName', 'mainAddress', 'zipCode', 'primaryContactName', 'fax', 'dotNumber'];

  constructor(
    onDestroy$: NgOnDestroy,
    private brokerCompaniesService: BrokerCompaniesService
  ) {
    super(onDestroy$);
  }

  fetchData(vm: PageView): Observable<PagedResult<BrokerCompany>> {
    return this.brokerCompaniesService.getPaged(vm);
  }
}
