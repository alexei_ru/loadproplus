import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CompaniesService, UsersService, AuthenticationService } from '../../services';
import { Company } from '../../models';
import { NgOnDestroy, SnackBarService } from '@shared';
import { takeUntil } from 'rxjs/operators';
import {
  checkUniqueCompanyUsernameValidator,
  checkUniqueUserEmailValidator,
  checkUniqueCompanyMcNumberValidator
} from '../../validators';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss'],
  providers: [NgOnDestroy]
})
export class CompanyProfileComponent implements OnInit {

  formGroup: FormGroup;
  isLoading = true;

  constructor(
    private companiesService: CompaniesService,
    private authenticationService: AuthenticationService,
    private onDestroy$: NgOnDestroy,
    private usersService: UsersService,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.initializeForm();
    this.companiesService.getById(this.authenticationService.userId).pipe(
      takeUntil(this.onDestroy$)
    )
    .subscribe((company: Company) => {
      this.formGroup.patchValue(company);
      this.isLoading = false;
    });
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.maxLength(50)],
        checkUniqueCompanyUsernameValidator(this.companiesService, this.authenticationService.userId)
      ),
      email: new FormControl('',
        [Validators.required, Validators.maxLength(254)],
        checkUniqueUserEmailValidator(this.usersService, this.authenticationService.userId)
      ),
      mcNumber: new FormControl('',
        [Validators.required, Validators.maxLength(70)],
        checkUniqueCompanyMcNumberValidator(this.companiesService, this.authenticationService.userId)
      ),
      companyName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      mainAddress: new FormControl('', [Validators.required, Validators.maxLength(200)]),
      zipCode: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      primaryContactName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      fax: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      dotNumber: new FormControl('', [Validators.required, Validators.maxLength(70)]),
      password: new FormControl('', Validators.maxLength(40)),
      confirmPassword: new FormControl('', Validators.maxLength(40))
    });
    this.formGroup.disable();
  }

  public triggerDisableForm(): void {
    if (this.formGroup.disabled) {
      this.formGroup.enable();
    } else {
      this.formGroup.disable();
    }
  }

  public onSubmit(): void {
    this.isLoading = true;
    this.companiesService.update(this.authenticationService.userId, this.formGroup.getRawValue()).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.formGroup.disable();
      this.snackBarService.openSuccess('Profile updated successfully');
      this.isLoading = false;
    }, () => {
      this.snackBarService.openError('Profile update failed');
    });
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

  get companyName(): AbstractControl {
    return this.formGroup.get('companyName');
  }

  get dotNumber(): AbstractControl {
    return this.formGroup.get('dotNumber');
  }

  get fax(): AbstractControl {
    return this.formGroup.get('fax');
  }

  get mainAddress(): AbstractControl {
    return this.formGroup.get('mainAddress');
  }

  get mcNumber(): AbstractControl {
    return this.formGroup.get('mcNumber');
  }

  get primaryContactName(): AbstractControl {
    return this.formGroup.get('primaryContactName');
  }

  get zipCode(): AbstractControl {
    return this.formGroup.get('zipCode');
  }
}
