import { Component } from '@angular/core';
import { DataTableComponent } from '../../components';
import { DispatcherCompaniesService } from '../../services';
import { Observable } from 'rxjs';
import { DispatcherCompany, PagedResult, PageView } from '../../models';
import { NgOnDestroy } from '@shared';

@Component({
  selector: 'app-dispatcher-companies-list',
  templateUrl: './dispatcher-companies-list.component.html',
  styleUrls: ['./dispatcher-companies-list.component.scss'],
  providers: [NgOnDestroy]
})
export class DispatcherCompaniesListComponent extends DataTableComponent {

  displayedColumns: string[] = ['mcNumber', 'companyName', 'mainAddress', 'zipCode', 'primaryContactName', 'fax', 'dotNumber'];

  constructor(
    onDestroy$: NgOnDestroy,
    private dispatcherCompaniesService: DispatcherCompaniesService
  ) {
    super(onDestroy$);
  }

  fetchData(vm: PageView): Observable<PagedResult<DispatcherCompany>> {
    return this.dispatcherCompaniesService.getPaged(vm);
  }
}
