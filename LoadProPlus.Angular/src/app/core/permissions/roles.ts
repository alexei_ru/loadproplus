export class Roles {
  public static readonly Administrator: string = 'Administrator';
  public static readonly BrokerCompany: string = 'BrokerCompany';
  public static readonly DispatcherCompany: string = 'DispatcherCompany';
  public static readonly Broker: string = 'Broker';
  public static readonly Dispatcher: string = 'Dispatcher';
}
