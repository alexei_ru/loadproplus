import { Roles } from './roles';

// Description is used with *appAuthorize directive
// Description is used in ...routing.module.ts
// Example of descrption 'Broker', 'ReadBrokerDetails', 'CreateTruck'...

export const permissionsTable = [
  {
    description: 'All',
    roles: [Roles.Administrator, Roles.Broker, Roles.BrokerCompany, Roles.Dispatcher, Roles.DispatcherCompany]
  },
  {
    description: 'Administrator',
    roles: [Roles.Administrator]
  },
  {
    description: 'Broker',
    roles: [Roles.Broker]
  },
  {
    description: 'BrokerCompany',
    roles: [Roles.BrokerCompany]
  },
  {
    description: 'Dispatcher',
    roles: [Roles.Dispatcher]
  },
  {
    description: 'DispatcherCompany',
    roles: [Roles.DispatcherCompany]
  },
  {
    description: 'ViewBrokerCompanies',
    roles: [Roles.DispatcherCompany, Roles.Dispatcher]
  },
  {
    description: 'ViewDispatcherCompanies',
    roles: [Roles.BrokerCompany, Roles.Broker]
  },
  {
    description: 'ViewCompanyProfile',
    roles: [Roles.BrokerCompany, Roles.DispatcherCompany]
  }
];
