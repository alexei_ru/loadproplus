import { EmployeesService } from '@core';
import { FormControl } from '@angular/forms';
import { timer } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

export const checkUniqueUsernameByCompanyIdValidator = (
  companyId: string,
  employeesService: EmployeesService,
  employeeId: string = '',
  time = 500
) => {
  return (input: FormControl) => {
    return timer(time).pipe(
      switchMap(() => employeesService.checkUniqueUsernameByCompanyId(companyId, employeeId, input.value)),
      map(response => {
        return response === false ? null : { notUnique: true };
      })
    );
  };
};
