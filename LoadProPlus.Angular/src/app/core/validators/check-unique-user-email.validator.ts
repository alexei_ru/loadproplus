import { UsersService } from '@core';
import { FormControl } from '@angular/forms';
import { timer } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

export const checkUniqueUserEmailValidator = (
  usersService: UsersService,
  userId: string = '',
  time = 500
) => {
  return (input: FormControl) => {
    return timer(time).pipe(
      switchMap(() => usersService.checkUniqueEmail(userId, input.value)),
      map(response => {
        return response === false ? null : { notUnique: true };
      })
    );
  };
};
