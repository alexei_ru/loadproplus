import { CompaniesService } from '@core';
import { FormControl } from '@angular/forms';
import { timer } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

export const checkUniqueCompanyUsernameValidator = (
  companiesService: CompaniesService,
  companyId: string = '',
  time = 500
) => {
  return (input: FormControl) => {
    return timer(time).pipe(
      switchMap(() => companiesService.checkUniqueUsername(companyId, input.value)),
      map(response => {
        return response === false ? null : { notUnique: true };
      })
    );
  };
};
