import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class SidebarService {
  private source = new Subject();
  toggleButtonIsCalled$ = this.source.asObservable();

  toggle(): void {
    this.source.next();
  }
}
