import { Dispatcher } from '../../models';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseApiService } from './base-api.service';

@Injectable()
export class DispatchersService extends BaseApiService<Dispatcher> {

  constructor(protected http: HttpClient) {
    super(http, 'api/dispatchers');
  }

}
