import { HttpClient } from '@angular/common/http';
import { State } from '../../models';
import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';

@Injectable()
export class StatesService extends BaseApiService<State> {

  constructor(protected http: HttpClient) {
    super(http, 'api/locations/states');
  }

}
