import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DispatcherCompany } from '../../models';
import { BaseApiService } from './base-api.service';

@Injectable()
export class DispatcherCompaniesService extends BaseApiService<DispatcherCompany> {

  constructor(protected http: HttpClient) {
    super(http, 'api/dispatcher-companies');
  }

}
