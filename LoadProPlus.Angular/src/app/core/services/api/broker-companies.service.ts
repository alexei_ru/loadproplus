import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BrokerCompany } from '../../models';
import { BaseApiService } from './base-api.service';

@Injectable()
export class BrokerCompaniesService extends BaseApiService<BrokerCompany> {

  constructor(protected http: HttpClient) {
    super(http, 'api/broker-companies');
  }

}
