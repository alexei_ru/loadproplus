import { PagedResult, PageView } from '@core';
import { Load, LoadStatus } from '../../models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';
import { Observable } from 'rxjs';
import { EditLoadModel } from '@broker';

@Injectable()
export class LoadsService extends BaseApiService<Load> {

  constructor(protected http: HttpClient) {
    super(http, 'api/loads');
  }

  getLoadStatuses(): Observable<LoadStatus[]> {
    return this.http.get<LoadStatus[]>(`api/loads/statuses`);
  }

  editLoad(loadId: string, load: EditLoadModel): Observable<void> {
    return this.http.put<void>(`api/loads/${loadId}`, load);
  }

  getPagedAll(tableParams?: PageView): Observable<PagedResult<Load>> {
    const params = tableParams.toSearchParams();
    return this.http.get<PagedResult<Load>>('api/loads/all', { params });
  }
}
