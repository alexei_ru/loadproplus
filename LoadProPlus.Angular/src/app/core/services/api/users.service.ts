import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) {

  }

  checkUniqueEmail(userId: string, email: string): Observable<boolean> {
    return this.http.get<boolean>('api/users/unique/email', {
      params: {
        email: email,
        userId: userId
      }
    });
  }
}
