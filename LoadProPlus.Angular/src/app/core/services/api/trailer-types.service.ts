import { TrailerType } from '../../models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';

@Injectable()
export class TrailerTypesService extends BaseApiService<TrailerType> {

  constructor(protected http: HttpClient) {
    super(http, 'api/trailer-types');
  }

}
