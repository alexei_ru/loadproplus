import { Broker } from '../../models';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseApiService } from './base-api.service';

@Injectable()
export class BrokersService extends BaseApiService<Broker> {

  constructor(protected http: HttpClient) {
    super(http, 'api/brokers');
  }

}
