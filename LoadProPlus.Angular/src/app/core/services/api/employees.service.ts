import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class EmployeesService {

  constructor(private http: HttpClient) {

  }

  checkUniqueUsernameByCompanyId(companyId: string, employeeId: string, username: string): Observable<boolean> {
    return this.http.get<boolean>('api/employees/unique/username', {
      params: {
        username: username,
        companyId: companyId,
        employeeId: employeeId
      }
    });
  }
}
