import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PagedResult, PageView } from '../../models';

export abstract class BaseApiService<T> {

  constructor(
    protected http: HttpClient,
    protected baseUrl: string
  ) {}

  create(t: T): Observable<T> {
    return this.http.post<T>(this.baseUrl, t);
  }

  update(id: string, t: T): Observable<void> {
    return this.http.put<void>(this.baseUrl + '/' + id, t);
  }

  getById(id: string): Observable<T> {
    return this.http.get<T>(this.baseUrl + '/' + id);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl + '/' + id);
  }

  getPaged(tableParams?: PageView): Observable<PagedResult<T>> {
    const params = tableParams.toSearchParams();
    return this.http.get<PagedResult<T>>(this.baseUrl, { params });
  }

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(this.baseUrl);
  }
}
