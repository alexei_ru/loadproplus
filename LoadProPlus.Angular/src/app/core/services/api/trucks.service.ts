import { PageView } from '@core';
import { Truck, PagedResult } from '../../models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';
import { Observable } from 'rxjs';

@Injectable()
export class TrucksService extends BaseApiService<Truck> {

  constructor(protected http: HttpClient) {
    super(http, 'api/trucks');
  }

  getPagedAll(tableParams?: PageView): Observable<PagedResult<Truck>> {
    const params = tableParams.toSearchParams();
    return this.http.get<PagedResult<Truck>>('api/trucks/all', { params });
  }
}
