import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Company } from 'app/core/models';

@Injectable()
export class CompaniesService {

  constructor(private http: HttpClient) {

  }

  checkUniqueMcNumber(companyId: string, mcNumber: string): Observable<boolean> {
    return this.http.get<boolean>('api/companies/unique/mc-number', {
      params: {
        companyId: companyId,
        mcNumber: mcNumber
      }
    });
  }

  checkUniqueUsername(companyId: string, username: string): Observable<boolean> {
    return this.http.get<boolean>('api/companies/unique/username', {
      params: {
        companyId: companyId,
        username: username
      }
    });
  }

  getById(id: string): Observable<Company> {
    return this.http.get<Company>(`api/companies/${id}`);
  }

  update(id: string, company: Company): Observable<void> {
    return this.http.put<void>(`api/companies/${id}`, company);
  }
}
