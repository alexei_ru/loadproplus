import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, finalize } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { Tokens, Company, Employee } from '../../models';
import { permissionsTable } from '../../permissions/permissions-table';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  accessTokenSubject: BehaviorSubject<string>;
  accessToken: Observable<string>;
  role: string;
  userId: string;

  constructor(private httpClient: HttpClient) {
    this.accessTokenSubject = new BehaviorSubject<string>(localStorage.getItem('access-token'));
    this.accessToken = this.accessTokenSubject.asObservable();

    this.accessToken.subscribe(accessToken => {
      if (accessToken) {
        const decodedToken = this.getDecodedAccessToken(accessToken);
        this.userId = decodedToken.nameid;
      }
    });
  }

  logout() {
    return this.httpClient.post('api/accounts/logout', this.getTokens()).pipe(
      finalize(() => {
        this.clearLocalStorage();
      })
    );
  }

  refreshToken(): Observable<Tokens> {
    return this.httpClient.put<Tokens>('api/accounts/refresh-token', this.getTokens()).pipe(
      tap((response: any) => {
        localStorage.setItem('access-token', response.accessToken);
        localStorage.setItem('refresh-token', response.refreshToken);
      })
    );
  }

  userHasPermission(functionality: string): boolean {
    const permission = permissionsTable.find((p) => p.description === functionality);
    if (permission) {
      this.accessToken.subscribe(accessToken => {
        if (accessToken) {
          const decodedToken = this.getDecodedAccessToken(accessToken);
          this.role = decodedToken.role;
        }
      });
      return this.role && permission.roles.includes(this.role);
    } else {
      console.error(`Permission ${functionality} not found`);
      return false;
    }
  }

  get userIsAuthenticated(): boolean {
    return localStorage.getItem('access-token') !== null;
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  loginCompany(company: Company): Observable<any> {
    return this.httpClient.post('api/signin/company', company).pipe(
      tap((tokens: Tokens) => {
        this.setTokens(tokens);
      })
    );
  }

  loginEmployee(employee: Employee): Observable<any> {
    return this.httpClient.post('api/signin/employee', employee).pipe(
      tap((tokens: Tokens) => {
        this.setTokens(tokens);
      })
    );
  }

  clearLocalStorage(): void {
    localStorage.removeItem('access-token');
    localStorage.removeItem('refresh-token');
    this.role = null;
    this.accessTokenSubject.next(null);
  }

  setTokens(tokens: Tokens): void {
    localStorage.setItem('access-token', tokens.accessToken);
    localStorage.setItem('refresh-token', tokens.refreshToken);
    const decodedToken = this.getDecodedAccessToken(tokens.accessToken);
    this.role = decodedToken.role;
    this.accessTokenSubject.next(tokens.accessToken);
  }

  getTokens(): any {
    return {
      refreshToken: localStorage.getItem('refresh-token'),
      accessToken: localStorage.getItem('access-token')
    };
  }
}
