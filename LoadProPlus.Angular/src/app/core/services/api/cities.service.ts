import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { City } from '../../models';
import { Observable } from 'rxjs';
import { BaseApiService } from './base-api.service';

@Injectable()
export class CitiesService extends BaseApiService<City> {

  constructor(protected http: HttpClient) {
    super(http, 'api/locations/cities');
  }

  getAllByStateId(stateId): Observable<City[]> {
    return this.http.get<City[]>(`api/locations/cities/${stateId}`);
  }
}
