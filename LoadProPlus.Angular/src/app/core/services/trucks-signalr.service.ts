import * as SignalR from '@aspnet/signalr';
import { Observable, Subject } from 'rxjs';
import { Truck } from '@core';

export class TrucksSignalRService {
  private hubConnection: SignalR.HubConnection;
  private _truckReceived$: Subject<Truck>;

  constructor() {
    this._truckReceived$ = new Subject<Truck>();
  }

  public startConnection = () => {
    this.hubConnection = new SignalR.HubConnectionBuilder()
                            .withUrl('http://localhost:5000/trucks')
                            .build();
    this._startConnection();
    this.hubConnection.onclose(() => this.startConnection());
  }

  private _startConnection = () => {
    this.hubConnection
      .start();
      // .then(() => console.log('Connection started'))
      // .catch(err => console.log('Error while starting connection: ' + err));
  }

  public truckBroadcast(): Observable<Truck> {
    this.hubConnection.on('BroadcastTruck', (data) => {
      this._truckReceived$.next(data);
    });

    return this._truckReceived$.asObservable();
  }
}
