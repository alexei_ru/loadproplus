import * as SignalR from '@aspnet/signalr';
import { Observable, Subject } from 'rxjs';
import { Load } from '@core';

export class LoadsSignalRService {
  private hubConnection: SignalR.HubConnection;
  private loadReceived$: Subject<Load>;

  constructor() {
    this.loadReceived$ = new Subject<Load>();
  }

  public startConnection = () => {
    this.hubConnection = new SignalR.HubConnectionBuilder()
                            .withUrl('http://localhost:5000/loads')
                            .build();
    this._startConnection();
    this.hubConnection.onclose(() => this.startConnection());
  }

  private _startConnection = () => {
    this.hubConnection
      .start();
      // .then(() => console.log('Connection started'))
      // .catch(err => console.log('Error while starting connection: ' + err));
  }

  public loadBroadcast(): Observable<Load> {
    this.hubConnection.on('BroadcastLoad', (data) => {
      this.loadReceived$.next(data);
    });

    return this.loadReceived$.asObservable();
  }
}
