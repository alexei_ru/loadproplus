import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Custom modules
import { CoreMaterialModule } from './core-material.module';
import { SharedModule } from '@shared';

// Services
import {
  AuthenticationService,
  SidebarService,
  BrokerCompaniesService,
  DispatcherCompaniesService,
  CitiesService,
  StatesService,
  BrokersService,
  DispatchersService,
  TrucksService,
  TrailerTypesService,
  CompaniesService,
  UsersService,
  EmployeesService,
  LoadsSignalRService,
  LoadsService,
  TrucksSignalRService
} from './services';

// Interceptors
import { RefreshTokenInterceptor } from './interceptors';

// Directives
import { AuthorizeDirective } from './directives';

// Guards
import {
  RoleGuard,
  NotAuthenticatedGuard,
  AuthenticatedGuard
} from './guards';

// Components
import {
  HomeComponent,
  DispatcherCompaniesListComponent,
  BrokerCompaniesListComponent,
  LayoutComponent,
  CompanyProfileComponent
} from './pages';

import {
  DispatcherSidebarComponent,
  BrokerSidebarComponent,
  BrokerCompanySidebarComponent,
  DispatcherCompanySidebarComponent,
  HeaderComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    CoreMaterialModule,
    FlexLayoutModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    HomeComponent,
    HeaderComponent,
    AuthorizeDirective,
    DispatcherCompaniesListComponent,
    BrokerCompaniesListComponent,
    LayoutComponent,
    DispatcherCompanySidebarComponent,
    BrokerCompanySidebarComponent,
    BrokerSidebarComponent,
    DispatcherSidebarComponent,
    CompanyProfileComponent
  ],
  exports: [
    HomeComponent,
    HeaderComponent,
    DispatcherCompaniesListComponent,
    BrokerCompaniesListComponent
  ],
  providers: [
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true
    },
    CitiesService,
    SidebarService,
    RoleGuard,
    AuthenticatedGuard,
    NotAuthenticatedGuard,
    BrokerCompaniesService,
    DispatcherCompaniesService,
    StatesService,
    BrokersService,
    DispatchersService,
    TrucksService,
    TrailerTypesService,
    CompaniesService,
    UsersService,
    EmployeesService,
    LoadsService,
    LoadsSignalRService,
    TrucksSignalRService
  ]
})
export class CoreModule { }
