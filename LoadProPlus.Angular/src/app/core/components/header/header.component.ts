import { Roles } from './../../permissions/roles';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import { SidebarService, AuthenticationService } from '../../services';
import { Router } from '@angular/router';
import { SnackBarService, ConfirmationDialogComponent } from '@shared';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    private sidebarService: SidebarService,
    public authenticationService: AuthenticationService,
    private router: Router,
    private dialog: MatDialog,
    private snackBarService: SnackBarService
  ) { }

  onClickToggle(): void {
    this.sidebarService.toggle();
  }

  onClickLogout(): void {
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        message: 'Are you sure you want to log out?'
      }
    });

    this.confirmationDialog.afterClosed().subscribe((response) => {
      if (response) {
        this.authenticationService.logout().subscribe(() => {
          this.snackBarService.openSuccess('Logout successfully');
          this.router.navigateByUrl('/');
        }, () => {
          this.snackBarService.openError('Logout failed');
        });
      }
    });
  }

  get isAuthenticated(): boolean {
    return this.authenticationService.userIsAuthenticated;
  }

  displaySidebarToggleButton(): boolean {
    return this.router.url.startsWith('/dispatcher')
      || this.router.url.startsWith('/broker');
  }

  public navigateToPrincipalPage(): void {
    const userRole = this.authenticationService.role;
    switch (userRole) {
      case Roles.BrokerCompany : this.router.navigateByUrl('/broker-company'); break;
      case Roles.DispatcherCompany : this.router.navigateByUrl('/dispatcher-company'); break;
      case Roles.Dispatcher : this.router.navigateByUrl('/dispatcher'); break;
      case Roles.Broker: this.router.navigateByUrl('/broker'); break;
      default: this.router.navigateByUrl('/');
    }
  }

  public navigateToProfilePage(): void {
    const userRole = this.authenticationService.role;
    switch (userRole) {
      case Roles.BrokerCompany : this.router.navigateByUrl('/broker-company/profile'); break;
      case Roles.DispatcherCompany : this.router.navigateByUrl('/dispatcher-company/profile'); break;
      default: this.router.navigateByUrl('/');
    }
  }
}
