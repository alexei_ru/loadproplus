import { SidebarService } from '../../../services';
import { Component } from '@angular/core';

@Component({
  selector: 'app-dispatcher-sidebar',
  templateUrl: './dispatcher-sidebar.component.html',
  styleUrls: ['./dispatcher-sidebar.component.scss']
})
export class DispatcherSidebarComponent {
  constructor(private sidebarService: SidebarService) {

  }

  toggleSidebar(): void {
    this.sidebarService.toggle();
  }
}
