import { SidebarService } from '../../../services';
import { Component } from '@angular/core';

@Component({
  selector: 'app-broker-company-sidebar',
  templateUrl: './broker-company-sidebar.component.html',
  styleUrls: ['./broker-company-sidebar.component.scss']
})
export class BrokerCompanySidebarComponent {
  constructor(private sidebarService: SidebarService) {

  }

  toggleSidebar(): void {
    this.sidebarService.toggle();
  }
}
