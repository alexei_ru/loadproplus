import { SidebarService } from '../../../services';
import { Component } from '@angular/core';

@Component({
  selector: 'app-broker-sidebar',
  templateUrl: './broker-sidebar.component.html',
  styleUrls: ['./broker-sidebar.component.scss']
})
export class BrokerSidebarComponent {
  constructor(private sidebarService: SidebarService) {

  }

  toggleSidebar(): void {
    this.sidebarService.toggle();
  }
}
