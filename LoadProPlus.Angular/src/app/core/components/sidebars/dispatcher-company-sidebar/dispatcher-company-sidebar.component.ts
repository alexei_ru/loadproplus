import { SidebarService } from '../../../services';
import { Component } from '@angular/core';

@Component({
  selector: 'app-dispatcher-company-sidebar',
  templateUrl: './dispatcher-company-sidebar.component.html',
  styleUrls: ['./dispatcher-company-sidebar.component.scss']
})
export class DispatcherCompanySidebarComponent {
  constructor(private sidebarService: SidebarService) {

  }

  toggleSidebar(): void {
    this.sidebarService.toggle();
  }
}
