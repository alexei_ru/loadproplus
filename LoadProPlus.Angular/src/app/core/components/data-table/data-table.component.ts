import { ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Observable,  merge, ConnectableObservable, ReplaySubject } from 'rxjs';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { publish, debounceTime, takeUntil } from 'rxjs/operators';
import { PageView, PagedResult } from '../../models';
import { NgOnDestroy } from '@shared';

export abstract class DataTableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public dataSource = new MatTableDataSource<any>();

  public isLoading = true;
  public resultsLength = 0;
  public pageSize = 10;
  public pageSizeOptions = [10, 15, 20];

  protected PageView: PageView = new PageView();
  protected fetchDataEvent$: Observable<any>;
  protected sourceChanges: Observable<any>;
  protected refresh = new ReplaySubject<void>();

  abstract fetchData(vm: PageView): Observable<PagedResult<any>>;

  constructor(public onDestroy$: NgOnDestroy) {}

  ngOnInit(): void {
    const start$ = new Observable(o => { o.next({}); });
    this.fetchDataEvent$ = this.refresh.asObservable();

    this.sourceChanges = merge(
      this.paginator.page,
      this.fetchDataEvent$,
      start$
    ).pipe(
      debounceTime(200),
      publish(),
    );

    this.sourceChanges.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.loadData();
    });
  }

  ngAfterViewInit(): void {
    (<ConnectableObservable<any>>this.sourceChanges).connect();
  }

  fillPageData(): void {
    this.PageView.pageSize = this.paginator.pageSize;
    this.PageView.page = this.paginator.pageIndex + 1;
  }

  loadData(): void {
    this.isLoading = true;
    this.fillPageData();

    this.fetchData(this.PageView).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(data => {
      this.isLoading = false;
      this.resultsLength = data.totalCount;
      this.pageSize = data.pageSize;
      this.dataSource.data = data.result;
    }, () => {
      this.isLoading = false;
    });
  }

  appendItem(item: any): void {
    const pageSize = this.paginator.pageSize;
    const dataLength = this.dataSource.data.length;

    if (pageSize !== dataLength) {
      this.dataSource.data = [...this.dataSource.data, item];
    }

    // TO DO: disable sort after append

    this.resultsLength += 1;
    this.paginator.length += 1;
    this.paginator.lastPage();
  }

  prependItem(item: any): void {
    const newArray = this.dataSource.data.slice();
    newArray.unshift(item);

    this.dataSource.data = newArray;

    // TO DO: disable sort after append

    this.resultsLength += 1;
    this.paginator.length += 1;
  }

  deleteItem(value: string, key: string): void {
    const index = this.dataSource.data.findIndex(item => item[key] === value);
    this.dataSource.data = [...this.dataSource.data.slice(0, index), ...this.dataSource.data.slice(index + 1)];

    this.resultsLength = this.resultsLength - 1;
    this.paginator.length -= 1;

    // When deleted item is last in the table, pageIndex == numberOfPages
    // If pageIndex is different than numberOfPages - 1, we need to load data
    if (this.paginator.pageIndex === this.paginator.getNumberOfPages()) {
      this.paginator.previousPage();
    } else if (this.paginator.pageIndex !== this.paginator.getNumberOfPages() - 1) {
      this.loadData();
    }
  }
}
