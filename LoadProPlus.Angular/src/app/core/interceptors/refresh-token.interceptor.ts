import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../services';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, filter, take, finalize } from 'rxjs/operators';
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { SnackBarService } from '@shared';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private snackBarService: SnackBarService
  ) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = this.addAuthenticationToken(request);

    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          switch (error.status) {
            case 401: {
              return this.handle401(request, next);
            }
            case 400: {
              return this.handle400(error);
            }
            default: {
              return throwError('');
            }
          }
        }
      })
    );
  }

  handle401(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      this.tokenSubject.next(null);
      return this.authService.refreshToken()
        .pipe(switchMap(newToken => {
          if (newToken) {
            this.tokenSubject.next(newToken.accessToken);
            return next.handle(this.addAuthenticationToken(request));
          }
        }), catchError(() => {
          this.authService.clearLocalStorage();
          this.router.navigateByUrl('/login');
          this.snackBarService.openError('Authentication failed');
          return throwError('');
        }), finalize(() => {
          this.isRefreshingToken = false;
        }));
    } else {
      return this.tokenSubject
        .pipe(
          filter(token => token != null),
          take(1),
          switchMap(() => {
            return next.handle(this.addAuthenticationToken(request));
          })
        );
    }
  }

  handle400(error: any) {
    return throwError(error);
  }

  addAuthenticationToken(request: HttpRequest<any>) {
    const token = localStorage.getItem('access-token');

    return request = request.clone({
      setHeaders: {
        'Authorization': `Bearer ${token}`
      }
    });
  }
}

