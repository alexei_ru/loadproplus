export class Dispatcher {
  id: string;
  username: string;
  email: string;
  name: string;
  surname: string;
  phone: string;
  password: string;
}
