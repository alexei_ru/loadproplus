import { City, TrailerType } from '@core';
import { LoadStatus } from './load-status.model';

export class Load {
  id: string;
  pickUpDateTime: Date;
  pickUpLocation: City;
  pickUpId: string;
  deliveryDateTime: Date;
  deliveryLocation: City;
  deliveryId: string;
  trailerType: TrailerType;
  trailerTypeId: string;
  commodity: string;
  weight: string;
  customerRate: string;
  additionalInfo: string;
  loadStatus: LoadStatus;
  isNew: boolean;
}
