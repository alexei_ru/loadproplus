export interface DispatcherCompany {
  mcNumber: string;
  companyName: string;
  mainAddress: string;
  zipCode: string;
  primaryContactName: string;
  fax: string;
  dotNumber: string;
  username: string;
  email: string;
  password: string;
}
