import { State } from '@core';

export class City {
  id: string;
  name: string;
  county: string;
  latitude: number;
  longitude: number;
  state: State;
}
