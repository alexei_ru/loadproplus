import { Driver } from './driver.model';

export class Truck {
  id: string;
  truckNumber: string;
  trailerNumber: string;
  trailerType: string;
  trailerTypeId: string;
  principalDriver: Driver;
  secondaryDriver: Driver;
  isNew: boolean;
}
