export class Driver {
  firstName: string;
  surname: string;
  phoneNumber: string;
  email: string;
}
