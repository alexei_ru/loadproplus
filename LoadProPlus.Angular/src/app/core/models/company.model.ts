export class Company {
  public username: string;
  public email: string;
  public password: string;
  public mcNumber: string;
  public companyName: string;
  public mainAddress: string;
  public zipCode: string;
  public primaryContactName: string;
  public fax: string;
  public dotNumber: string;
}
