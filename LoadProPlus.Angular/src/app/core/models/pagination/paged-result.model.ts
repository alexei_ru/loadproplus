export class PagedResult<T> {
  totalPages: number;
  currentPage: number;
  totalCount: number;
  pageSize: number;
  result: T[];
}
