import { HttpParams } from '@angular/common/http';

export class PageView {
  page: number;
  pageSize: number;

  constructor(page: number = 1, pageSize: number = 10) {
    this.page = page;
    this.pageSize = pageSize;
  }

  public toSearchParams(): HttpParams {
    let parameters = new HttpParams();
    for (const key in this) {
      if (this[key] && key !== 'toSearchParams') {
        parameters = parameters.append(`${key}`, `${this[key]}`);
      }
    }
    return parameters;
  }
}
