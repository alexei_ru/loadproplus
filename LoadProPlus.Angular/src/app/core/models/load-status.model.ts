export class LoadStatus {
  id: string;
  name: string;
  description: string;
}
