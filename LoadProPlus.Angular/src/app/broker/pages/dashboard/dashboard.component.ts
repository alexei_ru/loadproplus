import { MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import {
  DataTableComponent,
  PageView,
  PagedResult,
  TrucksService,
  Truck,
  TrucksSignalRService
} from '@core';
import { NgOnDestroy, ConfirmationDialogComponent } from '@shared';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [NgOnDestroy]
})
export class DashboardComponent extends DataTableComponent {
  displayedColumns: string[] = ['isNew', 'truckNumber', 'trailerNumber', 'trailerType', 'principalDriver', 'secondaryDriver'];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    private trucksService: TrucksService,
    private trucksSignalrService: TrucksSignalRService,
    onDestroy$: NgOnDestroy,
  ) {
    super(onDestroy$);
    this.trucksSignalrService.startConnection();
    this.trucksSignalrService.truckBroadcast().subscribe((truck) => {
      this.addUpdateTruck(truck);
    });
  }

  addUpdateTruck(truck: Truck) {
    const existingLoad = this.dataSource.data.filter((f: Truck) => f.id === truck.id)[0];

    truck.isNew = true;

    if (existingLoad) {
      this.dataSource.data.splice(this.dataSource.data.indexOf(existingLoad), 1);
    }

    this.prependItem(truck);
  }

  fetchData(vm: PageView): Observable<PagedResult<Truck>> {
    return this.trucksService.getPagedAll(vm);
  }
}
