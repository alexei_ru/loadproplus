import { LoadsSignalRService } from './../../../core/services/loads-signalr.service';
import { LoadsService } from '@core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import { DataTableComponent, PageView, PagedResult } from '@core';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CreateLoadComponent, EditLoadComponent } from '../../components/';
import { NgOnDestroy, SnackBarService, ConfirmationDialogComponent } from '@shared';

@Component({
  selector: 'app-loads-list',
  templateUrl: './loads-list.component.html',
  styleUrls: ['./loads-list.component.scss'],
  providers: [NgOnDestroy]
})

export class LoadsListComponent extends DataTableComponent {

  displayedColumns: string[] = [
    'pickUpDateTime',
    'pickUpLocation',
    'deliveryDateTime',
    'deliveryLocation',
    'trailerType',
    'commodity',
    'weight',
    'customerRate',
    'loadStatus',
    'actions'];
    // 'additionalInfo'];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    private dialog: MatDialog,
    private _loadsService: LoadsService,
    onDestroy$: NgOnDestroy,
    private _snackBarService: SnackBarService,
    private loadsSignalRService: LoadsSignalRService
  ) {
    super(onDestroy$);
  }

  fetchData(vm: PageView): Observable<PagedResult<any>> {
    return this._loadsService.getPaged(vm);
  }

  onClickAdd(): void {
    const dialogRef = this.dialog.open(CreateLoadComponent, {
      width: '850px'
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this._snackBarService.openSuccess('Load added successfully');
        this.appendItem(response.result);
      }
    });
  }

  onClickDelete(id: string): void {
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        message: 'Are you sure you want to delete this load?'
      }
    });

    this.confirmationDialog.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(result => {
      if (result) {
        this._loadsService.delete(id).subscribe(() => {
          this._snackBarService.openSuccess('Load deleted successfully');
          this.deleteItem(id, 'id');
        }, () => {
          this._snackBarService.openError('Load deletion failed');
        });
      }
    });
  }

  onClickEdit(id: string): void {
    const dialogRef = this.dialog.open(EditLoadComponent, {
      width: '850px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(response => {
      if (response) {
        this._snackBarService.openSuccess('Load edited successfully');
        this.refresh.next();
      }
    });
  }
}
