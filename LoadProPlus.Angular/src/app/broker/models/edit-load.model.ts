import { LoadStatus } from '.';

export class EditLoadModel {
  pickUpDateTime: Date;
  pickUpId: string;
  deliveryDateTime: Date;
  deliveryId: string;
  trailerTypeId: string;
  commodity: string;
  weight: string;
  customerRate: string;
  additionalInfo: string;
  loadStatusId: string;
}
