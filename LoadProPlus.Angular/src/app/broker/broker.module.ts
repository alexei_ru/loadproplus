import { FlexLayoutModule } from '@angular/flex-layout';
import { LoadsService } from '@core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Custom modules
import { BrokerMaterialModule } from './broker-material.module';
import { BrokerRoutingModule } from './broker-routing.module';
import { SharedModule } from '@shared';

// Components
import { DashboardComponent } from './pages';
import { LoadsListComponent } from './pages/loads-list/loads-list.component';
import { CreateLoadComponent, EditLoadComponent } from './components';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BrokerMaterialModule,
    BrokerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  declarations: [
    DashboardComponent,
    CreateLoadComponent,
    EditLoadComponent,
    LoadsListComponent
  ],
  entryComponents: [
    EditLoadComponent
  ],
  providers: [LoadsService]
})
export class BrokerModule { }
