import {
  StatesService,
  CitiesService,
  State,
  City,
  TrailerTypesService,
  TrailerType
} from '@core';
import { Load } from '@core';
import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { SpinnerService, ValidatorHelper } from '@shared';
import { CreateLoadModel } from '../../models';
import { LoadsService } from '@core';


@Component({
  selector: 'app-create-load',
  templateUrl: './create-load.component.html',
  styleUrls: ['./create-load.component.scss']
})

export class CreateLoadComponent implements OnInit {
  formGroup: FormGroup;

  deliveryCities: City[];
  filteredDeliveryCities: City[];
  pickUpCities: City[];
  filteredPickUpCities: City[];

  filteredStates: State[];
  statesFilterPattern: string;

  states: State[];

  trailerTypes: TrailerType[];
  filteredTrailerTypes: TrailerType[];

  constructor(
    private trailerTypesService: TrailerTypesService,
    private citiesService: CitiesService,
    private spinnerService: SpinnerService,
    private loadsService: LoadsService,
    public dialogRef: MatDialogRef<CreateLoadComponent>,
    private statesService: StatesService
  ) {
  }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      pickUpDateTime: new FormControl('', [Validators.required]),
      pickUpState: new FormControl('', Validators.compose([Validators.required, ValidatorHelper.RequireAutocompleteObject])),
      pickUpCity: new FormControl(
        { value: '', disabled: true },
        Validators.compose([Validators.required, ValidatorHelper.RequireAutocompleteObject])),
      deliveryDateTime: new FormControl('', [Validators.required]),
      deliveryState: new FormControl('', Validators.compose([Validators.required, ValidatorHelper.RequireAutocompleteObject])),
      deliveryCity: new FormControl(
        { value: '', disabled: true },
        Validators.compose([Validators.required, ValidatorHelper.RequireAutocompleteObject])),
      trailerType: new FormControl('', Validators.compose([Validators.required, ValidatorHelper.RequireAutocompleteObject])),
      commodity: new FormControl('', [Validators.required]),
      weight: new FormControl('', [Validators.required, Validators.min(0.1)]),
      customerRate: new FormControl('', [Validators.required, Validators.min(0.1)]),
      additionalInfo: new FormControl('', [Validators.maxLength(255)])
    });

    this.trailerTypesService.getAll().subscribe((trailerTypes: TrailerType[]) => {
      this.trailerTypes = trailerTypes;
      this.filteredTrailerTypes = trailerTypes;
    });

    this.statesService.getAll().subscribe((states: State[]) => {
      this.states = states;
      this.filteredStates = states;
    });
  }

  onLoadAddSubmit() {
    const load: Load = this.formGroup.getRawValue();

    load.deliveryId = this.deliveryCity.value.id;
    load.pickUpId = this.pickUpCity.value.id;
    load.trailerTypeId = this.trailerType.value.id;

    this.loadsService.create(load).subscribe((createdLoad: Load) => {
      this.dialogRef.close({
        result: createdLoad
      });
    }, () => {
      console.error('Error');
    });
  }

  onPickUpCityChange(city) {
    if (typeof city === 'string' && this.pickUpCities) {
      const filterValue = city.toLowerCase();

      this.filteredPickUpCities = this.pickUpCities.filter(f => f.name.toLowerCase().indexOf(filterValue) === 0);
    }
  }

  onDeliveryCityChange(city) {
    if (typeof city === 'string' && this.deliveryCities) {
      const filterValue = city.toLowerCase();

      this.filteredDeliveryCities = this.deliveryCities.filter(f => f.name.toLowerCase().indexOf(filterValue) === 0);
    }
  }

  onTrailerTypeChange(trailerType) {
    if (typeof trailerType === 'string' && this.trailerTypes) {
      const filterTerm = trailerType.toLowerCase();

      this.filteredTrailerTypes = this.trailerTypes
        .filter(f => f.name.toLowerCase()
          .indexOf(filterTerm) >= 0);
    }
  }

  onStateChange(state) {
    if (this.states) {
      const filterValue = state.toLowerCase();

      this.filteredStates = this.states.filter(s => s.name.toLowerCase().indexOf(filterValue) === 0);
    }
  }

  onPickUpStateChange(state) {
    if (typeof state === 'string') {
      this.onStateChange(state);
    } else {
      this.pickUpCity.disable();
      this.spinnerService.show('pickUpCitySpinner');
      this.citiesService.getAllByStateId(state.id).subscribe({
        next: (cities: City[]) => {
          cities.sort((a, b) => a.name.localeCompare(b.name));
          this.pickUpCities = cities;
          this.filteredPickUpCities = cities;
          this.pickUpCity.enable();
        },
        complete: () => {
          this.spinnerService.hide('pickUpCitySpinner');
        }
      });
    }
  }

  onDeliveryStateChange(state) {
    if (typeof state === 'string') {
      this.onStateChange(state);
    } else {
      this.deliveryCity.disable();
      this.spinnerService.show('deliveryCitySpinner');
      this.citiesService.getAllByStateId(state.id).subscribe({
        next: (cities: City[]) => {
          cities.sort((a, b) => a.name.localeCompare(b.name));
          this.deliveryCities = cities;
          this.filteredDeliveryCities = cities;
          this.deliveryCity.enable();
        },
        complete: () => {
          this.spinnerService.hide('deliveryCitySpinner');
        }
      });
    }
  }

  displayState(state): string {
    return state ? state.name : state;
  }

  displayCity(city): string {
    return city ? city.name : city;
  }

  displayTrailerType(trailerType): string {
    return trailerType ? trailerType.name : trailerType;
  }

  refreshFilteredStates() {
    this.filteredStates = this.states;
  }

  refreshFilteredCities() {
    this.filteredDeliveryCities = this.deliveryCities;
    this.filteredPickUpCities = this.pickUpCities;
  }

  refreshFilteredTrailerTypes() {
    this.filteredTrailerTypes = this.trailerTypes;
  }

  // FromControl gets
  get pickUpDateTime(): AbstractControl {
    return this.formGroup.get('pickUpDateTime');
  }
  get pickUpState(): AbstractControl {
    return this.formGroup.get('pickUpState');
  }
  get pickUpCity(): AbstractControl {
    return this.formGroup.get('pickUpCity');
  }
  get deliveryDateTime(): AbstractControl {
    return this.formGroup.get('deliveryDateTime');
  }
  get deliveryState(): AbstractControl {
    return this.formGroup.get('deliveryState');
  }
  get deliveryCity(): AbstractControl {
    return this.formGroup.get('deliveryCity');
  }
  get trailerType(): AbstractControl {
    return this.formGroup.get('trailerType');
  }
  get commodity(): AbstractControl {
    return this.formGroup.get('commodity');
  }
  get weight(): AbstractControl {
    return this.formGroup.get('weight');
  }
  get customerRate(): AbstractControl {
    return this.formGroup.get('customerRate');
  }
  get additionalInfo(): AbstractControl {
    return this.formGroup.get('additionalInfo');
  }
}
