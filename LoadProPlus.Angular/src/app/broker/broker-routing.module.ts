import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Custom modules
import {
  RoleGuard,
  DispatcherCompaniesListComponent,
  LayoutComponent
} from '@core';

// Components
import {
  DashboardComponent,
  LoadsListComponent
} from './pages';

import {
  CreateLoadComponent
} from './components';

const routes: Routes = [
  {
    path: 'broker',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'Broker'
        }
      },
      {
        path: 'loads-list',
        component: LoadsListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'Broker'
        }
      },
      {
        path: 'create-load',
        component: CreateLoadComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'Broker'
        }
      },
      {
        path: 'dispatcher-companies',
        component: DispatcherCompaniesListComponent,
        canActivate: [RoleGuard],
        data: {
          permission: 'ViewDispatcherCompanies'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BrokerRoutingModule {

}
