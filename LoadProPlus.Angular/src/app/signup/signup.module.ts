import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Custom modules
import { SharedModule } from '@shared';
import { SignupMaterialModule } from './signup-material.module';
import { SignupRoutingModule } from './signup-routing.module';

// Components
import { CompanyRegisterFormComponent } from './components';
import { SignupComponent } from './signup.component';
import {
  RegisterComponent,
  RegisterBrokerCompanyComponent,
  RegisterDispatcherCompanyComponent
} from './pages';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    SignupMaterialModule,
    SignupRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    SignupComponent,
    RegisterComponent,
    RegisterBrokerCompanyComponent,
    RegisterDispatcherCompanyComponent,
    CompanyRegisterFormComponent
  ]
})
export class SignupModule { }
