import { Component, OnInit, Output } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import {
  CompaniesService,
  UsersService,
  checkUniqueUserEmailValidator,
  checkUniqueCompanyUsernameValidator,
  checkUniqueCompanyMcNumberValidator
} from '@core';

@Component({
  selector: 'app-company-register-form',
  templateUrl: './company-register-form.component.html',
  styleUrls: ['./company-register-form.component.scss']
})
export class CompanyRegisterFormComponent implements OnInit {

  @Output()
  public submitForm = new Subject();

  formGroup: FormGroup;

  constructor(
    private dialog: MatDialog,
    private companiesService: CompaniesService,
    private usersService: UsersService
  ) {
  }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm(): void {
    this.formGroup = new FormGroup({
      username: new FormControl('',
        [Validators.required, Validators.maxLength(50)],
        checkUniqueCompanyUsernameValidator(this.companiesService)
      ),
      email: new FormControl('',
        [Validators.required, Validators.maxLength(254)],
        checkUniqueUserEmailValidator(this.usersService)
      ),
      password: new FormControl('', [Validators.required, Validators.maxLength(40)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(40)]),
      companyName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      dotNumber: new FormControl('', [Validators.required, Validators.maxLength(70)]),
      fax: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      mainAddress: new FormControl('', [Validators.required, Validators.maxLength(200)]),
      mcNumber: new FormControl('',
        [Validators.required, Validators.maxLength(70)],
        checkUniqueCompanyMcNumberValidator(this.companiesService)
      ),
      primaryContactName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      zipCode: new FormControl('', [Validators.required, Validators.maxLength(15)])
    });
  }

  onSubmit(): void {
    this.submitForm.next(this.formGroup.getRawValue());
  }

  get username(): AbstractControl {
    return this.formGroup.get('username');
  }

  get email(): AbstractControl {
    return this.formGroup.get('email');
  }

  get password(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

  get companyName(): AbstractControl {
    return this.formGroup.get('companyName');
  }

  get dotNumber(): AbstractControl {
    return this.formGroup.get('dotNumber');
  }

  get fax(): AbstractControl {
    return this.formGroup.get('fax');
  }

  get mainAddress(): AbstractControl {
    return this.formGroup.get('mainAddress');
  }

  get mcNumber(): AbstractControl {
    return this.formGroup.get('mcNumber');
  }

  get primaryContactName(): AbstractControl {
    return this.formGroup.get('primaryContactName');
  }

  get zipCode(): AbstractControl {
    return this.formGroup.get('zipCode');
  }
}
