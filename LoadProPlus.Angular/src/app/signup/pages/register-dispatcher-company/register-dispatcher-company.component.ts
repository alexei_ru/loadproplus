import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {
  DispatcherCompaniesService,
  DispatcherCompany
} from '@core';
import {
  NgOnDestroy,
  SnackBarService,
  ConfirmationDialogComponent,
  CanDeactivateComponent
} from '@shared';

@Component({
  selector: 'app-register-dispatcher-company',
  templateUrl: './register-dispatcher-company.component.html',
  styleUrls: ['./register-dispatcher-company.component.scss'],
  providers: [NgOnDestroy]
})
export class RegisterDispatcherCompanyComponent extends CanDeactivateComponent {

  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;
  isSubmitted = false;
  isLoading = false;

  constructor(
    private dispatcherCompaniesService: DispatcherCompaniesService,
    private router: Router,
    private dialog: MatDialog,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) {
    super();
  }

  onSubmit(company: DispatcherCompany): void {
    this.isLoading = true;
    this.dispatcherCompaniesService.create(company).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.isSubmitted = true;
      this.isLoading = false;
      this.snackBarService.openSuccess('Dispatcher registration successfully');
      this.router.navigateByUrl('/');
    }, () => {
      this.isLoading = false;
      this.snackBarService.openError('Dispatcher registration failed');
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.isSubmitted) {
      this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          message: 'Are you sure you want to leave this page?'
        }
      });
      return this.confirmationDialog.afterClosed()
        .pipe(
          tap(result => {
            return result;
          }),
          takeUntil(this.onDestroy$)
        );
    }
    return true;
  }

}
