import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  BrokerCompaniesService,
  BrokerCompany
} from '@core';
import {
  NgOnDestroy,
  SnackBarService,
  ConfirmationDialogComponent,
  CanDeactivateComponent
} from '@shared';

@Component({
  selector: 'app-register-broker-company',
  templateUrl: './register-broker-company.component.html',
  styleUrls: ['./register-broker-company.component.scss'],
  providers: [NgOnDestroy]
})
export class RegisterBrokerCompanyComponent extends CanDeactivateComponent {

  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;
  isSubmitted = false;
  isLoading = false;

  constructor(
    private brokerCompaniesService: BrokerCompaniesService,
    private router: Router,
    private dialog: MatDialog,
    private onDestroy$: NgOnDestroy,
    private snackBarService: SnackBarService
  ) {
    super();
  }

  onSubmit(company: BrokerCompany): void {
    this.isLoading = true;
    this.brokerCompaniesService.create(company).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.isSubmitted = true;
      this.isLoading = false;
      this.snackBarService.openSuccess('Registration successfully');
      this.router.navigateByUrl('/');
    }, () => {
      this.isLoading = false;
      this.snackBarService.openError('Registration failed');
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.isSubmitted) {
      this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          message: 'Are you sure you want to leave this page?'
        }
      });
      return this.confirmationDialog.afterClosed()
        .pipe(
          tap(result => {
            return result;
          }),
          takeUntil(this.onDestroy$)
        );
    }
    return true;
  }
}
