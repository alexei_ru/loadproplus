import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { NotAuthenticatedGuard } from '@core';
import { CanDeactivateGuard } from '@shared';

// Components
import {
  RegisterComponent,
  RegisterBrokerCompanyComponent,
  RegisterDispatcherCompanyComponent
} from './pages';
import { SignupComponent } from './signup.component';

const routes: Routes = [
  {
    path: 'register',
    component: SignupComponent,
    children: [
      {
        path: '',
        component: RegisterComponent,
        canActivate: [NotAuthenticatedGuard]
      },
      {
        path: 'broker-company',
        component: RegisterBrokerCompanyComponent,
        canDeactivate: [CanDeactivateGuard],
        canActivate: [NotAuthenticatedGuard]
      },
      {
        path: 'dispatcher-company',
        component: RegisterDispatcherCompanyComponent,
        canDeactivate: [CanDeactivateGuard],
        canActivate: [NotAuthenticatedGuard]
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SignupRoutingModule {

}
