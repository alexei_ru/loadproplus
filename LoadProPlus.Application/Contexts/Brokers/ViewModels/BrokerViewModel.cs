﻿namespace LoadProPlus.Application.Contexts.Brokers.ViewModels
{
    public class BrokerViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
