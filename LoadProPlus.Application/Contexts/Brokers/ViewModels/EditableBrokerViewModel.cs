﻿namespace LoadProPlus.Application.Contexts.Brokers.ViewModels
{
    public class EditableBrokerViewModel
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
