﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.DeleteBroker
{
    public class DeleteBrokerCommand : IRequest
    {
        public Guid BrokerId { get; set; }
    }
}
