﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.DeleteBroker
{
    public class DeleteBrokerCommandHandler : IRequestHandler<DeleteBrokerCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ICurrentUser _currentUser;

        public DeleteBrokerCommandHandler(ILoadProPlusDbContext dbContext, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _currentUser = currentUser;
        }

        public async Task<Unit> Handle(DeleteBrokerCommand request, CancellationToken cancellationToken)
        {
            var brokerCompanyId = new Guid(_currentUser.Id);
            var broker = await _dbContext
                .Brokers
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.BrokerId && x.CompanyId == brokerCompanyId);

            if (broker == null)
                throw new EntityNotFoundException(typeof(Broker));

            _dbContext.Brokers.Remove(broker);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
