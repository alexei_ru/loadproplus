﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.UpdateBroker
{
    public class UpdateBrokerCommandHandler : IRequestHandler<UpdateBrokerCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public UpdateBrokerCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<Unit> Handle(UpdateBrokerCommand request, CancellationToken cancellationToken)
        {
            var brokerCompanyId = new Guid(_currentUser.Id);
            var broker = await _dbContext
                .Brokers
                .FirstOrDefaultAsync(x => x.Id == request.BrokerId && x.CompanyId == brokerCompanyId);

            if (broker == null)
                throw new EntityNotFoundException(typeof(Broker));

            if (!string.IsNullOrEmpty(request.Password))
                broker.GeneratePassword(request.Password);

            _mapper.Map(request, broker);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
