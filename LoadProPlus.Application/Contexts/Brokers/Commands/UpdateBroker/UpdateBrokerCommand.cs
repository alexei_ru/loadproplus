﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.UpdateBroker
{
    public class UpdateBrokerCommand : IRequest
    {
        public Guid BrokerId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
    }
}
