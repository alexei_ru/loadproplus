﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.SignupBroker
{
    public class SignupBrokerCommandHandler : IRequestHandler<SignupBrokerCommand, BrokerViewModel>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public SignupBrokerCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<BrokerViewModel> Handle(SignupBrokerCommand request, CancellationToken cancellationToken)
        {
            var brokerCompanyId = new Guid(_currentUser.Id);
            var broker = _mapper.Map<SignupBrokerCommand, Broker>(request);
            broker.CompanyId = brokerCompanyId;
            broker.RoleId = (await _dbContext
                .Roles
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == Roles.Broker)).Id;

            broker.GeneratePassword(request.Password);
            _dbContext.Brokers.Add(broker);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return _mapper.Map<Broker, BrokerViewModel>(broker);
        }
    }
}
