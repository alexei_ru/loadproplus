﻿using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using MediatR;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.SignupBroker
{
    public class SignupBrokerCommand : IRequest<BrokerViewModel>
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
    }
}
