﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.Brokers.Commands.SignupBroker
{
    public class SignupBrokerCommandValidator : AbstractValidator<SignupBrokerCommand>
    {
        public SignupBrokerCommandValidator()
        {
            RuleFor(x => x.Username)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(x => x.Email)
                .MaximumLength(254)
                .EmailAddress()
                .NotEmpty();

            RuleFor(x => x.Password)
                .MaximumLength(40)
                .NotEmpty();

            RuleFor(x => x.Phone)
                .MaximumLength(30)
                .NotEmpty();

            RuleFor(x => x.Name)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(x => x.Surname)
                .MaximumLength(50)
                .NotEmpty();
        }
    }
}
