﻿using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Brokers.Queries.GetBrokerByIdAndCompanyId
{
    public class GetBrokerByIdAndCompanyIdQuery : IRequest<EditableBrokerViewModel>
    {
        public Guid BrokerId { get; set; }
    }
}
