﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Brokers.Queries.GetBrokerByIdAndCompanyId
{
    public class GetBrokerByIdAndCompanyIdQueryHandler : IRequestHandler<GetBrokerByIdAndCompanyIdQuery, EditableBrokerViewModel>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetBrokerByIdAndCompanyIdQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<EditableBrokerViewModel> Handle(GetBrokerByIdAndCompanyIdQuery request, CancellationToken cancellationToken)
        {
            var brokerCompanyId = new Guid(_currentUser.Id);
            var broker = await _dbContext
                .Brokers
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.BrokerId && x.CompanyId == brokerCompanyId);

            if (broker == null)
                throw new EntityNotFoundException(typeof(Broker));

            return _mapper.Map<Broker, EditableBrokerViewModel>(broker);
        }
    }
}
