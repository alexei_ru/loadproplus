﻿using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Brokers.Queries.GetPagedBrokersByCompanyId
{
    public class GetPagedBrokersByCompanyIdQuery : IRequest<PagedResult<BrokerViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
    }
}
