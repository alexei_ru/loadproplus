﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Brokers.Queries.GetPagedBrokersByCompanyId
{
    public class GetPagedBrokersByCompanyIdQueryHandler : IRequestHandler<GetPagedBrokersByCompanyIdQuery, PagedResult<BrokerViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly ICurrentUser _currentUser;

        public GetPagedBrokersByCompanyIdQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
            _currentUser = currentUser;
        }

        public async Task<PagedResult<BrokerViewModel>> Handle(GetPagedBrokersByCompanyIdQuery request, CancellationToken cancellationToken)
        {
            var companyId = new Guid(_currentUser.Id);

            var query = _dbContext
                .Brokers
                .AsNoTracking()
                .Where(x => x.CompanyId == companyId);

            return await PagedResult<BrokerViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
