﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Brokers.Commands.SignupBroker;
using LoadProPlus.Application.Contexts.Brokers.Commands.UpdateBroker;
using LoadProPlus.Application.Contexts.Brokers.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.Brokers
{
    public class BrokerMapperProfile : Profile
    {
        public BrokerMapperProfile()
        {
            CreateMap<SignupBrokerCommand, Broker>()
                .ForMember(x => x.Company, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore());

            CreateMap<Broker, BrokerViewModel>();

            CreateMap<Broker, EditableBrokerViewModel>();

            CreateMap<UpdateBrokerCommand, Broker>()
                .ForMember(x => x.Company, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore());
        }
    }
}
