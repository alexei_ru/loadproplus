﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.Commands.CreateLoad;
using LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoad;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.Loads
{
    public class LoadsMapperProfile : Profile
    {
        public LoadsMapperProfile()
        {
			CreateMap<CreateLoadCommand, Load>()
				.ForMember(load => load.Id, opt => opt.Ignore());

            CreateMap<UpdateLoadCommand, Load>();

			CreateMap<LoadStatus, LoadStatusViewModel>();

            CreateMap<Load, LoadViewModel>()
				.ForMember(m => m.DeliveryLocation, opt => opt.MapFrom(src => src.Delivery))
				.ForMember(m => m.PickUpLocation, opt => opt.MapFrom(src => src.PickUp))
				.ForMember(m => m.TrailerType, opt => opt.MapFrom(src => src.TrailerType))
				.ForMember(m => m.LoadStatus, opt => opt.MapFrom(src => src.LoadStatus));
        }
    }
}
