﻿using System;

namespace LoadProPlus.Application.Contexts.Loads.ViewModels
{
    public class LoadStatusViewModel
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}
