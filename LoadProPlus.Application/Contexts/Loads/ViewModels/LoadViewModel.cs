﻿using LoadProPlus.Application.Contexts.Locations.ViewModels;
using LoadProPlus.Domain.Entities;
using System;

namespace LoadProPlus.Application.Contexts.Loads.ViewModels
{
    public class LoadViewModel
	{
		public Guid Id { get; set; }
		public CityViewModel PickUpLocation { get; set; }
		public DateTime PickUpDateTime { get; set; }
		public CityViewModel DeliveryLocation { get; set; }
		public DateTime DeliveryDateTime { get; set; }
		public TrailerType TrailerType { get; set; }
		public string Commodity { get; set; }
		public decimal Weight { get; set; }
		public decimal CustomerRate { get; set; }
		public string AdditionalInfo { get; set; }
		public LoadStatusViewModel LoadStatus { get; set; }
	}
}
