﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Enums;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Loads.Commands.CreateLoad
{
	public class CreateLoadCommandHandler : IRequestHandler<CreateLoadCommand, LoadViewModel>
	{
		private readonly ILoadsBroadcaster _loadsBroadcaster;
		private readonly ILoadProPlusDbContext _dbContext;
		private readonly IMapper _mapper;
		private readonly ICurrentUser _currentUser;

		public CreateLoadCommandHandler(
			ILoadProPlusDbContext dbContext,
			IMapper mapper,
			ICurrentUser currentUser,
			ILoadsBroadcaster loadsBroadcaster)
		{
			_dbContext = dbContext;
			_mapper = mapper;
			_currentUser = currentUser;
			_loadsBroadcaster = loadsBroadcaster;
		}

		public async Task<LoadViewModel> Handle(CreateLoadCommand request, CancellationToken cancellationToken)
		{
			var brokerId = new Guid(_currentUser.Id);
			var broker = _dbContext.Brokers
				.AsNoTracking()
				.FirstOrDefault(f => f.Id == brokerId);

			if (broker == null)
				throw new EntityNotFoundException(typeof(Broker));

			var acceptedLoadStatus = await _dbContext
				.LoadStatuses
				.AsNoTracking()
				.FirstOrDefaultAsync(f => f.Name.ToLower() == LoadStatusesEnum.Accepted.ToString().ToLower());

			if (acceptedLoadStatus == null)
				throw new Exception($"Failed to choose a default LoadStatus");

			var load = _mapper.Map<CreateLoadCommand, Load>(request);

			load.BrokerId = brokerId;
			load.LoadStatusId = acceptedLoadStatus.Id;

			_dbContext.Loads.Add(load);

			await _dbContext.SaveChangesAsync(cancellationToken);
            
            // HZ.....
            var createdLoad = await _dbContext
                .Loads
                .AsNoTracking()
                .Include(x => x.Delivery)
                .Include(x => x.PickUp)
                .Include(x => x.TrailerType)
                .Include(x => x.LoadStatus)
                .FirstOrDefaultAsync(x => x.Id == load.Id);

			var createdLoadViewModel = _mapper.Map<Load, LoadViewModel>(createdLoad);

			await _loadsBroadcaster.BroadcastLoad(createdLoadViewModel);

			return createdLoadViewModel;
		}
	}
}
