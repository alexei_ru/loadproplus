﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadProPlus.Application.Contexts.Loads.Commands.CreateLoad
{
    public class CreateLoadCommandValidator : AbstractValidator<CreateLoadCommand>
    {
        public CreateLoadCommandValidator()
        {
            RuleFor(x => x.CustomerRate)
                .GreaterThan(0)
                .NotEmpty();

            //RuleFor(x => x.Comments)
            //    .MaximumLength(254)
            //    .NotEmpty();

            RuleFor(x => x.Commodity)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(x => x.DeliveryDateTime)
                //.GreaterThan(DateTime.Now)
                .NotEmpty();

            RuleFor(x => x.DeliveryId)
                .NotEmpty();

            RuleFor(x => x.PickUpDateTime)
                //.GreaterThan(DateTime.Now)
                .NotEmpty();

            RuleFor(x => x.PickUpId)
                .NotEmpty();

            RuleFor(x => x.TrailerTypeId)
                .NotEmpty();

            RuleFor(x => x.Weight)
                .GreaterThan(0)
                .NotEmpty();

        }
    }
}
