﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoad
{
    public class UpdateLoadCommandValidator : AbstractValidator<UpdateLoadCommand>
    {
        public UpdateLoadCommandValidator()
        {
            RuleFor(x => x.CustomerRate)
                .GreaterThan(0)
                .NotEmpty();

			RuleFor(x => x.AdditionalInfo)
					.MaximumLength(254);

            RuleFor(x => x.Commodity)
                    .MaximumLength(50)
                    .NotEmpty();

            RuleFor(x => x.DeliveryDateTime)
                    //.GreaterThan(DateTime.Now)
                    .NotEmpty();

            RuleFor(x => x.DeliveryId)
                    .NotEmpty();

            RuleFor(x => x.PickUpDateTime)
                    //.GreaterThan(DateTime.Now)
                    .NotEmpty();

            RuleFor(x => x.PickUpId)
                    .NotEmpty();

            RuleFor(x => x.TrailerTypeId)
                    .NotEmpty();

            RuleFor(x => x.Weight)
                    .GreaterThan(0)
                    .NotEmpty();
        }
    }
}
