﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoad
{
    public class UpdateLoadCommand : IRequest
    {
        public Guid Id { get; set; }
        public Guid PickUpId { get; set; }
        public DateTime PickUpDateTime { get; set; }
        public Guid DeliveryId { get; set; }
        public DateTime DeliveryDateTime { get; set; }
        public Guid TrailerTypeId { get; set; }
        public string Commodity { get; set; }
        public decimal Weight { get; set; }
        public decimal CustomerRate { get; set; }
        public string AdditionalInfo { get; set; }
		public Guid LoadStatusId { get; set; }
    }
}
