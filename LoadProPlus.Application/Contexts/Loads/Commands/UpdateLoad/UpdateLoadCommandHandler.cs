﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoad
{
	public class UpdateLoadCommandHandler : IRequestHandler<UpdateLoadCommand, Unit>
	{
		private readonly ILoadsBroadcaster _loadsBroadcaster;
		private readonly ILoadProPlusDbContext _dbContext;
		private readonly IMapper _mapper;
		private readonly ICurrentUser _currentUser;

		public UpdateLoadCommandHandler(
			ILoadProPlusDbContext dbContext,
			IMapper mapper,
			ICurrentUser currentUser,
			ILoadsBroadcaster loadsBroadcaster)
		{
			_dbContext = dbContext;
			_mapper = mapper;
			_currentUser = currentUser;
			_loadsBroadcaster = loadsBroadcaster;
		}

		public async Task<Unit> Handle(UpdateLoadCommand request, CancellationToken cancellationToken)
		{
			var brokerId = new Guid(_currentUser.Id);
			var load = await _dbContext
				.Loads
				.FirstOrDefaultAsync(x => x.Id == request.Id && x.BrokerId == brokerId);

			if (load == null)
				throw new EntityNotFoundException();

			_mapper.Map(request, load);
			await _dbContext.SaveChangesAsync(cancellationToken);

			var updatedLoad = await _dbContext
				.Loads
				.Include(x => x.Delivery)
				.Include(x => x.PickUp)
				.Include(x => x.TrailerType)
				.Include(x => x.LoadStatus)
				.FirstOrDefaultAsync(x => x.Id == load.Id);
			
			await _loadsBroadcaster.BroadcastLoad(_mapper.Map<LoadViewModel>(updatedLoad));

			return Unit.Value;
		}
	}
}
