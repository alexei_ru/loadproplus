﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoadStatus
{
	public class UpdateLoadStatusCommand : IRequest
	{
		public Guid LoadId { get; set; }
		public Guid LoadStatusId { get; set; }
	}
}
