﻿using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Loads.Commands.UpdateLoadStatus
{
	public class UpdateLoadStatusCommandHandler : IRequestHandler<UpdateLoadStatusCommand, Unit>
	{
		private readonly ILoadProPlusDbContext _dbContext;

		public UpdateLoadStatusCommandHandler(ILoadProPlusDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<Unit> Handle(UpdateLoadStatusCommand request, CancellationToken cancellationToken)
		{
			var loadToUpdate = (await _dbContext.Loads.FirstOrDefaultAsync(f => f.Id == request.LoadId));

			if (loadToUpdate == null)
				throw new EntityNotFoundException(typeof(Load));

			if (!await _dbContext.LoadStatuses.AnyAsync(a => a.Id == request.LoadStatusId))
				throw new EntityNotFoundException(typeof(LoadStatus));

			loadToUpdate.LoadStatusId = request.LoadStatusId;

			await _dbContext.SaveChangesAsync(cancellationToken);

			return Unit.Value;
		}
	}
}
