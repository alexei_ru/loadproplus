﻿using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Loads.Commands.DeleteLoad
{
	public class DeleteLoadCommandHandler : IRequestHandler<DeleteLoadCommand, Unit>
	{
		private readonly ILoadProPlusDbContext _dbContext;
        private readonly ICurrentUser _currentUser;

        public DeleteLoadCommandHandler(ILoadProPlusDbContext dbContext, ICurrentUser currentUser)
		{
			_dbContext = dbContext;
            _currentUser = currentUser;
        }

		public async Task<Unit> Handle(DeleteLoadCommand request, CancellationToken cancellationToken)
		{
            var brokerId = new Guid(_currentUser.Id);
            var load = await _dbContext
                .Loads
                .AsNoTracking()
				.FirstOrDefaultAsync(x => x.Id == request.Id && x.BrokerId == brokerId);

            if (load == null)
                throw new EntityNotFoundException(typeof(Load));

			_dbContext.Loads.Remove(load);
			await _dbContext.SaveChangesAsync(cancellationToken);

			return Unit.Value;
		}
	}
}
