﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadProPlus.Application.Contexts.Loads.Commands.DeleteLoad
{
    public class DeleteLoadCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}
