﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetPagedAvailableLoads
{
    public class GetPagedAvailableLoadsQueryHandler : IRequestHandler<GetPagedAvailableLoadsQuery ,PagedResult<LoadViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetPagedAvailableLoadsQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public async Task<PagedResult<LoadViewModel>> Handle(GetPagedAvailableLoadsQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext
                .Loads
                .AsNoTracking()
                .Include(i => i.Broker)
                .Include(i => i.PickUp).ThenInclude(ti => ti.State)
                .Include(i => i.Delivery).ThenInclude(ti => ti.State)
                .Include(i => i.TrailerType);

            return await PagedResult<LoadViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
