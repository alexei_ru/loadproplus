﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetPagedAvailableLoads
{
    public class GetPagedAvailableLoadsQuery : IRequest<PagedResult<LoadViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
    }
}
