﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetLoadsByBrokerId
{
	public class GetLoadsByBrokerIdQuery : IRequest<PagedResult<LoadViewModel>>
	{
		public PageViewModel PageViewModel { get; set; }
		public string AccessToken { get; set; }
	}
}
