﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetLoadsByBrokerId
{
	public class GetLoadsByBrokerIdQueryHandler : IRequestHandler<GetLoadsByBrokerIdQuery, PagedResult<LoadViewModel>>
	{
		private readonly ILoadProPlusDbContext _dbContext;
		private readonly IConfigurationProvider _configurationProvider;
        private readonly ICurrentUser _currentUser;

        public GetLoadsByBrokerIdQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider, ICurrentUser currentUser)
		{
			_dbContext = dbContext;
			_configurationProvider = configurationProvider;
            _currentUser = currentUser;
        }

		public async Task<PagedResult<LoadViewModel>> Handle(GetLoadsByBrokerIdQuery request, CancellationToken cancellationToken)
		{
			var brokerId = new Guid(_currentUser.Id);
            var query = _dbContext.Loads
                .AsNoTracking()
                .Include(i => i.Broker)
                .Include(i => i.PickUp).ThenInclude(ti => ti.State)
                .Include(i => i.Delivery).ThenInclude(ti => ti.State)
                .Include(i => i.TrailerType)
                .Where(w => w.BrokerId == brokerId);

			return await PagedResult<LoadViewModel>.From(query, request.PageViewModel, _configurationProvider);
		}
	}
}
