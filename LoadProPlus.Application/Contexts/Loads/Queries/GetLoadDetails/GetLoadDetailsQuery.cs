﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetLoadDetails
{
    public class GetLoadDetailsQuery : IRequest<LoadViewModel>
    {
        public Guid Id { get; set; }
    }
}
