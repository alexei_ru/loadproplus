﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetLoadDetails
{
	public class GetLoadDetailsQueryHandler : IRequestHandler<GetLoadDetailsQuery, LoadViewModel>
	{
		private readonly ILoadProPlusDbContext _dbContext;
		private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetLoadDetailsQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
		{
			_dbContext = dbContext;
			_mapper = mapper;
            _currentUser = currentUser;
        }

		public async Task<LoadViewModel> Handle(GetLoadDetailsQuery request, CancellationToken cancellationToken)
		{
            var brokerId = new Guid(_currentUser.Id);
            var load = (await _dbContext.Loads
				.AsNoTracking()
				.Include(i => i.PickUp).ThenInclude(ti => ti.State)
				.Include(i => i.Delivery).ThenInclude(ti => ti.State)
				.Include(i => i.Broker)
				.Include(i => i.TrailerType)
				.Include(i => i.LoadStatus)
				.FirstOrDefaultAsync(f => f.Id == request.Id && f.BrokerId == brokerId));

			if (load == null)
				throw new EntityNotFoundException();

			return _mapper.Map<Load, LoadViewModel>(load);
		}
	}
}
