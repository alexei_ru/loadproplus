﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Loads.ViewModels;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetLoadStatuses
{
	public class GetLoadStatusesQueryHandler : IRequestHandler<GetLoadStatusesQuery, IEnumerable<LoadStatusViewModel>>
	{
		private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetLoadStatusesQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
		{
			_dbContext = dbContext;
            _mapper = mapper;
        }

		public async Task<IEnumerable<LoadStatusViewModel>> Handle(GetLoadStatusesQuery request, CancellationToken cancellationToken)
		{
            var loadStatuses = await _dbContext
                .LoadStatuses
                .AsNoTracking()
                .ToListAsync();

            return _mapper.Map<IEnumerable<LoadStatusViewModel>>(loadStatuses);
		}
	}
}
