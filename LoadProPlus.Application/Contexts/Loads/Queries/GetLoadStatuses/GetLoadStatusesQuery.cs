﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using MediatR;
using System.Collections.Generic;

namespace LoadProPlus.Application.Contexts.Loads.Queries.GetLoadStatuses
{
    public class GetLoadStatusesQuery : IRequest<IEnumerable<LoadStatusViewModel>>
	{
	}
}
