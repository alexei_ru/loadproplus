﻿using LoadProPlus.Application.Contexts.Dispatchers.ViewModels;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Dispatchers.Queries.GetDispatcherByIdAndCompanyId
{
    public class GetDispatcherByIdAndCompanyIdQuery : IRequest<EditableDispatcherViewModel>
    {
        public Guid DispatcherId { get; set; }
    }
}
