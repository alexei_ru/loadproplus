﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Dispatchers.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Dispatchers.Queries.GetDispatcherByIdAndCompanyId
{
    public class GetDispatcherByIdAndCompanyIdQueryHandler : IRequestHandler<GetDispatcherByIdAndCompanyIdQuery, EditableDispatcherViewModel>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetDispatcherByIdAndCompanyIdQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<EditableDispatcherViewModel> Handle(GetDispatcherByIdAndCompanyIdQuery request, CancellationToken cancellationToken)
        {
            var dispatcherCompanyId = new Guid(_currentUser.Id);
            var dispatcher = await _dbContext
                .Dispatchers
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.DispatcherId && x.CompanyId == dispatcherCompanyId);

            if (dispatcher == null)
                throw new EntityNotFoundException(typeof(Dispatcher));

            return _mapper.Map<Dispatcher, EditableDispatcherViewModel>(dispatcher);
        }
    }
}
