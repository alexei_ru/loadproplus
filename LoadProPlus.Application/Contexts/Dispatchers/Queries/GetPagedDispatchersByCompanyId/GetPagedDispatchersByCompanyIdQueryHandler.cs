﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Dispatchers.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Dispatchers.Queries.GetPagedDispatchersByCompanyId
{
    public class GetPagedDispatchersByCompanyIdQueryHandler : IRequestHandler<GetPagedDispatchersByCompanyIdQuery, PagedResult<DispatcherViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly ICurrentUser _currentUser;

        public GetPagedDispatchersByCompanyIdQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
            _currentUser = currentUser;
        }

        public async Task<PagedResult<DispatcherViewModel>> Handle(GetPagedDispatchersByCompanyIdQuery request, CancellationToken cancellationToken)
        {
            var dispatcherCompanyId = new Guid(_currentUser.Id);

            var query = _dbContext
                .Dispatchers
                .AsNoTracking()
                .Where(x => x.CompanyId == dispatcherCompanyId);

            return await PagedResult<DispatcherViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
