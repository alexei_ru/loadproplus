﻿using LoadProPlus.Application.Contexts.Dispatchers.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Dispatchers.Queries.GetPagedDispatchersByCompanyId
{
    public class GetPagedDispatchersByCompanyIdQuery : IRequest<PagedResult<DispatcherViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
    }
}
