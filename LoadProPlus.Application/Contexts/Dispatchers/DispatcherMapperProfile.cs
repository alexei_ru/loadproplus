﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Dispatchers.Commands.SignupDispatcher;
using LoadProPlus.Application.Contexts.Dispatchers.Commands.UpdateDispatcherCommand;
using LoadProPlus.Application.Contexts.Dispatchers.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.Dispatchers
{
    public class DispatcherMapperProfile : Profile
    {
        public DispatcherMapperProfile()
        {
            CreateMap<SignupDispatcherCommand, Dispatcher>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Company, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore());

            CreateMap<Dispatcher, DispatcherViewModel>();

            CreateMap<Dispatcher, EditableDispatcherViewModel>();

            CreateMap<UpdateDispatcherCommand, Dispatcher>()
                .ForMember(x => x.Company, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore());
        }
    }
}
