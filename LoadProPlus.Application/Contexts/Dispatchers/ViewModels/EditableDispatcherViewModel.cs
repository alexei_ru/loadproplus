﻿namespace LoadProPlus.Application.Contexts.Dispatchers.ViewModels
{
    public class EditableDispatcherViewModel
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
