﻿namespace LoadProPlus.Application.Contexts.Dispatchers.ViewModels
{
    public class DispatcherViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
