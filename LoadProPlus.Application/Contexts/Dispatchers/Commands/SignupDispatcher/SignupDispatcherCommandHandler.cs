﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Dispatchers.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Dispatchers.Commands.SignupDispatcher
{
    public class SignupDispatcherCommandHandler : IRequestHandler<SignupDispatcherCommand, DispatcherViewModel>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public SignupDispatcherCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<DispatcherViewModel> Handle(SignupDispatcherCommand request, CancellationToken cancellationToken)
        {
            var dispatcherCompanyId = new Guid(_currentUser.Id);
            var dispatcher = _mapper.Map<SignupDispatcherCommand, Dispatcher>(request);
            dispatcher.CompanyId = dispatcherCompanyId;

            dispatcher.RoleId = (await _dbContext
                .Roles
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == Roles.Dispatcher)).Id;

            dispatcher.GeneratePassword(request.Password);

            _dbContext.Dispatchers.Add(dispatcher);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return _mapper.Map<Dispatcher, DispatcherViewModel>(dispatcher);
        }
    }
}
