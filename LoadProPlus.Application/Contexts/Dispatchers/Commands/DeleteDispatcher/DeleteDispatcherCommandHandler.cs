﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Dispatchers.Commands.DeleteDispatcher
{
    public class DeleteDispatcherCommandHandler : IRequestHandler<DeleteDispatcherCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ICurrentUser _currentUser;

        public DeleteDispatcherCommandHandler(ILoadProPlusDbContext dbContext, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _currentUser = currentUser;
        }

        public async Task<Unit> Handle(DeleteDispatcherCommand request, CancellationToken cancellationToken)
        {
            var dispatcherCompanyId = new Guid(_currentUser.Id);
            var dispatcher = await _dbContext
                .Dispatchers
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.DispatcherId && x.CompanyId == dispatcherCompanyId);

            if (dispatcher == null)
                throw new EntityNotFoundException(typeof(Dispatcher));

            _dbContext.Dispatchers.Remove(dispatcher);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
