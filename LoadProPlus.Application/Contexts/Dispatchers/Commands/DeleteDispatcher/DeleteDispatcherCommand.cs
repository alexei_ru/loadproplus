﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Dispatchers.Commands.DeleteDispatcher
{
    public class DeleteDispatcherCommand : IRequest
    {
        public Guid DispatcherId { get; set; }
    }
}
