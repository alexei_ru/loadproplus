﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Dispatchers.Commands.UpdateDispatcherCommand
{
    public class UpdateDispatcherCommand : IRequest
    {
        public Guid DispatcherId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
    }
}
