﻿using AutoMapper;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Dispatchers.Commands.UpdateDispatcherCommand
{
    class UpdateDispatcherCommandHandler : IRequestHandler<UpdateDispatcherCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public UpdateDispatcherCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<Unit> Handle(UpdateDispatcherCommand request, CancellationToken cancellationToken)
        {
            var dispatcherCompanyId = new Guid(_currentUser.Id);

            var dispatcher = await _dbContext
                .Dispatchers
                .FirstOrDefaultAsync(x => x.Id == request.DispatcherId && x.CompanyId == dispatcherCompanyId);

            if (dispatcher == null)
                throw new EntityNotFoundException(typeof(Dispatcher));

            if (!string.IsNullOrEmpty(request.Password))
                dispatcher.GeneratePassword(request.Password);

            _mapper.Map(request, dispatcher);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
