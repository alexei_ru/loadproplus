﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadProPlus.Application.Contexts.Dispatchers.Commands.UpdateDispatcherCommand
{
    public class UpdateDispatcherCommandValidator : AbstractValidator<UpdateDispatcherCommand>
    {
        public UpdateDispatcherCommandValidator()
        {
            RuleFor(x => x.Username)
                    .MaximumLength(50)
                    .NotEmpty();

            RuleFor(x => x.Email)
                    .MaximumLength(254)
                    .EmailAddress()
                    .NotEmpty();

            RuleFor(x => x.Password)
                    .MaximumLength(40);

            RuleFor(x => x.Phone)
                    .MaximumLength(30)
                    .NotEmpty();

            RuleFor(x => x.Name)
                    .MaximumLength(50)
                    .NotEmpty();

            RuleFor(x => x.Surname)
                    .MaximumLength(50)
                    .NotEmpty();
        }
    }
}
