﻿using AutoMapper;
using LoadProPlus.Application.Contexts.BrokerCompanies.Commands.SignupBrokerCompany;
using LoadProPlus.Application.Contexts.BrokerCompanies.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.BrokerCompanies
{
    public class BrokerCompanyMapperProfile : Profile
    {
        public BrokerCompanyMapperProfile()
        {
            CreateMap<SignupBrokerCompanyCommand, BrokerCompany>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore());

            CreateMap<BrokerCompany, BrokerCompanyViewModel>();
        }
    }
}
