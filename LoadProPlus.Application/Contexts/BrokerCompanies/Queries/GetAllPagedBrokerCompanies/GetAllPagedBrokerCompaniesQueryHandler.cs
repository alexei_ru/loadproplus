﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.BrokerCompanies.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.BrokerCompanies.Queries.GetAllPagedBrokerCompanies
{
    public class GetAllPagedBrokerCompaniesQueryHandler : IRequestHandler<GetAllPagedBrokerCompaniesQuery, PagedResult<BrokerCompanyViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetAllPagedBrokerCompaniesQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public async Task<PagedResult<BrokerCompanyViewModel>> Handle(GetAllPagedBrokerCompaniesQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext
                .BrokerCompanies
                .AsNoTracking();

            return await PagedResult<BrokerCompanyViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
