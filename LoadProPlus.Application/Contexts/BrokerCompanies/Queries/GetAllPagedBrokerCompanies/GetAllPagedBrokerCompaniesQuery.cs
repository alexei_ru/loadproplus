﻿using LoadProPlus.Application.Contexts.BrokerCompanies.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;

namespace LoadProPlus.Application.Contexts.BrokerCompanies.Queries.GetAllPagedBrokerCompanies
{
    public class GetAllPagedBrokerCompaniesQuery : IRequest<PagedResult<BrokerCompanyViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
    }
}
