﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.BrokerCompanies.Commands.SignupBrokerCompany
{
    public class SignupBrokerCompanyCommandHandler : IRequestHandler<SignupBrokerCompanyCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;

        public SignupBrokerCompanyCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(SignupBrokerCompanyCommand request, CancellationToken cancellationToken)
        {
            var brokerCompany = _mapper.Map<SignupBrokerCompanyCommand, BrokerCompany>(request);
            brokerCompany.RoleId = (await _dbContext
                    .Roles
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Name == Roles.BrokerCompany)).Id;

            brokerCompany.GeneratePassword(request.Password);
            _dbContext.BrokerCompanies.Add(brokerCompany);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
