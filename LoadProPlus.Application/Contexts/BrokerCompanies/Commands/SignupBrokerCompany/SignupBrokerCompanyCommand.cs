﻿using MediatR;

namespace LoadProPlus.Application.Contexts.BrokerCompanies.Commands.SignupBrokerCompany
{
    public class SignupBrokerCompanyCommand : IRequest
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string McNumber { get; set; }
        public string CompanyName { get; set; }
        public string MainAddress { get; set; }
        public string ZipCode { get; set; }
        public string PrimaryContactName { get; set; }
        public string Fax { get; set; }
        public string DotNumber { get; set; }
    }
}
