﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Users.Queries.CheckUniqueUserEmail
{
    public class CheckUniqueUserEmailQuery : IRequest<bool>
    {
        public Guid? UserId { get; set; }
        public string Email { get; set; }
    }
}
