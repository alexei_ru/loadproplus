﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Users.Queries.CheckUniqueUserEmail
{
    public class CheckUniqueUserEmailQueryHandler : IRequestHandler<CheckUniqueUserEmailQuery, bool>
    {
        private readonly ILoadProPlusDbContext _dbContext;

        public CheckUniqueUserEmailQueryHandler(ILoadProPlusDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(CheckUniqueUserEmailQuery request, CancellationToken cancellationToken)
        {
            if (request.UserId.HasValue)
                return await _dbContext
                    .Users
                    .Where(x => x.Id != request.UserId.Value)
                    .AnyAsync(x => x.Email.Trim().ToLower() == request.Email.Trim().ToLower());
            else
                return await _dbContext
                    .Users
                    .AnyAsync(x => x.Email.Trim().ToLower() == request.Email.Trim().ToLower());
        }
    }
}
