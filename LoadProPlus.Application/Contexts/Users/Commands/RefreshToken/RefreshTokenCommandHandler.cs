﻿using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Interfaces.Utils;
using LoadProPlus.Application.Models.Shared;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Users.Commands.RefreshToken
{
	public class RefreshTokenCommandHandler : IRequestHandler<RefreshTokenCommand, SignInResultViewModel>
	{
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ITokenGenerator _tokenGenerator;
		private readonly IJwtHelpers _jwtHelper;

		public RefreshTokenCommandHandler(ILoadProPlusDbContext dbContext, ITokenGenerator tokenGenerator, IJwtHelpers jwtHelper)
		{
            _dbContext = dbContext;
            _tokenGenerator = tokenGenerator;
			_jwtHelper = jwtHelper;
		}

		public async Task<SignInResultViewModel> Handle(RefreshTokenCommand request, CancellationToken cancellationToken)
		{
			var userId = new Guid(_jwtHelper.GetClaimsFromJwt(request.AccessToken).FindFirst(ClaimTypes.NameIdentifier).Value);

            var user = await _dbContext
                .Users
                .AsNoTracking()
                .Include(x => x.UserRefreshToken)
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
                throw new EntityNotFoundException(typeof(User));

            var userRefreshToken = user.UserRefreshToken;
            if (userRefreshToken == null || userRefreshToken.IsExpired() || userRefreshToken.RefreshToken != request.RefreshToken)
                throw new InvalidTokenException();

			//var refreshToken = _tokenGenerator.GenerateRefreshToken();
			var accessToken = _tokenGenerator.CreateToken(user);

			//Can be useful later
			//_userRefreshTokenRepo.Delete(userToken.Id);
			//_userRefreshTokenRepo.Create(new UserRefreshToken
			//{
			//	ExpirationTime = DateTime.UtcNow.AddMinutes(1000), //Add minutes to config. Create separate method for expiration time calculation
			//	RefreshToken = refreshToken,
			//	UserId = request.UserId
			//});

			return new SignInResultViewModel
			{
				AccessToken = accessToken,
				RefreshToken = request.RefreshToken
			};
		}
	}
}
