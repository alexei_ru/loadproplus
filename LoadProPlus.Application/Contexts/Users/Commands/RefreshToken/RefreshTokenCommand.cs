﻿using LoadProPlus.Application.Models.Shared;
using MediatR;

namespace LoadProPlus.Application.Contexts.Users.Commands.RefreshToken
{
    public class RefreshTokenCommand : IRequest<SignInResultViewModel>
	{
		public string RefreshToken { get; set; }
		public string AccessToken { get; set; }
	}
}
