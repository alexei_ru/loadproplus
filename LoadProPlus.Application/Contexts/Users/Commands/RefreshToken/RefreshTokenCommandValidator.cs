﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.Users.Commands.RefreshToken
{
    public class RefreshTokenCommandValidator : AbstractValidator<RefreshTokenCommand>
	{
		public RefreshTokenCommandValidator()
		{
			RuleFor(r => r.AccessToken)
				.NotEmpty();

			RuleFor(r => r.RefreshToken)
				.NotEmpty();
		}
	}
}
