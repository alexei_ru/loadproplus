﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Users.Commands.LogoutUser
{
    public class LogoutUserCommandHandler : IRequestHandler<LogoutUserCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ICurrentUser _currentUser;

        public LogoutUserCommandHandler(ILoadProPlusDbContext dbContext, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _currentUser = currentUser;
        }

        public async Task<Unit> Handle(LogoutUserCommand request, CancellationToken cancellationToken)
        {
            var userId = new Guid(_currentUser.Id);
            var userRefreshToken = await _dbContext
                .UserRefreshTokens
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.RefreshToken == request.RefreshToken && x.UserId == userId);

            if (userRefreshToken == null)
                throw new EntityNotFoundException(typeof(UserRefreshToken));

            _dbContext.UserRefreshTokens.Remove(userRefreshToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
