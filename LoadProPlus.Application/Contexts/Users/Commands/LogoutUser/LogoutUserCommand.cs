﻿using MediatR;

namespace LoadProPlus.Application.Contexts.Users.Commands.LogoutUser
{
    public class LogoutUserCommand : IRequest
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
