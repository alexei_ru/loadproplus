﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.Users.Commands.SigninCompany
{
    public class SigninCompanyCommandValidator : AbstractValidator<SigninCompanyCommand>
    {
        public SigninCompanyCommandValidator()
        {
            RuleFor(x => x.MCNumber)
                .MaximumLength(70)
                .NotEmpty();

            RuleFor(x => x.Password)
                .MaximumLength(40)
                .NotEmpty();
        }
    }
}
