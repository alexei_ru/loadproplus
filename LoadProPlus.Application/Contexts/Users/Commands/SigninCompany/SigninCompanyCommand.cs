﻿using LoadProPlus.Application.Models.Shared;
using MediatR;

namespace LoadProPlus.Application.Contexts.Users.Commands.SigninCompany
{
    public class SigninCompanyCommand : IRequest<SignInResultViewModel>
    {
        public string MCNumber { get; set; }
        public string Password { get; set; }
    }
}
