﻿using LoadProPlus.Application.Models.Shared;
using MediatR;

namespace LoadProPlus.Application.Contexts.Users.Commands.SigninEmployee
{
    public class SigninEmployeeCommand : IRequest<SignInResultViewModel>
    {
        public string MCNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
