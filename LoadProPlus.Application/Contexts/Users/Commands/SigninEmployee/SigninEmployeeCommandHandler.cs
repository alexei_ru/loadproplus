﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Models.Shared;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Application.Utils.Helpers;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LoadProPlus.Application.Contexts.Users.Commands.SigninEmployee
{
    public class SigninEmployeeCommandHandler : IRequestHandler<SigninEmployeeCommand, SignInResultViewModel>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly AppSettings _appSettings;

        public SigninEmployeeCommandHandler(ILoadProPlusDbContext dbContext, ITokenGenerator tokenGenerator, IOptions<AppSettings> appSettings)
        {
            _dbContext = dbContext;
            _tokenGenerator = tokenGenerator;
            _appSettings = appSettings.Value;
        }

        public async Task<SignInResultViewModel> Handle(SigninEmployeeCommand request, CancellationToken cancellationToken)
        {
            var employee = await _dbContext
                .Employees
                .AsNoTracking()
                .Include(x => x.Role)
                .Include(x => x.Company)
                .Include(x => x.UserRefreshToken)
                .FirstOrDefaultAsync(x => x.Username == request.Username && x.Company.McNumber == request.MCNumber);

            if (employee == null)
                throw new EntityNotFoundException(typeof(Employee));

            employee.PasswordMatch(request.Password);

            var refreshToken = _tokenGenerator.GenerateRefreshToken();
            var expirationTokenTime = int.Parse(_appSettings.TokenSettings.RefreshTokenExpirationTime);

            var userRefreshToken = employee.UserRefreshToken;

            if (userRefreshToken != null)
            {
                userRefreshToken.RefreshToken = refreshToken;
                userRefreshToken.ExpirationTime = DateTime.UtcNow.AddMinutes(expirationTokenTime);
                _dbContext.UserRefreshTokens.Update(userRefreshToken);
            }
            else
            {
                _dbContext.UserRefreshTokens.Add(new UserRefreshToken
                {
                    ExpirationTime = DateTime.UtcNow.AddMinutes(expirationTokenTime),
                    RefreshToken = refreshToken,
                    UserId = employee.Id
                });
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            var token = _tokenGenerator.CreateToken(employee);

            return new SignInResultViewModel
            {
                AccessToken = token,
                RefreshToken = refreshToken
            };
        }
    }
}
