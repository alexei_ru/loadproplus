﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.Users.Commands.SigninEmployee
{
    public class SigninEmployeeCommandValidator : AbstractValidator<SigninEmployeeCommand>
    {
        public SigninEmployeeCommandValidator()
        {
            RuleFor(x => x.MCNumber)
                .NotEmpty()
                .MaximumLength(70);

            RuleFor(x => x.Username)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Password)
                .NotEmpty()
                .MaximumLength(40);
        }
    }
}
