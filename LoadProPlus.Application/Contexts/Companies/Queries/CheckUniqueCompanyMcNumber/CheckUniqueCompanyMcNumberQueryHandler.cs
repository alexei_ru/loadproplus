﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Companies.Queries.CheckUniqueCompanyMcNumber
{
    public class CheckUniqueCompanyMcNumberQueryHandler : IRequestHandler<CheckUniqueCompanyMcNumberQuery, bool>
    {
        private readonly ILoadProPlusDbContext _dbContext;

        public CheckUniqueCompanyMcNumberQueryHandler(ILoadProPlusDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(CheckUniqueCompanyMcNumberQuery request, CancellationToken cancellationToken)
        {
            if (request.CompanyId.HasValue)
                return await _dbContext
                    .Companies
                    .AnyAsync(x => x.Id != request.CompanyId && x.McNumber.Trim().ToLower() == request.McNumber.Trim().ToLower());
            else
                return await _dbContext
                    .Companies
                    .AnyAsync(x => x.McNumber.Trim().ToLower() == request.McNumber.Trim().ToLower());
        }
    }
}
