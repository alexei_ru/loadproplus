﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Companies.Queries.CheckUniqueCompanyMcNumber
{
    public class CheckUniqueCompanyMcNumberQuery : IRequest<bool>
    {
        public Guid? CompanyId { get; set; }
        public string McNumber { get; set; }
    }
}
