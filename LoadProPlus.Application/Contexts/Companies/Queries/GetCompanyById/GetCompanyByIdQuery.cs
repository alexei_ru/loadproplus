﻿using LoadProPlus.Application.Contexts.Companies.ViewModels;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Companies.Queries.GetCompanyById
{
    public class GetCompanyByIdQuery : IRequest<CompanyViewModel>
    {
        public Guid Id { get; set; }
    }
}
