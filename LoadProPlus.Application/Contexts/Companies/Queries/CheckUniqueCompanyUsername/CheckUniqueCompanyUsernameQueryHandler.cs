﻿using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Companies.Queries.CheckUniqueCompanyUsername
{
    public class CheckUniqueCompanyUsernameQueryHandler : IRequestHandler<CheckUniqueCompanyUsernameQuery, bool>
    {
        private readonly ILoadProPlusDbContext _dbContext;

        public CheckUniqueCompanyUsernameQueryHandler(ILoadProPlusDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(CheckUniqueCompanyUsernameQuery request, CancellationToken cancellationToken)
        {
            if (request.CompanyId.HasValue)
                return await _dbContext
                    .Companies
                    .AnyAsync(x => x.Id != request.CompanyId && x.Username.Trim().ToLower() == request.Username.Trim().ToLower());
            else
                return await _dbContext
                    .Companies
                    .AnyAsync(x => x.Username.Trim().ToLower() == request.Username.Trim().ToLower());
        }
    }
}
