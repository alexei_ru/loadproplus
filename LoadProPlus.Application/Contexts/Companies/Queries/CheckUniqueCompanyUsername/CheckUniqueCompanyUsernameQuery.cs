﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Companies.Queries.CheckUniqueCompanyUsername
{
    public class CheckUniqueCompanyUsernameQuery : IRequest<bool>
    {
        public Guid? CompanyId { get; set; }
        public string Username { get; set; }
    }
}
