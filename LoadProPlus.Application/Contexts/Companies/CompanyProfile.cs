﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Companies.Commands.UpdateCompany;
using LoadProPlus.Application.Contexts.Companies.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.Companies
{
    public class CompanyProfile : Profile
    {
        public CompanyProfile()
        {
            CreateMap<Company, CompanyViewModel>();

            CreateMap<UpdateCompanyCommand, Company>()
                .ForMember(x => x.Employees, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore())
                .ForMember(x => x.UserRefreshToken, opt => opt.Ignore());
        }
    }
}
