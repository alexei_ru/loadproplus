﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Companies.Commands.UpdateCompany
{
    public class UpdateCompanyCommandHandler : IRequestHandler<UpdateCompanyCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;

        public UpdateCompanyCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateCompanyCommand request, CancellationToken cancellationToken)
        {
            var company = await _dbContext
                .Companies
                .FirstOrDefaultAsync(x => x.Id == request.Id);

            if (company == null)
                throw new EntityNotFoundException(typeof(Company));

            if (!string.IsNullOrEmpty(request.Password))
                company.GeneratePassword(request.Password);

            _mapper.Map(request, company);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
