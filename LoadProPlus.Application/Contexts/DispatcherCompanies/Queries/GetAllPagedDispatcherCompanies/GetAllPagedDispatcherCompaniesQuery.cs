﻿using LoadProPlus.Application.Contexts.DispatcherCompanies.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;

namespace LoadProPlus.Application.Contexts.DispatcherCompanies.Queries.GetAllPagedDispatcherCompanies
{
    public class GetAllPagedDispatcherCompaniesQuery : IRequest<PagedResult<DispatcherCompanyViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
    }
}
