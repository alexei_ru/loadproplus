﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.DispatcherCompanies.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.DispatcherCompanies.Queries.GetAllPagedDispatcherCompanies
{
    public class GetAllPagedDispatcherCompaniesQueryHandler : IRequestHandler<GetAllPagedDispatcherCompaniesQuery, PagedResult<DispatcherCompanyViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetAllPagedDispatcherCompaniesQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public async Task<PagedResult<DispatcherCompanyViewModel>> Handle(GetAllPagedDispatcherCompaniesQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext
                .DispatcherCompanies
                .AsNoTracking();

            return await PagedResult<DispatcherCompanyViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
