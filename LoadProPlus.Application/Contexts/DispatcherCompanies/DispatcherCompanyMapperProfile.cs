﻿using AutoMapper;
using LoadProPlus.Application.Contexts.DispatcherCompanies.Commands.SignupDispatcherCompany;
using LoadProPlus.Application.Contexts.DispatcherCompanies.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.DispatcherCompanies.Commands
{
    public class DispatcherCompanyMapperProfile : Profile
    {
        public DispatcherCompanyMapperProfile()
        {
            CreateMap<SignupDispatcherCompanyCommand, DispatcherCompany>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.PasswordSalt, opt => opt.Ignore());

            CreateMap<DispatcherCompany, DispatcherCompanyViewModel>();
        }
    }
}
