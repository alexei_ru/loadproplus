﻿namespace LoadProPlus.Application.Contexts.DispatcherCompanies.ViewModels
{
    public class DispatcherCompanyViewModel
    {
        public string McNumber { get; set; }
        public string CompanyName { get; set; }
        public string MainAddress { get; set; }
        public string ZipCode { get; set; }
        public string PrimaryContactName { get; set; }
        public string Fax { get; set; }
        public string DotNumber { get; set; }
    }
}
