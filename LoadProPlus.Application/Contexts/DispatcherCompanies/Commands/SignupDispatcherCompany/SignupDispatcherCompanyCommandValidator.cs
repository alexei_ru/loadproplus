﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.DispatcherCompanies.Commands.SignupDispatcherCompany
{
    public class SignupDispatcherCompanyCommandValidator : AbstractValidator<SignupDispatcherCompanyCommand>
    {
        public SignupDispatcherCompanyCommandValidator()
        {
            RuleFor(x => x.Username)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(x => x.Email)
                .MaximumLength(254)
                .EmailAddress()
                .NotEmpty();

            RuleFor(x => x.Password)
                .MaximumLength(40)
                .NotEmpty();

            RuleFor(x => x.CompanyName)
                .MaximumLength(100)
                .NotEmpty();

            RuleFor(x => x.DotNumber)
                .MaximumLength(70)
                .NotEmpty();

            RuleFor(x => x.Fax)
                .MaximumLength(30)
                .NotEmpty();

            RuleFor(x => x.MainAddress)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(x => x.McNumber)
                .MaximumLength(70)
                .NotEmpty();

            RuleFor(x => x.PrimaryContactName)
                .MaximumLength(100)
                .NotEmpty();

            RuleFor(x => x.ZipCode)
                .MaximumLength(15)
                .NotEmpty();
        }
    }
}
