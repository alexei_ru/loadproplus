﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Constants;
using LoadProPlus.Application.Utils.Extensions;
using LoadProPlus.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.DispatcherCompanies.Commands.SignupDispatcherCompany
{
    public class SignupDispatcherCompanyCommandHandler : IRequestHandler<SignupDispatcherCompanyCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;

        public SignupDispatcherCompanyCommandHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(SignupDispatcherCompanyCommand request, CancellationToken cancellationToken)
        {
            var dispatcherCompany = _mapper.Map<SignupDispatcherCompanyCommand, DispatcherCompany>(request);

            dispatcherCompany.RoleId = (await _dbContext
                .Roles
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == Roles.DispatcherCompany)).Id;

            dispatcherCompany.GeneratePassword(request.Password);

            _dbContext.DispatcherCompanies.Add(dispatcherCompany);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
