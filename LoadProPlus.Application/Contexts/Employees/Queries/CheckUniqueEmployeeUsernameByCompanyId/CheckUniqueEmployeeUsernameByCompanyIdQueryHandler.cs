﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Employees.Queries.CheckUniqueEmployeeUsernameByCompanyId
{
    public class CheckUniqueEmployeeUsernameByCompanyIdQueryHandler : IRequestHandler<CheckUniqueEmployeeUsernameByCompanyIdQuery, bool>
    {
        private readonly ILoadProPlusDbContext _dbContext;

        public CheckUniqueEmployeeUsernameByCompanyIdQueryHandler(ILoadProPlusDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(CheckUniqueEmployeeUsernameByCompanyIdQuery request, CancellationToken cancellationToken)
        {
            if (request.EmployeeId.HasValue)
                return await _dbContext
                    .Employees
                    .Where(x => x.Id != request.EmployeeId.Value)
                    .AnyAsync(x => x.CompanyId == request.CompanyId && x.Username.Trim().ToLower() == request.Username.Trim().ToLower());
            else 
                return await _dbContext
                    .Employees
                    .AnyAsync(x => x.CompanyId == request.CompanyId && x.Username.Trim().ToLower() == request.Username.Trim().ToLower());
        }
    }
}
