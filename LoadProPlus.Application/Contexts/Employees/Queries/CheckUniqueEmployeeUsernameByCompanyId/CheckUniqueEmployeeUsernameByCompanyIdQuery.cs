﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Employees.Queries.CheckUniqueEmployeeUsernameByCompanyId
{
    public class CheckUniqueEmployeeUsernameByCompanyIdQuery : IRequest<bool>
    {
        public Guid CompanyId { get; set; }
        public Guid? EmployeeId { get; set; }
        public string Username { get; set; }
    }
}
