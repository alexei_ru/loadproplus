﻿using AutoMapper;
using LoadProPlus.Application.Contexts.TrailerTypes.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.TrailerTypes
{
    public class TrailerTypeMapperProfile : Profile
    {
        public TrailerTypeMapperProfile()
        {
            CreateMap<TrailerType, TrailerTypeViewModel>();
        }
    }
}
