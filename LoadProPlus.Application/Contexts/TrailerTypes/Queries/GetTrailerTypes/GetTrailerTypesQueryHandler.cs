﻿using AutoMapper;
using LoadProPlus.Application.Contexts.TrailerTypes.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.TrailerTypes.Queries.GetTrailerTypes
{
    public class GetTrailerTypesQueryHandler : IRequestHandler<GetTrailerTypesQuery, IEnumerable<TrailerTypeViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetTrailerTypesQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TrailerTypeViewModel>> Handle(GetTrailerTypesQuery request, CancellationToken cancellationToken)
        {
            var trailerTypes = await _dbContext
                .TrailerTypes
                .AsNoTracking()
                .ToListAsync();

            return _mapper.Map<IEnumerable<TrailerType>, IEnumerable<TrailerTypeViewModel>>(trailerTypes);
        }
    }
}
