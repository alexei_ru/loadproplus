﻿using LoadProPlus.Application.Contexts.TrailerTypes.ViewModels;
using MediatR;
using System.Collections.Generic;

namespace LoadProPlus.Application.Contexts.TrailerTypes.Queries.GetTrailerTypes
{
    public class GetTrailerTypesQuery : IRequest<IEnumerable<TrailerTypeViewModel>>
    {
    }
}
