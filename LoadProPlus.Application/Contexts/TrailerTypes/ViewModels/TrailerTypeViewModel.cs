﻿using System;

namespace LoadProPlus.Application.Contexts.TrailerTypes.ViewModels
{
    public class TrailerTypeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
