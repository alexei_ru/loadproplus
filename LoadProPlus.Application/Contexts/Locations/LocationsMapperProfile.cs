﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Locations.ViewModels;
using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Contexts.Locations
{
	public class LocationsMapperProfile : Profile
	{
		public LocationsMapperProfile()
		{
			
			CreateMap<State, StateViewModel>();
			CreateMap<City, CityViewModel>()
				.ForMember(m => m.State, opt => opt.MapFrom( (src => src.State)));
		}
	}
}
