﻿using System;

namespace LoadProPlus.Application.Contexts.Locations.ViewModels
{
    public class CityViewModel
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string County { get; set; }
		public float Latitude { get; set; }
		public float Longitude { get; set; }
		public StateViewModel State { get;set;}
	}
}
