﻿using System;

namespace LoadProPlus.Application.Contexts.Locations.ViewModels
{
    public class StateViewModel
    {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }
		public float Latitude { get; set; }
		public float Longitude { get; set; }
	}
}
