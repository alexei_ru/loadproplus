﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Locations.ViewModels;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Locations.Queries.GetStates
{
	public class GetAllStatesQueryHandler : IRequestHandler<GetAllStatesQuery, IEnumerable<StateViewModel>>
	{
		private readonly IMapper _mapper;
		private readonly ILoadProPlusDbContext _dbContext;

		public GetAllStatesQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
		{
			_dbContext = dbContext;
			_mapper = mapper;
		}

		public async Task<IEnumerable<StateViewModel>> Handle(GetAllStatesQuery request, CancellationToken cancellationToken)
		{
			var states = await _dbContext
                .States
                .AsNoTracking()
                .ToListAsync();

			return _mapper.Map<IEnumerable<StateViewModel>>(states);
		}
	}
}
