﻿using LoadProPlus.Application.Contexts.Locations.ViewModels;
using MediatR;
using System.Collections.Generic;

namespace LoadProPlus.Application.Contexts.Locations.Queries.GetStates
{
    public class GetAllStatesQuery : IRequest<IEnumerable<StateViewModel>>
	{
		
	}
}
