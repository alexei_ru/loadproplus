﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Locations.ViewModels;
using LoadProPlus.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Contexts.Locations.Queries.GetCitiesByState
{
	class GetCitiesByStateQueryHandler : IRequestHandler<GetCitiesByStateQuery, IEnumerable<CityViewModel>>
	{
		private readonly IMapper _mapper;
		private readonly ILoadProPlusDbContext _dbContext;

		public GetCitiesByStateQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper)
		{
			_dbContext = dbContext;
			_mapper = mapper;
		}

		public async Task<IEnumerable<CityViewModel>> Handle(GetCitiesByStateQuery request, CancellationToken cancellationToken)
		{
			var cities = await _dbContext.Cities
				.Include(i => i.State)
				.AsNoTracking()
				.Where(w => w.StateId == request.StateId)
                .ToListAsync();

			return _mapper.Map<IEnumerable<CityViewModel>>(cities);
		}
	}
}
