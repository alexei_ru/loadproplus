﻿using LoadProPlus.Application.Contexts.Locations.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace LoadProPlus.Application.Contexts.Locations.Queries.GetCitiesByState
{
	public class GetCitiesByStateQuery : IRequest<IEnumerable<CityViewModel>>
	{
		public Guid StateId { get; set; }
	}
}
