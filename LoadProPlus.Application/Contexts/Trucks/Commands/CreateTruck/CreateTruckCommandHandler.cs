﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Trucks.Commands.CreateTruck
{
	public class CreateTruckCommandHandler : IRequestHandler<CreateTruckCommand, TruckViewModel>
	{
		private readonly ITrucksBroadcaster _trucksBroadcaster;
		private readonly ILoadProPlusDbContext _dbContext;
		private readonly IMapper _mapper;
		private readonly ICurrentUser _currentUser;

		public CreateTruckCommandHandler(
			ILoadProPlusDbContext dbContext,
			IMapper mapper,
			ICurrentUser currentUser,
			ITrucksBroadcaster trucksBroadcaster)
		{
			_dbContext = dbContext;
			_mapper = mapper;
			_currentUser = currentUser;
			_trucksBroadcaster = trucksBroadcaster;
		}

		public async Task<TruckViewModel> Handle(CreateTruckCommand request, CancellationToken cancellationToken)
		{
			var dispatcherId = new Guid(_currentUser.Id);
			var truck = _mapper.Map<CreateTruckCommand, Truck>(request);
			truck.DispatcherId = dispatcherId;

			// Check if Secondary Driver exists, if it is null, only the Principal Driver is created
			if (request.SecondaryDriver == null)
				truck.Drivers.Add(_mapper.Map<DriverDto, Driver>(request.PrincipalDriver));
			else
			{
				truck.Drivers = new List<Driver>
				{
					_mapper.Map<DriverDto, Driver>(request.PrincipalDriver),
					_mapper.Map<DriverDto, Driver>(request.SecondaryDriver)
				};
			}

			_dbContext.Trucks.Add(truck);
			await _dbContext.SaveChangesAsync(cancellationToken);

			var addedTruck = await _dbContext
				.Trucks
				.AsNoTracking()
				.Include(x => x.Drivers)
				.Include(x => x.TrailerType)
				.FirstOrDefaultAsync(f => f.Id == truck.Id);

			var truckViewModel = _mapper.Map<Truck, TruckViewModel>(addedTruck);

			await _trucksBroadcaster.BroadcastTruck(truckViewModel);

			return truckViewModel;
		}
	}
}
