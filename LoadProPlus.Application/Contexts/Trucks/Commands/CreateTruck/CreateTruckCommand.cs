﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Trucks.Commands.CreateTruck
{
    public class CreateTruckCommand : IRequest<TruckViewModel>
    {
        public string TruckNumber { get; set; }
        public string TrailerNumber { get; set; }
        public Guid TrailerTypeId { get; set; }
        public DriverDto PrincipalDriver { get; set; }
        public DriverDto SecondaryDriver { get; set; }
    }
}
