﻿using FluentValidation;

namespace LoadProPlus.Application.Contexts.Trucks.Commands.CreateTruck
{
    public class CreateTruckCommandValidator : AbstractValidator<CreateTruckCommand>
    {
        public CreateTruckCommandValidator()
        {
            RuleFor(x => x.TrailerNumber)
                .MaximumLength(30)
                .NotEmpty();

            RuleFor(x => x.TruckNumber)
                .MaximumLength(30)
                .NotEmpty();

            RuleFor(x => x.TrailerTypeId)
                .NotEmpty();


            // Principal driver
            RuleFor(x => x.PrincipalDriver.Email)
                .NotEmpty()
                .EmailAddress()
                .MaximumLength(254);

            RuleFor(x => x.PrincipalDriver.FirstName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.PrincipalDriver.Surname)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.PrincipalDriver.PhoneNumber)
                .NotEmpty()
                .MaximumLength(30);

            //Secondary driver
            RuleFor(x => x.SecondaryDriver.Email)
                .NotEmpty()
                .EmailAddress()
                .MaximumLength(254)
                .When(x => x.SecondaryDriver != null);

            RuleFor(x => x.SecondaryDriver.FirstName)
                .NotEmpty()
                .MaximumLength(50)
                .When(x => x.SecondaryDriver != null);

            RuleFor(x => x.SecondaryDriver.Surname)
                .NotEmpty()
                .MaximumLength(50)
                .When(x => x.SecondaryDriver != null);

            RuleFor(x => x.SecondaryDriver.PhoneNumber)
                .NotEmpty()
                .MaximumLength(30)
                .When(x => x.SecondaryDriver != null);
        }
    }
}
