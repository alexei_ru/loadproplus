﻿using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Trucks.Commands.DeleteTruck
{
    public class DeleteTruckCommand : IRequest
    {
        public Guid TruckId { get; set; }
    }
}
