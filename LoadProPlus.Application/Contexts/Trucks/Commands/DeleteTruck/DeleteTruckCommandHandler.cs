﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Trucks.Commands.DeleteTruck
{
    public class DeleteTruckCommandHandler : IRequestHandler<DeleteTruckCommand, Unit>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ICurrentUser _currentUser;

        public DeleteTruckCommandHandler(ILoadProPlusDbContext dbContext, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _currentUser = currentUser;
        }

        public async Task<Unit> Handle(DeleteTruckCommand request, CancellationToken cancellationToken)
        {
            var dispatcherId = new Guid(_currentUser.Id);
            var truck = await _dbContext
                .Trucks
                .AsNoTracking()
                .Include(x => x.Drivers)
                .FirstOrDefaultAsync(x => x.Id == request.TruckId && x.DispatcherId == dispatcherId);

            if (truck == null)
                throw new EntityNotFoundException(typeof(Truck));

            _dbContext.Drivers.RemoveRange(truck.Drivers);
            _dbContext.Trucks.Remove(truck);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
