﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Interfaces.Broadcasters;
using LoadProPlus.Application.Utils.Helpers;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Trucks.Commands.UpdateTruck
{
	public class UpdateTruckCommandHandler : IRequestHandler<UpdateTruckCommand, Unit>
	{
		private readonly ITrucksBroadcaster _trucksBroadcaster;
		private readonly ILoadProPlusDbContext _dbContext;
		private readonly IMapper _mapper;
		private readonly ICurrentUser _currentUser;

		public UpdateTruckCommandHandler(
			ILoadProPlusDbContext dbContext,
			IMapper mapper,
			ICurrentUser currentUser,
			ITrucksBroadcaster trucksBroadcaster)
		{
			_dbContext = dbContext;
			_mapper = mapper;
			_currentUser = currentUser;
			_trucksBroadcaster = trucksBroadcaster;
		}

		public async Task<Unit> Handle(UpdateTruckCommand request, CancellationToken cancellationToken)
		{
			var dispatcherId = new Guid(_currentUser.Id);
			var truck = await _dbContext
				.Trucks
				.Include(x => x.Drivers)
				.FirstOrDefaultAsync(x => x.Id == request.TruckId && x.DispatcherId == dispatcherId);

			if (truck == null)
				throw new EntityNotFoundException(typeof(Truck));

			_mapper.Map(request, truck);
			_mapper.Map(request.PrincipalDriver, truck.Drivers.First());

			UpdateSecondaryDriver(truck.Drivers, request.SecondaryDriver);

			await _dbContext.SaveChangesAsync(cancellationToken);

			var updatedTruck = await _dbContext
				.Trucks
				.AsNoTracking()
				.Include(x => x.Drivers)
				.Include(x => x.TrailerType)
				.FirstOrDefaultAsync(f => f.Id == truck.Id);

			var truckViewModel = _mapper.Map<Truck, TruckViewModel>(updatedTruck);

			await _trucksBroadcaster.BroadcastTruck(truckViewModel);

			return Unit.Value;
		}

		private void UpdateSecondaryDriver(ICollection<Driver> drivers, DriverDto secondaryDriver)
		{
			// Gavnacod
			int driversCount = drivers.Count();

			if (driversCount == DriversCount.SingleDriver && secondaryDriver != null)
				drivers.Add(_mapper.Map<DriverDto, Driver>(secondaryDriver));

			if (driversCount == DriversCount.BothDrivers && secondaryDriver != null)
				_mapper.Map(secondaryDriver, drivers.Last());

			if (driversCount == DriversCount.BothDrivers && secondaryDriver == null)
				_dbContext.Drivers.Remove(drivers.Last());
		}
	}
}
