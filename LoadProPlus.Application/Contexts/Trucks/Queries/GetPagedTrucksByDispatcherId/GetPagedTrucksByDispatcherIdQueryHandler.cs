﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Trucks.Queries.GetPagedTrucksByDispatcherId
{
    public class GetPagedTrucksByDispatcherIdQueryHandler : IRequestHandler<GetPagedTrucksByDispatcherIdQuery, PagedResult<TruckViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly ICurrentUser _currentUser;
        private readonly IConfigurationProvider _configurationProvider;

        public GetPagedTrucksByDispatcherIdQueryHandler(ILoadProPlusDbContext dbContext, ICurrentUser currentUser, IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _currentUser = currentUser;
            _configurationProvider = configurationProvider;
        }

        public async Task<PagedResult<TruckViewModel>> Handle(GetPagedTrucksByDispatcherIdQuery request, CancellationToken cancellationToken)
        {
            var dispatcherId = new Guid(_currentUser.Id);
            var query = _dbContext
                .Trucks
                .AsNoTracking()
                .Where(x => x.DispatcherId == dispatcherId);

            return await PagedResult<TruckViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
