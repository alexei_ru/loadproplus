﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Trucks.Queries.GetPagedTrucksByDispatcherId
{
    public class GetPagedTrucksByDispatcherIdQuery : IRequest<PagedResult<TruckViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
        public string AccessToken { get; set; }
    }
}
