﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;

namespace LoadProPlus.Application.Contexts.Trucks.Queries.GetPagedAvailableTrucks
{
    public class GetPagedAvailableTrucksQuery : IRequest<PagedResult<TruckViewModel>>
    {
        public PageViewModel PageViewModel { get; set; }
    }
}
