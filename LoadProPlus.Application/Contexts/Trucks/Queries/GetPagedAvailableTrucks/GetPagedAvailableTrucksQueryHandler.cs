﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Application.Utils.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Trucks.Queries.GetPagedAvailableTrucks
{
    public class GetPagedAvailableTrucksQueryHandler : IRequestHandler<GetPagedAvailableTrucksQuery, PagedResult<TruckViewModel>>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetPagedAvailableTrucksQueryHandler(ILoadProPlusDbContext dbContext, IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public async Task<PagedResult<TruckViewModel>> Handle(GetPagedAvailableTrucksQuery request, CancellationToken cancellationToken)
        {
            var query = _dbContext
                .Trucks
                .AsNoTracking();

            return await PagedResult<TruckViewModel>.From(query, request.PageViewModel, _configurationProvider);
        }
    }
}
