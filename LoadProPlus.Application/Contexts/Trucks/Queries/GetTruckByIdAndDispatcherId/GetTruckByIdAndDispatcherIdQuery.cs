﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using MediatR;
using System;

namespace LoadProPlus.Application.Contexts.Trucks.Queries.GetTruckByIdAndDispatcherId
{
    public class GetTruckByIdAndDispatcherIdQuery : IRequest<EditableTruckViewModel>
    {
        public Guid TruckId { get; set; }
        public string AccessToken { get; set; }
    }
}
