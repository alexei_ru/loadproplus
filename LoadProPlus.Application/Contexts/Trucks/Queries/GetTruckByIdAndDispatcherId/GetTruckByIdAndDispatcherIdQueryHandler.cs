﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LoadProPlus.Application.Contexts.Trucks.Queries.GetTruckByIdAndDispatcherId
{
    public class GetTruckByIdAndDispatcherIdQueryHandler : IRequestHandler<GetTruckByIdAndDispatcherIdQuery, EditableTruckViewModel>
    {
        private readonly ILoadProPlusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetTruckByIdAndDispatcherIdQueryHandler(ILoadProPlusDbContext dbContext, IMapper mapper, ICurrentUser currentUser)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUser = currentUser;
        }

        public async Task<EditableTruckViewModel> Handle(GetTruckByIdAndDispatcherIdQuery request, CancellationToken cancellationToken)
        {
            var dispatcherId = new Guid(_currentUser.Id);
            var truck = await _dbContext
                .Trucks
                .AsNoTracking()
                .Include(x => x.Drivers)
                .Include(x => x.TrailerType)
                .FirstOrDefaultAsync(x => x.Id == request.TruckId && x.DispatcherId == dispatcherId);

            if (truck == null)
                throw new EntityNotFoundException(typeof(Truck));

            return _mapper.Map<Truck, EditableTruckViewModel>(truck);
        }
    }
}
