﻿namespace LoadProPlus.Application.Contexts.Trucks.ViewModels
{
    public class DriverDto
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
