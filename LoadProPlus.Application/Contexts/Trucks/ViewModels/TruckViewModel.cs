﻿using System;

namespace LoadProPlus.Application.Contexts.Trucks.ViewModels
{
    public class TruckViewModel
    {
        public Guid Id { get; set; }
        public string TruckNumber { get; set; }
        public string TrailerNumber { get; set; }
        public string TrailerType { get; set; }
        public DriverDto PrincipalDriver { get; set; }
        public DriverDto SecondaryDriver { get; set; }
    }
}
