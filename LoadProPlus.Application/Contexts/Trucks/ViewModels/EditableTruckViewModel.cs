﻿using System;

namespace LoadProPlus.Application.Contexts.Trucks.ViewModels
{
    public class EditableTruckViewModel
    {
        public string TruckNumber { get; set; }
        public string TrailerNumber { get; set; }
        public Guid TrailerTypeId { get; set; }
        public DriverDto PrincipalDriver { get; set; }
        public DriverDto SecondaryDriver { get; set; }
    }
}
