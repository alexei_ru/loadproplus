﻿using AutoMapper;
using LoadProPlus.Application.Contexts.Trucks.Commands.CreateTruck;
using LoadProPlus.Application.Contexts.Trucks.Commands.UpdateTruck;
using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using LoadProPlus.Application.Utils.Helpers;
using LoadProPlus.Domain.Entities;
using System.Linq;

namespace LoadProPlus.Application.Contexts.Trucks
{
    public class TrucksMapperProfile : Profile
    {
        public TrucksMapperProfile()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Driver, DriverDto>();
            });

            CreateMap<CreateTruckCommand, Truck>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Dispatcher, opt => opt.Ignore())
                .ForMember(x => x.Drivers, opt => opt.Ignore())
                .ForMember(x => x.TrailerType, opt => opt.Ignore());

            CreateMap<UpdateTruckCommand, Truck>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.TruckId))
                .ForMember(x => x.Dispatcher, opt => opt.Ignore())
                .ForMember(x => x.Drivers, opt => opt.Ignore())
                .ForMember(x => x.TrailerType, opt => opt.Ignore());

            CreateMap<Truck, TruckViewModel>()
                .ForMember(x => x.TrailerType, opt => opt.MapFrom(x => x.TrailerType.Name))
                .ForMember(x => x.PrincipalDriver, opt => opt.MapFrom(x => Mapper.Map<DriverDto>(x.Drivers.First())))
                .ForMember(x => x.SecondaryDriver, opt => opt.MapFrom(x => x.Drivers.Count() == DriversCount.SingleDriver ? null : Mapper.Map<DriverDto>(x.Drivers.Last())));

            CreateMap<Truck, EditableTruckViewModel>()
                .ForMember(x => x.PrincipalDriver, opt => opt.MapFrom(x => Mapper.Map<DriverDto>(x.Drivers.First())))
                .ForMember(x => x.SecondaryDriver, opt => opt.MapFrom(x => x.Drivers.Count() == DriversCount.SingleDriver ? null : Mapper.Map<DriverDto>(x.Drivers.Last())));

            // TO DO: move this in DriverMapperProfile
            CreateMap<DriverDto, Driver>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Truck, opt => opt.Ignore());
        }
    }
}
