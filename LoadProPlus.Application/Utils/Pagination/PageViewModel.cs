﻿namespace LoadProPlus.Application.Utils.Pagination
{
    public class PageViewModel
    {
        public int PageSize { get; set; } = 10;
        public int Page { get; set; } = 1;
        public string Sort { get; set; }
        public string Filter { get; set; }
        public string FilterFields { get; set; }
        public bool Ascending { get; set; }
    }
}
