﻿namespace LoadProPlus.Application.Utils.Helpers
{
    public static class DriversCount
    {
        public static int SingleDriver = 1;
        public static int BothDrivers = 2;
    }
}
