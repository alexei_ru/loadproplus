﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadProPlus.Application.Utils.Helpers
{
	public class TokenSettings
	{
		public string AccessTokenExpirationTime { get; set; }
		public string RefreshTokenExpirationTime { get; set; }
	}
}
