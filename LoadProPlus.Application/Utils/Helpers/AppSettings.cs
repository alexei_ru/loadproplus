﻿namespace LoadProPlus.Application.Utils.Helpers
{
	public class AppSettings
	{
		public string Secret { get; set; }
		public TokenSettings TokenSettings { get; set; }
	}
}
