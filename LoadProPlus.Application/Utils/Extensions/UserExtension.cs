﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Domain.Exceptions;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace LoadProPlus.Application.Utils.Extensions
{
    internal static class PasswordHelper
    {
        public static void GeneratePassword(this User user, string password)
        {
            using (var hmac = new HMACSHA512())
            {
                user.PasswordSalt = hmac.Key;
                user.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        public static void PasswordMatch(this User user, string password)
        {
            using (var hmac = new HMACSHA512(user.PasswordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                if (!Enumerable.SequenceEqual(computedHash, user.PasswordHash))
                {
                    throw new IncorrectPasswordException();
                }
            }
        }
    }
}
