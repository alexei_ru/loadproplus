﻿using LoadProPlus.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadProPlus.Application.Utils.Extensions
{
	public static class UserRefreshTokenExtensions
	{
		public static bool IsExpired(this UserRefreshToken token)
		{
			return DateTime.UtcNow > token.ExpirationTime;
		}
	}
}
