﻿using LoadProPlus.Application.Interfaces.Utils;
using LoadProPlus.Application.Utils.Helpers;
using LoadProPlus.Domain.Exceptions;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LoadProPlus.Application.Utils.Tokens
{
	public class JwtHelper : IJwtHelpers
	{
		private readonly AppSettings _appSettings;

		public JwtHelper(IOptions<AppSettings> appSettings)
		{
			_appSettings = appSettings.Value;
		}

		public ClaimsPrincipal GetClaimsFromJwt(string jwt)
		{
			var jwtSecurityHandler = new JwtSecurityTokenHandler();
			var tokenValidationParameters = GetTokenValidationParameters();
			ClaimsPrincipal claimsPrincipal;

			try
			{
				claimsPrincipal = jwtSecurityHandler.ValidateToken(jwt, tokenValidationParameters, out var rawJwt);
			}
			catch (SecurityTokenValidationException)
			{
				throw new InvalidTokenException();
			}
			catch (Exception)
			{
				throw;
			}

			return claimsPrincipal;
		}

		public TokenValidationParameters GetTokenValidationParameters()
		{
			return new TokenValidationParameters
			{
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.Secret)),
				ValidateIssuer = false,
				ValidateAudience = false,
				ValidateLifetime = false
			};
		}
	}
}
