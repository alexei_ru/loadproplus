﻿namespace LoadProPlus.Application.Utils.Constants
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string BrokerCompany = "BrokerCompany";
        public const string DispatcherCompany = "DispatcherCompany";
        public const string Broker = "Broker";
        public const string Dispatcher = "Dispatcher";
    }
}
