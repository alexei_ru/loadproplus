﻿namespace LoadProPlus.Application.Models.Shared
{
    public class SignInResultViewModel
	{
		public string AccessToken { get; set; }
		public string RefreshToken { get; set; }
	}
}
