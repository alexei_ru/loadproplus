﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Interfaces.Broadcasters
{
	public interface ITrucksBroadcaster
	{
		Task BroadcastTruck(TruckViewModel truck);
	}
}
