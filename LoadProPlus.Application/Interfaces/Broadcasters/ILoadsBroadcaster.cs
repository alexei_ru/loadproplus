﻿using LoadProPlus.Application.Contexts.Loads.ViewModels;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Interfaces.Broadcasters
{
	public interface ILoadsBroadcaster
	{
		Task BroadcastLoad(LoadViewModel load);
	}
}
