﻿using LoadProPlus.Domain.Entities;

namespace LoadProPlus.Application.Interfaces
{
	public interface ITokenGenerator
	{
		string CreateToken(User user);
		string GenerateRefreshToken(int size = 32);
	}
}
