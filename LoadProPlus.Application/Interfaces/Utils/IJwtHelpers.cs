﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace LoadProPlus.Application.Interfaces.Utils
{
	public interface IJwtHelpers
	{
		ClaimsPrincipal GetClaimsFromJwt(string jwt);
		TokenValidationParameters GetTokenValidationParameters();
	}
}
