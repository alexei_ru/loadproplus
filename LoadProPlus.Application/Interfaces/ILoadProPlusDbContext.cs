﻿using LoadProPlus.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Interfaces
{
    public interface ILoadProPlusDbContext
    {
        DbSet<State> States { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<Truck> Trucks { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<BrokerCompany> BrokerCompanies { get; set; }
        DbSet<Broker> Brokers { get; set; }
        DbSet<DispatcherCompany> DispatcherCompanies { get; set; }
        DbSet<Dispatcher> Dispatchers { get; set; }
        DbSet<TrailerType> TrailerTypes { get; set; }
        DbSet<Driver> Drivers { get; set; }
        DbSet<UserRefreshToken> UserRefreshTokens { get; set; }
        DbSet<Load> Loads { get; set; }
        DbSet<Company> Companies { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<LoadStatus> LoadStatuses { get; set; }
		Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
