﻿using LoadProPlus.Application.Contexts.Trucks.ViewModels;
using System.Threading.Tasks;

namespace LoadProPlus.Application.Interfaces.Hubs
{
	public interface ITrucksHub
	{
		Task BroadcastTruck(TruckViewModel truck);
	}
}
