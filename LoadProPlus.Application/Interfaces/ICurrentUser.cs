﻿namespace LoadProPlus.Application.Interfaces
{
    public interface ICurrentUser
    {
        string Id { get; }
    }
}
