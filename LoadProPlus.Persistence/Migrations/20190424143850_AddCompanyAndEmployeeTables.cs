﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class AddCompanyAndEmployeeTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Trucks",
                newName: "Surname");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Username = table.Column<string>(maxLength: 50, nullable: false),
                    PasswordHash = table.Column<byte[]>(nullable: false),
                    PasswordSalt = table.Column<byte[]>(nullable: false),
                    Email = table.Column<string>(maxLength: 320, nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    McNumber = table.Column<string>(maxLength: 70, nullable: true),
                    CompanyName = table.Column<string>(maxLength: 100, nullable: true),
                    MainAddress = table.Column<string>(maxLength: 200, nullable: true),
                    ZipCode = table.Column<string>(maxLength: 15, nullable: true),
                    PrimaryContactName = table.Column<string>(maxLength: 100, nullable: true),
                    Fax = table.Column<string>(maxLength: 30, nullable: true),
                    DotNumber = table.Column<string>(maxLength: 70, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Surname = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 30, nullable: true),
                    BrokerCompanyId = table.Column<Guid>(nullable: true),
                    DispatcherCompanyId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Users_BrokerCompanyId",
                        column: x => x.BrokerCompanyId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Users_DispatcherCompanyId",
                        column: x => x.DispatcherCompanyId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_BrokerCompanyId",
                table: "Users",
                column: "BrokerCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DispatcherCompanyId",
                table: "Users",
                column: "DispatcherCompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.RenameColumn(
                name: "Surname",
                table: "Trucks",
                newName: "LastName");
        }
    }
}
