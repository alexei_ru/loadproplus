﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class AddSeedTruckTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TruckTypeId",
                table: "Trucks",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "TruckTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 10, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TruckTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_TruckTypeId",
                table: "Trucks",
                column: "TruckTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trucks_TruckTypes_TruckTypeId",
                table: "Trucks",
                column: "TruckTypeId",
                principalTable: "TruckTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trucks_TruckTypes_TruckTypeId",
                table: "Trucks");

            migrationBuilder.DropTable(
                name: "TruckTypes");

            migrationBuilder.DropIndex(
                name: "IX_Trucks_TruckTypeId",
                table: "Trucks");

            migrationBuilder.DropColumn(
                name: "TruckTypeId",
                table: "Trucks");
        }
    }
}
