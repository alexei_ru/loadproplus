﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class SeedStatesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO States (Id, Code, Name, Latitude, Longitude) VALUES
                    ('9e1171b5-8505-4178-9adf-fe89b48e01c2', 'AL', 'Alabama', 32.806671, -86.791130),
                    ('af70254c-ad86-4e8f-9d08-c90404a9a771', 'AK', 'Alaska', 61.370716, -152.404419),
                    ('58a7bd90-3e88-4d6b-bed6-45ca4143d9da', 'AZ', 'Arizona', 33.729759, -111.431221),
                    ('970396d4-787e-43e2-a87a-37ee22d819c7', 'AR', 'Arkansas', 34.969704, -92.373123),
                    ('19fb8f47-68ca-413f-b87c-363f1eb9f2c6', 'CA', 'California', 36.116203, -119.681564),
                    ('6a885a4f-b230-4cd3-ac7c-da2917fd7c3a', 'CO', 'Colorado', 39.059811, -105.311104),
                    ('e17c853c-6cae-418b-9d19-dbaf7d59311c', 'CT', 'Connecticut', 41.597782, -72.755371),
                    ('64e8250f-8add-43c6-972d-1d5bb2acc14b', 'DE', 'Delaware', 39.318523, -75.507141),
                    ('5cfd15b2-a4d2-4c78-911f-9afd985c3f21', 'DC', 'District of Columbia', 38.897438, -77.026817),
                    ('ea871b67-f29b-4435-bff0-2d172d7e165f', 'FL', 'Florida', 27.766279, -81.686783),
                    ('2f00d18b-7545-4cae-b0bc-bf20e99dbd9c', 'GA', 'Georgia', 33.040619, -83.643074),
                    ('7be84ad9-072b-45c5-9bab-8d31b374eb09', 'HI', 'Hawaii', 21.094318, -157.498337),
                    ('162469a4-dc9d-4200-bd48-85ab9f06e4ba', 'ID', 'Idaho', 44.240459, -114.478828),
                    ('aeddff11-5c7a-47b2-a5dd-c7145e05f24e', 'IL', 'Illinois', 40.349457, -88.986137),
                    ('ec197cca-cbf9-43ca-b88f-90ee785924a8', 'IN', 'Indiana', 39.849426, -86.258278),
                    ('17d151f4-137c-4712-b47a-bc2003a1d6ad', 'IA', 'Iowa', 42.011539, -93.210526),
                    ('033ffc31-0396-4a10-a53e-b6287d2852a6', 'KS', 'Kansas', 38.526600, -96.726486),
                    ('92e7d43f-414b-4319-a246-318f04521f4e', 'KY', 'Kentucky', 37.668140, -84.670067),
                    ('0272c5fc-5a1a-472c-9595-9bd006ad9dce', 'LA', 'Louisiana', 31.169546, -91.867805),
                    ('97d5fdda-4b18-40f4-ba91-b9650844514b', 'ME', 'Maine', 44.693947, -69.381927),
                    ('7abb45d1-fb3a-46f9-9dae-71faef5d2f9d', 'MD', 'Maryland', 39.063946, -76.802101),
                    ('32e50437-0ecd-4897-8b1c-646df49fa8a2', 'MA', 'Massachusetts', 42.230171, -71.530106),
                    ('04b2c3ed-e6b0-4acc-a865-89e3ebeed8e5', 'MI', 'Michigan', 43.326618, -84.536095),
                    ('f1c1d3f2-293f-4c04-9560-3d88da65a26f', 'MN', 'Minnesota', 45.694454, -93.900192),
                    ('5017a088-6c03-4497-9e44-c3d8603a12b3', 'MS', 'Mississippi', 32.741646, -89.678696),
                    ('392d3076-651e-48fe-b036-f9c2e99c5758', 'MO', 'Missouri', 38.456085, -92.288368),
                    ('26712ee0-490b-4bff-a900-073bcf1d471f', 'MT', 'Montana', 46.921925, -110.454353),
                    ('39caae99-c0a3-4891-9603-b0b40952699d', 'NE', 'Nebraska', 41.125370, -98.268082),
                    ('c10eee2d-5cac-4378-a8a5-c6d5105826f4', 'NV', 'Nevada', 38.313515, -117.055374),
                    ('02dc6d07-2a5e-44a2-8a75-bbeb6f8c5f50', 'NH', 'New Hampshire', 43.452492, -71.563896),
                    ('265e9573-3f9c-41e9-af1a-ad2ad837f02f', 'NJ', 'New Jersey', 40.298904, -74.521011),
                    ('1a9b7f89-ee8f-463a-9ef4-b783ff3b571f', 'NM', 'New Mexico', 34.840515, -106.248482),
                    ('d922287f-d4a0-4bce-b598-f2d262af4fb3', 'NY', 'New York', 42.165726, -74.948051),
                    ('c1a3c556-7e1d-4fda-abef-f0d837dcbf5b', 'NC', 'North Carolina', 35.630066, -79.806419),
                    ('735d01de-d6f5-4818-8f8b-0ed949eb5765', 'ND', 'North Dakota', 47.528912, -99.784012),
                    ('3a234025-cc23-4127-a146-3eece94ec33a', 'OH', 'Ohio', 40.388783, -82.764915),
                    ('d8074cae-4360-4e41-845e-4a115bdb6c65', 'OK', 'Oklahoma', 35.565342, -96.928917),
                    ('081d6d88-827d-4655-9596-6c696145ba27', 'OR', 'Oregon', 44.572021, -122.070938),
                    ('0160f6e4-86dd-48f0-80d1-a982771aaf1a', 'PA', 'Pennsylvania', 40.590752, -77.209755),
                    ('4ef007e1-738d-428b-a208-4dd8c4465902', 'PR', 'Puerto Rico', 18.24914, -66.62804),
                    ('5b961c88-c115-4b50-9728-180ad0f2ec74', 'RI', 'Rhode Island', 41.680893, -71.511780),
                    ('54983f5d-5e15-47b1-9f3d-65578fe81f76', 'SC', 'South Carolina', 33.856892, -80.945007),
                    ('aa9d0450-e8dc-46af-9f53-e0436d3092e6', 'SD', 'South Dakota', 44.299782, -99.438828),
                    ('ad6910f7-f1b0-46b2-bca7-e2f485d4ab2e', 'TN', 'Tennessee', 35.747845, -86.692345),
                    ('2df05806-9585-4ddd-b07f-e9732ea3e309', 'TX', 'Texas', 31.054487, -97.563461),
                    ('d91ecdcf-749d-48fc-b00e-a4976dde6a7e', 'UT', 'Utah', 40.150032, -111.862434),
                    ('314b3c70-923b-4d3e-a011-79bac315c202', 'VT', 'Vermont', 44.045876, -72.710686),
                    ('5041d510-2b6e-48e9-96b2-ab56554e6e39', 'VA', 'Virginia', 37.769337, -78.169968),
                    ('a4c66933-a845-45df-9be4-29b22a0393d0', 'WA', 'Washington', 47.400902, -121.490494),
                    ('f96ca535-b44a-4c84-a1d4-e33946fddccb', 'WV', 'West Virginia', 38.491226, -80.954453),
                    ('7a70f82d-f55b-4674-9c8a-695c0863fc65', 'WI', 'Wisconsin', 44.268543, -89.616508),
                    ('4cab3473-d549-49df-8af4-ef6aed5f4f9d', 'WY', 'Wyoming', 42.755966, -107.302490);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM States");
        }
    }
}
