﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class FixedSomeLoadColumnsName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_Cities_DelieveryId",
                table: "Loads");

            migrationBuilder.RenameColumn(
                name: "DelieveryId",
                table: "Loads",
                newName: "DeliveryId");

            migrationBuilder.RenameColumn(
                name: "DelieveryDateTime",
                table: "Loads",
                newName: "DeliveryDateTime");

            migrationBuilder.RenameColumn(
                name: "Comodity",
                table: "Loads",
                newName: "Commodity");

            migrationBuilder.RenameColumn(
                name: "Comments",
                table: "Loads",
                newName: "AdditionalInfo");

            migrationBuilder.RenameIndex(
                name: "IX_Loads_DelieveryId",
                table: "Loads",
                newName: "IX_Loads_DeliveryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_Cities_DeliveryId",
                table: "Loads",
                column: "DeliveryId",
                principalTable: "Cities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_Cities_DeliveryId",
                table: "Loads");

            migrationBuilder.RenameColumn(
                name: "DeliveryId",
                table: "Loads",
                newName: "DelieveryId");

            migrationBuilder.RenameColumn(
                name: "DeliveryDateTime",
                table: "Loads",
                newName: "DelieveryDateTime");

            migrationBuilder.RenameColumn(
                name: "Commodity",
                table: "Loads",
                newName: "Comodity");

            migrationBuilder.RenameColumn(
                name: "AdditionalInfo",
                table: "Loads",
                newName: "Comments");

            migrationBuilder.RenameIndex(
                name: "IX_Loads_DeliveryId",
                table: "Loads",
                newName: "IX_Loads_DelieveryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_Cities_DelieveryId",
                table: "Loads",
                column: "DelieveryId",
                principalTable: "Cities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
