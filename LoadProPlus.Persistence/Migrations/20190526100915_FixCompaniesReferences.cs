﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class FixCompaniesReferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_Users_BrokerCompanyId",
                table: "Loads");

            migrationBuilder.DropForeignKey(
                name: "FK_Trucks_Users_DispatcherCompanyId",
                table: "Trucks");

            migrationBuilder.DropIndex(
                name: "IX_Trucks_DispatcherCompanyId",
                table: "Trucks");

            migrationBuilder.DropIndex(
                name: "IX_Loads_BrokerCompanyId",
                table: "Loads");

            migrationBuilder.DropColumn(
                name: "DispatcherCompanyId",
                table: "Trucks");

            migrationBuilder.DropColumn(
                name: "BrokerCompanyId",
                table: "Loads");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DispatcherCompanyId",
                table: "Trucks",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "BrokerCompanyId",
                table: "Loads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_DispatcherCompanyId",
                table: "Trucks",
                column: "DispatcherCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Loads_BrokerCompanyId",
                table: "Loads",
                column: "BrokerCompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_Users_BrokerCompanyId",
                table: "Loads",
                column: "BrokerCompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trucks_Users_DispatcherCompanyId",
                table: "Trucks",
                column: "DispatcherCompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
