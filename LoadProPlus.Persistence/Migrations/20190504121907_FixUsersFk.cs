﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class FixUsersFk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_BrokerCompanyId",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_DispatcherCompanyId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_BrokerCompanyId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_RoleId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "BrokerCompanyId",
                table: "Users");

            migrationBuilder.RenameColumn(
                name: "DispatcherCompanyId",
                table: "Users",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Users_DispatcherCompanyId",
                table: "Users",
                newName: "IX_Users_CompanyId");

            migrationBuilder.AddColumn<Guid>(
                name: "DispatcherCompanyId",
                table: "Trucks",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DispatcherId",
                table: "Trucks",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "BrokerCompanyId",
                table: "Loads",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "BrokerId",
                table: "Loads",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_DispatcherCompanyId",
                table: "Trucks",
                column: "DispatcherCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_DispatcherId",
                table: "Trucks",
                column: "DispatcherId");

            migrationBuilder.CreateIndex(
                name: "IX_Loads_BrokerCompanyId",
                table: "Loads",
                column: "BrokerCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Loads_BrokerId",
                table: "Loads",
                column: "BrokerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_Users_BrokerCompanyId",
                table: "Loads",
                column: "BrokerCompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_Users_BrokerId",
                table: "Loads",
                column: "BrokerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Trucks_Users_DispatcherCompanyId",
                table: "Trucks",
                column: "DispatcherCompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Trucks_Users_DispatcherId",
                table: "Trucks",
                column: "DispatcherId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_CompanyId",
                table: "Users",
                column: "CompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_Users_BrokerCompanyId",
                table: "Loads");

            migrationBuilder.DropForeignKey(
                name: "FK_Loads_Users_BrokerId",
                table: "Loads");

            migrationBuilder.DropForeignKey(
                name: "FK_Trucks_Users_DispatcherCompanyId",
                table: "Trucks");

            migrationBuilder.DropForeignKey(
                name: "FK_Trucks_Users_DispatcherId",
                table: "Trucks");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Users_CompanyId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_RoleId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Trucks_DispatcherCompanyId",
                table: "Trucks");

            migrationBuilder.DropIndex(
                name: "IX_Trucks_DispatcherId",
                table: "Trucks");

            migrationBuilder.DropIndex(
                name: "IX_Loads_BrokerCompanyId",
                table: "Loads");

            migrationBuilder.DropIndex(
                name: "IX_Loads_BrokerId",
                table: "Loads");

            migrationBuilder.DropColumn(
                name: "DispatcherCompanyId",
                table: "Trucks");

            migrationBuilder.DropColumn(
                name: "DispatcherId",
                table: "Trucks");

            migrationBuilder.DropColumn(
                name: "BrokerCompanyId",
                table: "Loads");

            migrationBuilder.DropColumn(
                name: "BrokerId",
                table: "Loads");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "Users",
                newName: "DispatcherCompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Users_CompanyId",
                table: "Users",
                newName: "IX_Users_DispatcherCompanyId");

            migrationBuilder.AddColumn<Guid>(
                name: "BrokerCompanyId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_BrokerCompanyId",
                table: "Users",
                column: "BrokerCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_BrokerCompanyId",
                table: "Users",
                column: "BrokerCompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Users_DispatcherCompanyId",
                table: "Users",
                column: "DispatcherCompanyId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
