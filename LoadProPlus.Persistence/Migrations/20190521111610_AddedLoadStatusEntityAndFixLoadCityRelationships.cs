﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class AddedLoadStatusEntityAndFixLoadCityRelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "LoadStatusId",
                table: "Loads",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "LoadsStatuses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 25, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoadsStatuses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Loads_LoadStatusId",
                table: "Loads",
                column: "LoadStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_LoadsStatuses_LoadStatusId",
                table: "Loads",
                column: "LoadStatusId",
                principalTable: "LoadsStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_LoadsStatuses_LoadStatusId",
                table: "Loads");

            migrationBuilder.DropTable(
                name: "LoadsStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Loads_LoadStatusId",
                table: "Loads");

            migrationBuilder.DropColumn(
                name: "LoadStatusId",
                table: "Loads");
        }
    }
}
