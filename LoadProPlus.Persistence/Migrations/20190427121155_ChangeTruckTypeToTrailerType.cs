﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class ChangeTruckTypeToTrailerType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trucks_TruckTypes_TruckTypeId",
                table: "Trucks");

            migrationBuilder.DropTable(
                name: "TruckTypes");

            migrationBuilder.RenameColumn(
                name: "TruckTypeId",
                table: "Trucks",
                newName: "TrailerTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Trucks_TruckTypeId",
                table: "Trucks",
                newName: "IX_Trucks_TrailerTypeId");

            migrationBuilder.CreateTable(
                name: "TrailerTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 10, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrailerTypes", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Trucks_TrailerTypes_TrailerTypeId",
                table: "Trucks",
                column: "TrailerTypeId",
                principalTable: "TrailerTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trucks_TrailerTypes_TrailerTypeId",
                table: "Trucks");

            migrationBuilder.DropTable(
                name: "TrailerTypes");

            migrationBuilder.RenameColumn(
                name: "TrailerTypeId",
                table: "Trucks",
                newName: "TruckTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Trucks_TrailerTypeId",
                table: "Trucks",
                newName: "IX_Trucks_TruckTypeId");

            migrationBuilder.CreateTable(
                name: "TruckTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    Name = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TruckTypes", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Trucks_TruckTypes_TruckTypeId",
                table: "Trucks",
                column: "TruckTypeId",
                principalTable: "TruckTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
