﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class ConfigLoadAndCity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Loads",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PickUpId = table.Column<Guid>(nullable: false),
                    PickUpDateTime = table.Column<DateTime>(nullable: false),
                    DelieveryId = table.Column<Guid>(nullable: false),
                    DelieveryDateTime = table.Column<DateTime>(nullable: false),
                    TrailerTypeId = table.Column<Guid>(nullable: false),
                    Comodity = table.Column<string>(maxLength: 50, nullable: false),
                    Weight = table.Column<decimal>(nullable: false),
                    CustomerRate = table.Column<decimal>(nullable: false),
                    Comments = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Loads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Loads_Cities_DelieveryId",
                        column: x => x.DelieveryId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Loads_Cities_PickUpId",
                        column: x => x.PickUpId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Loads_TrailerTypes_TrailerTypeId",
                        column: x => x.TrailerTypeId,
                        principalTable: "TrailerTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Loads_DelieveryId",
                table: "Loads",
                column: "DelieveryId");

            migrationBuilder.CreateIndex(
                name: "IX_Loads_PickUpId",
                table: "Loads",
                column: "PickUpId");

            migrationBuilder.CreateIndex(
                name: "IX_Loads_TrailerTypeId",
                table: "Loads",
                column: "TrailerTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Loads");
        }
    }
}
