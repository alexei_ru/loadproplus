﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LoadProPlus.Persistence.Migrations
{
    public partial class FixLoadStatusesTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_LoadsStatuses_LoadStatusId",
                table: "Loads");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LoadsStatuses",
                table: "LoadsStatuses");

            migrationBuilder.RenameTable(
                name: "LoadsStatuses",
                newName: "LoadStatuses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LoadStatuses",
                table: "LoadStatuses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_LoadStatuses_LoadStatusId",
                table: "Loads",
                column: "LoadStatusId",
                principalTable: "LoadStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Loads_LoadStatuses_LoadStatusId",
                table: "Loads");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LoadStatuses",
                table: "LoadStatuses");

            migrationBuilder.RenameTable(
                name: "LoadStatuses",
                newName: "LoadsStatuses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LoadsStatuses",
                table: "LoadsStatuses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Loads_LoadsStatuses_LoadStatusId",
                table: "Loads",
                column: "LoadStatusId",
                principalTable: "LoadsStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
