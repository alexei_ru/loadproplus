﻿using LoadProPlus.Domain.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Infrastructure
{
    public abstract class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            // TO DO: Here we will setup BaseEntity properties (ex: CreatedAt, UpdatedBy...)
            // So we avoid repeating the code
        }
    }
}
