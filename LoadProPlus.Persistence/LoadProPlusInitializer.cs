﻿using LoadProPlus.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace LoadProPlus.Persistence
{
    public class LoadProPlusInitializer
    {
        public static void Initialize(LoadProPlusDbContext dbContext)
        {
            var initializer = new LoadProPlusInitializer();
            initializer.SeedDatabase(dbContext);
        }

        private void SeedDatabase(LoadProPlusDbContext dbContext)
        {
            SeedRoles(dbContext);
            SeedTrailerTypes(dbContext);
            SeedLoadStatuses(dbContext);
        }

        private void SeedLoadStatuses(LoadProPlusDbContext dbContext)
        {
            if (!dbContext.LoadStatuses.Any())
            {
                var loadStatuses = new List<LoadStatus>
                {
                    new LoadStatus
                    {
                        Name = "Accepted"
                    },
                    new LoadStatus
                    {
                        Name = "Booked"
                    },
                    new LoadStatus
                    {
                        Name = "PickedUp"
                    },
                    new LoadStatus
                    {
                        Name = "Delivered"
                    },
                    new LoadStatus
                    {
                        Name = "Closed"
                    }
                };
                dbContext.AddRange(loadStatuses);
                dbContext.SaveChanges();
            }
        }

        private void SeedRoles(LoadProPlusDbContext dbContext)
        {
            if (!dbContext.Roles.Any())
            {
                var roles = new List<Role>
                {
                    new Role
                    {
                        Name = "Administrator"
                    },
                    new Role
                    {
                        Name = "BrokerCompany"
                    },
                    new Role
                    {
                        Name = "DispatcherCompany"
                    },
                    new Role
                    {
                        Name = "Broker"
                    },
                    new Role
                    {
                        Name = "Dispatcher"
                    }
                };

                dbContext.AddRange(roles);
                dbContext.SaveChanges();
            }
        }
        private void SeedTrailerTypes(LoadProPlusDbContext dbContext)
        {
            if (!dbContext.TrailerTypes.Any())
            {
                var truckTypes = new List<TrailerType>
                {
                    new TrailerType
                    {
                        Name = "2F",
                        Description = "Two 24 or 28 Foot Flatbeds"
                    },
                    new TrailerType
                    {
                        Name = "ANIM",
                        Description = "Animal Carrier"
                    },
                    new TrailerType
                    {
                        Name = "ANY",
                        Description = "Any Equipment"
                    },
                    new TrailerType
                    {
                        Name = "AUTO",
                        Description = "Auto Carrier"
                    },
                    new TrailerType
                    {
                        Name = "B-TR",
                        Description = "B-Train/Supertrain (Canada only)"
                    },
                    new TrailerType
                    {
                        Name = "BDMP",
                        Description = "Belly Dump"
                    },
                    new TrailerType
                    {
                        Name = "BEAM",
                        Description = "Beam"
                    },
                    new TrailerType
                    {
                        Name = "BELT",
                        Description = "Conveyor Belt"
                    },
                    new TrailerType
                    {
                        Name = "BOAT",
                        Description = "Boat Hauling Trailer"
                    },
                    new TrailerType
                    {
                        Name = "CH",
                        Description = "Convertible Hopper"
                    },
                    new TrailerType
                    {
                        Name = "CONG",
                        Description = "Conestoga"
                    },
                    new TrailerType
                    {
                        Name = "CONT",
                        Description = "Container Trailer"
                    },
                    new TrailerType
                    {
                        Name = "CV",
                        Description = "Curtain Van"
                    },
                    new TrailerType
                    {
                        Name = "DA",
                        Description = "Drive Away"
                    },
                    new TrailerType
                    {
                        Name = "DD",
                        Description = "Double Drop"
                    },
                    new TrailerType
                    {
                        Name = "DDE",
                        Description = "Double Drop Extendable"
                    },
                    new TrailerType
                    {
                        Name = "DUMP",
                        Description = "Dump Trucks"
                    },
                    new TrailerType
                    {
                        Name = "ENDP",
                        Description = "End Dump"
                    },
                    new TrailerType
                    {
                        Name = "F",
                        Description = "Flatbed"
                    },
                    new TrailerType
                    {
                        Name = "FA",
                        Description = "FlatBed - Air-Ride"
                    },
                    new TrailerType
                    {
                        Name = "FEXT",
                        Description = "Stretch Trailers or Extendable Flatbed"
                    },
                    new TrailerType
                    {
                        Name = "FINT",
                        Description = "Flatbed Intermodal"
                    },
                    new TrailerType
                    {
                        Name = "FO",
                        Description = "Flatbed Over-Dimension Loads"
                    },
                    new TrailerType
                    {
                        Name = "FRV",
                        Description = "Flatbed, Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "FSD",
                        Description = "Flatbed or Step Deck"
                    },
                    new TrailerType
                    {
                        Name = "FSDV",
                        Description = "Flatbed, Step Deck or Van"
                    },
                    new TrailerType
                    {
                        Name = "FV",
                        Description = "Van or Flatbed"
                    },
                    new TrailerType
                    {
                        Name = "FVR",
                        Description = "Flatbed, Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "FVV",
                        Description = "Flatbed or Vented Van"
                    },
                    new TrailerType
                    {
                        Name = "FVVR",
                        Description = "Flatbed, Vented Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "FWS",
                        Description = "Flatbed With Sides"
                    },
                    new TrailerType
                    {
                        Name = "HOPP",
                        Description = "Hopper Bottom"
                    },
                    new TrailerType
                    {
                        Name = "HS",
                        Description = "Hot Shot"
                    },
                    new TrailerType
                    {
                        Name = "HTU",
                        Description = "Haul and Tow Unit"
                    },
                    new TrailerType
                    {
                        Name = "LAF",
                        Description = "Landoll Flatbed"
                    },
                    new TrailerType
                    {
                        Name = "LB",
                        Description = "Lowboy"
                    },
                    new TrailerType
                    {
                        Name = "LBO",
                        Description = "Lowboy Over-Dimension Loads"
                    },
                    new TrailerType
                    {
                        Name = "LDOT",
                        Description = "Load-Out are empty trailers you load and haul"
                    },
                    new TrailerType
                    {
                        Name = "LIVE",
                        Description = "Live Bottom Trailer"
                    },
                    new TrailerType
                    {
                        Name = "MAXI",
                        Description = "Maxi or Double Flat Trailers"
                    },
                    new TrailerType
                    {
                        Name = "MBHM",
                        Description = "Mobile Home"
                    },
                    new TrailerType
                    {
                        Name = "PNEU",
                        Description = "Pneumatic"
                    },
                    new TrailerType
                    {
                        Name = "PO",
                        Description = "Power Only (Tow-Away)"
                    },
                    new TrailerType
                    {
                        Name = "R",
                        Description = "Refrigerated (Reefer)"
                    },
                    new TrailerType
                    {
                        Name = "RFV",
                        Description = "Flatbed, Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "RGN",
                        Description = "Removable Goose Neck & Multi-Axle Heavy Haulers"
                    },
                    new TrailerType
                    {
                        Name = "RGNE",
                        Description = "RGN Extendable"
                    },
                    new TrailerType
                    {
                        Name = "RINT",
                        Description = "Refrigerated Intermodal"
                    },
                    new TrailerType
                    {
                        Name = "ROLL",
                        Description = "Roll Top Conestoga"
                    },
                    new TrailerType
                    {
                        Name = "RPD",
                        Description = "Refrigerated Carrier with Plant Decking"
                    },
                    new TrailerType
                    {
                        Name = "RV",
                        Description = "Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "RVF",
                        Description = "Flatbed, Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "SD",
                        Description = "Step Deck"
                    },
                    new TrailerType
                    {
                        Name = "SDC",
                        Description = "Step Deck Conestoga"
                    },
                    new TrailerType
                    {
                        Name = "SDE",
                        Description = "Step Deck Extendable"
                    },
                    new TrailerType
                    {
                        Name = "SDL",
                        Description = "Step Deck with Loading Ramps"
                    },
                    new TrailerType
                    {
                        Name = "SDO",
                        Description = "Step Deck Over-Dimension Loads"
                    },
                    new TrailerType
                    {
                        Name = "SDRG",
                        Description = "Step Deck or Removable Gooseneck"
                    },
                    new TrailerType
                    {
                        Name = "SPEC",
                        Description = "Unspecified Specialized Trailers"
                    },
                    new TrailerType
                    {
                        Name = "SPV",
                        Description = "Cargo/Small/Sprinter Van"
                    },
                    new TrailerType
                    {
                        Name = "SV",
                        Description = "Straight Van"
                    },
                    new TrailerType
                    {
                        Name = "TANK",
                        Description = "Tanker (Food grade, liquid, bulk, etc.)"
                    },
                    new TrailerType
                    {
                        Name = "V",
                        Description = "Van"
                    },
                    new TrailerType
                    {
                        Name = "V-OT",
                        Description = "Open Top Van"
                    },
                    new TrailerType
                    {
                        Name = "VA",
                        Description = "Van - Air-Ride"
                    },
                    new TrailerType
                    {
                        Name = "VB",
                        Description = "Blanket Wrap Van"
                    },
                    new TrailerType
                    {
                        Name = "VCAR",
                        Description = "Cargo Vans (1 Ton capacity)"
                    },
                    new TrailerType
                    {
                        Name = "VF",
                        Description = "Flatbed or Van"
                    },
                    new TrailerType
                    {
                        Name = "VFR",
                        Description = "Flatbed, Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "VINT",
                        Description = "Van Intermodal"
                    },
                    new TrailerType
                    {
                        Name = "VIV",
                        Description = "Vented Insulated Van"
                    },
                    new TrailerType
                    {
                         Name = "VIVR",
                         Description = "Vented Insulated Van or Refrigerated"
                    },
                    new TrailerType
                    {
                        Name = "VLG",
                        Description = "Van with Liftgate"
                    },
                    new TrailerType
                    {
                        Name = "VM",
                        Description = "Moving Van"
                    },
                    new TrailerType
                    {
                        Name = "VR",
                        Description = "Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "VRDD",
                        Description = "Van, Reefer or Double Drop"
                    },
                    new TrailerType
                    {
                        Name = "VRF",
                        Description = "Flatbed, Van or Reefer"
                    },
                    new TrailerType
                    {
                        Name = "VV",
                        Description = "Vented Van"
                    },
                    new TrailerType
                    {
                        Name = "VVR",
                        Description = "Vented Van or Refrigerated"
                    },
                    new TrailerType
                    {
                        Name = "WALK",
                        Description = "Walking Floor"
                    }
                };

                dbContext.AddRange(truckTypes);
                dbContext.SaveChanges();
            }
        }
    }
}
