﻿using LoadProPlus.Application.Interfaces;
using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;

namespace LoadProPlus.Persistence
{
    public class LoadProPlusDbContext : DbContext, ILoadProPlusDbContext
	{
		public DbSet<State> States { get; set; }
		public DbSet<City> Cities { get; set; }
		public DbSet<Truck> Trucks { get; set; }
        public DbSet<Role> Roles { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<BrokerCompany> BrokerCompanies { get; set; }
		public DbSet<Broker> Brokers { get; set; }
        public DbSet<DispatcherCompany> DispatcherCompanies { get; set; }
		public DbSet<Dispatcher> Dispatchers { get; set; }
        public DbSet<TrailerType> TrailerTypes { get; set; }
        public DbSet<Driver> Drivers { get; set; }
		public DbSet<UserRefreshToken> UserRefreshTokens { get; set; }
        public DbSet<Load> Loads { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }
		public DbSet<LoadStatus> LoadStatuses { get; set; }

		public LoadProPlusDbContext(DbContextOptions<LoadProPlusDbContext> options) : base(options)
		{
			
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
				.Where(type => !string.IsNullOrEmpty(type.Namespace))
				.Where(type => type.BaseType != null && type.BaseType.IsGenericType
													 && type.BaseType.GetGenericTypeDefinition() == typeof(BaseEntityConfiguration<>));

			foreach (var type in typesToRegister)
			{
				dynamic configurationInstance = Activator.CreateInstance(type);

				modelBuilder.ApplyConfiguration(configurationInstance);
			}
		}
	}
}
