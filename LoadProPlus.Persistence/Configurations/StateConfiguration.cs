﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class StateConfiguration : BaseEntityConfiguration<State>
    {
        public override void Configure(EntityTypeBuilder<State> builder)
        {
            builder.Property(state => state.Name)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(state => state.Latitude)
                .IsRequired();

            builder.Property(state => state.Longitude)
                .IsRequired();

            builder.Property(state => state.Code)
                .IsRequired()
                .HasMaxLength(2);

            base.Configure(builder);
        }
    }
}
