﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    class LoadStatusConfiguration : BaseEntityConfiguration<LoadStatus>
    {
        public override void Configure(EntityTypeBuilder<LoadStatus> builder)
        {
            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(25);

			builder.Property(p => p.Description)
				.HasMaxLength(100);

            base.Configure(builder);
        }
    }
}
