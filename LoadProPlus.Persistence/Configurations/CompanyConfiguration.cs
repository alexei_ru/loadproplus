﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class CompanyConfiguration : BaseEntityConfiguration<Company>
	{
		public override void Configure(EntityTypeBuilder<Company> builder)
		{
			builder.Property(p => p.CompanyName)
				.IsRequired()
				.HasMaxLength(100);

			builder.Property(p => p.DotNumber)
				.IsRequired()
				.HasMaxLength(70);

			builder.Property(p => p.Fax)
				.IsRequired()
				.HasMaxLength(30);

			builder.Property(p => p.MainAddress)
				.IsRequired()
				.HasMaxLength(200);

			builder.Property(p => p.McNumber)
				.IsRequired()
				.HasMaxLength(70);

			builder.Property(p => p.PrimaryContactName)
				.IsRequired()
				.HasMaxLength(100);

			builder.Property(p => p.ZipCode)
				.IsRequired()
				.HasMaxLength(15);

            base.Configure(builder);
		}
	}
}
