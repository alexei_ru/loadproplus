﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class EmployeeConfiguration : BaseEntityConfiguration<Employee>
	{
		public override void Configure(EntityTypeBuilder<Employee> builder)
		{
			builder.Property(p => p.Phone)
				.IsRequired()
				.HasMaxLength(30);

			builder.Property(p => p.Name)
				.IsRequired()
				.HasMaxLength(50);

			builder.Property(p => p.Surname)
				.IsRequired()
				.HasMaxLength(50);

            builder
                .HasOne(p => p.Company)
                .WithMany(p => p.Employees)
                .OnDelete(DeleteBehavior.Restrict);

            base.Configure(builder);
		}
	}
}
