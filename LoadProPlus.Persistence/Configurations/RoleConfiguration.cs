﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    class RoleConfiguration : BaseEntityConfiguration<Role>
    {
        public override void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(role => role.Name)
                .IsRequired()
                .HasMaxLength(25);

            base.Configure(builder);
        }
    }
}
