﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    class DriverConfiguration : BaseEntityConfiguration<Driver>
    {
        public override void Configure(EntityTypeBuilder<Driver> builder)
        {
            builder.Property(driver => driver.Email)
                 .HasMaxLength(254)
                 .IsRequired();

            builder.Property(driver => driver.FirstName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(driver => driver.Surname)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(driver => driver.PhoneNumber)
                .HasMaxLength(30)
                .IsRequired();

            base.Configure(builder);
        }
    }
}
