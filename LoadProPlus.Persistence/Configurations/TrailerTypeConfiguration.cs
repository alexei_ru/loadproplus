﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class TrailerTypeConfiguration : BaseEntityConfiguration<TrailerType>
    {
        public override void Configure(EntityTypeBuilder<TrailerType> builder)
        {
            builder.Property(truckType => truckType.Name)
                .HasMaxLength(10)
                .IsRequired();

            builder.Property(truckType => truckType.Description)
                .HasMaxLength(100)
                .IsRequired();

            base.Configure(builder);
        }
    }
}
