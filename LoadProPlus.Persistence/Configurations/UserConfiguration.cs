﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class UserConfiguration : BaseEntityConfiguration<User>
	{
		public override void Configure(EntityTypeBuilder<User> builder)
		{
			builder.Property(p => p.Email)
				.IsRequired()
				.HasMaxLength(254);

			builder.Property(p => p.PasswordHash)
				.IsRequired();

			builder.Property(p => p.PasswordSalt)
				.IsRequired();

			builder.Property(p => p.Username)
				.IsRequired()
				.HasMaxLength(50);

			builder
				.HasOne(ho => ho.UserRefreshToken)
				.WithOne(wo => wo.User)
				.HasForeignKey<UserRefreshToken>(fk => fk.UserId)
				.OnDelete(DeleteBehavior.ClientSetNull);

			base.Configure(builder);
		}
	}
}
