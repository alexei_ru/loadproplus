﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class LoadConfiguration : BaseEntityConfiguration<Load>
    {
        public override void Configure(EntityTypeBuilder<Load> builder)
        {
            builder.Property(load => load.PickUpId)
                .IsRequired();

            builder.Property(load => load.PickUpDateTime)
                .IsRequired();

            builder.Property(load => load.DeliveryId)
                .IsRequired();

            builder.Property(load => load.DeliveryDateTime)
                .IsRequired();

            builder.Property(load => load.TrailerTypeId)
                .IsRequired();

            builder.Property(load => load.Commodity)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(load => load.Weight)
                .IsRequired();

            builder.Property(load => load.CustomerRate)
                .IsRequired();

            builder.Property(load => load.AdditionalInfo)
                .HasMaxLength(255)
                .IsRequired();

            builder
                .HasOne(x => x.Delivery)
                .WithMany(x => x.DeliveryLoads)
				.OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(x => x.PickUp)
                .WithMany(x => x.PickUpLoads)
				.OnDelete(DeleteBehavior.Restrict);

			builder
				.HasOne(ho => ho.LoadStatus)
				.WithMany(wm => wm.Loads)
				.HasForeignKey(fk => fk.LoadStatusId);

            base.Configure(builder);
        }
    }
}
