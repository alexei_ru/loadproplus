﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
    public class TruckConfiguration : BaseEntityConfiguration<Truck>
    {
        public override void Configure(EntityTypeBuilder<Truck> builder)
        {
            builder.Property(truck => truck.TruckNumber)
                .HasMaxLength(30)
                .IsRequired();

            builder.Property(truck => truck.TrailerNumber)
                .HasMaxLength(30)
                .IsRequired();

            builder.Property(truck => truck.TrailerTypeId)
                .IsRequired();

            base.Configure(builder);
        }
    }
}
