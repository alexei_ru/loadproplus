﻿using LoadProPlus.Domain.Entities;
using LoadProPlus.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoadProPlus.Persistence.Configurations
{
	public class CityConfiguration : BaseEntityConfiguration<City>
	{
		public override void Configure(EntityTypeBuilder<City> builder)
		{
			builder.Property(city => city.Name)
				.IsRequired()
				.HasMaxLength(30);

			builder.Property(city => city.Longitude)
				.IsRequired();

			builder.Property(city => city.Latitude)
				.IsRequired();

			builder.Property(city => city.County)
				.IsRequired()
				.HasMaxLength(25);

			base.Configure(builder);
		}
	}
}
