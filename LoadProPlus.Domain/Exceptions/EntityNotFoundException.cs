﻿using System;

namespace LoadProPlus.Domain.Exceptions
{
	public class EntityNotFoundException : Exception
	{
		public EntityNotFoundException() : base("Entity not found") { }
		public EntityNotFoundException(Type entityType) : base($"Entity of type {entityType.Name} not found") { }
	}
}
