﻿namespace LoadProPlus.Domain.Exceptions
{
    public class IncorrectUsernameException : BadRequestException
	{
		public IncorrectUsernameException() : base("Incorrect username") { }
	}
}
