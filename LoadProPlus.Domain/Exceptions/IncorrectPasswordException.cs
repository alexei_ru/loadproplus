﻿namespace LoadProPlus.Domain.Exceptions
{
	public class IncorrectPasswordException : BadRequestException
	{
		public IncorrectPasswordException() : base("Incorrect password") { }
	}
}
