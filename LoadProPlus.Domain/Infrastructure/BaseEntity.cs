﻿using System;

namespace LoadProPlus.Domain.Infrastructure
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
