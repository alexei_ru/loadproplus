﻿using LoadProPlus.Domain.Infrastructure;
using System;

namespace LoadProPlus.Domain.Entities
{
    public class UserRefreshToken : BaseEntity
	{
		public string RefreshToken { get; set; }
		public DateTime ExpirationTime { get; set; }
		public DateTime CreationTime { get; set; }

		public Guid UserId { get; set; }
		public User User { get; set; }

		public UserRefreshToken()
		{
			CreationTime = DateTime.UtcNow;
		}
	}
}
