﻿using LoadProPlus.Domain.Infrastructure;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class LoadStatus : BaseEntity
	{
		public string Name { get; set; }
		public string Description { get; set; }

		public IEnumerable<Load> Loads { get; set; }
	}
}
