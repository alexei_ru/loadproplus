﻿using LoadProPlus.Domain.Infrastructure;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class Role : BaseEntity
	{
		public string Name { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
