﻿using LoadProPlus.Domain.Infrastructure;
using System;

namespace LoadProPlus.Domain.Entities
{
	public class Load : BaseEntity
	{
		public decimal Weight { get; set; }
		public decimal CustomerRate { get; set; }
		public string Commodity { get; set; }
		public string AdditionalInfo { get; set; }
		public DateTime PickUpDateTime { get; set; }
		public DateTime DeliveryDateTime { get; set; }

		public City PickUp { get; set; }
		public Guid PickUpId { get; set; }

		public City Delivery { get; set; }
		public Guid DeliveryId { get; set; }

		public TrailerType TrailerType { get; set; }
		public Guid TrailerTypeId { get; set; }

		public Broker Broker { get; set; }
		public Guid BrokerId { get; set; }

		public Guid LoadStatusId { get; set; }
		public LoadStatus LoadStatus { get; set; }
	}
}
