﻿using System;

namespace LoadProPlus.Domain.Entities
{
    public class Employee : User
	{
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Phone { get; set; }

        public Company Company { get; set; }
        public Guid CompanyId { get; set; }
	}
}
