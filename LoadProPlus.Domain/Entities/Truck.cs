﻿using LoadProPlus.Domain.Infrastructure;
using System;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
	public class Truck : BaseEntity
	{
		public string TruckNumber { get; set; }
		public string TrailerNumber { get; set; }

		public TrailerType TrailerType { get; set; }
		public Guid TrailerTypeId { get; set; }

        public Dispatcher Dispatcher { get; set; }
        public Guid DispatcherId { get; set; }

		public ICollection<Driver> Drivers { get; set; }

        public Truck()
        {
            Drivers = new HashSet<Driver>();
        }
	}
}
