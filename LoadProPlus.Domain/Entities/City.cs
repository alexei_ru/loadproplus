﻿using LoadProPlus.Domain.Infrastructure;
using System;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class City : BaseEntity
    {
        public string Name { get; set; }
        public string County { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public State State { get; set; }
        public Guid StateId { get; set; }

        public IEnumerable<Load> PickUpLoads { get; set; }
        public IEnumerable<Load> DeliveryLoads { get; set; }
    }
}
