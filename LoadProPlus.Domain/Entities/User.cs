﻿using LoadProPlus.Domain.Infrastructure;
using System;

namespace LoadProPlus.Domain.Entities
{
	public class User : BaseEntity
	{
		public string Username { get; set; }
		public byte[] PasswordHash { get; set; }
		public byte[] PasswordSalt { get; set; }
		public string Email { get; set; }

		public Guid RoleId { get; set; }
		public Role Role { get; set; }

		public UserRefreshToken UserRefreshToken { get; set; }
	}
}
