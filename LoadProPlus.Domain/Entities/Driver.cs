﻿using LoadProPlus.Domain.Infrastructure;
using System;

namespace LoadProPlus.Domain.Entities
{
    public class Driver : BaseEntity
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public Truck Truck { get; set; }
        public Guid? TruckId { get; set; }
    }
}
