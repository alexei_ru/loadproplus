﻿using System;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class Dispatcher : Employee
	{
        public IEnumerable<Truck> Trucks { get; set; }
    }
}
