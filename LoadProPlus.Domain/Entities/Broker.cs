﻿using System;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class Broker : Employee
	{
        public IEnumerable<Load> Loads { get; set; }
    }
}
