﻿using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class Company : User
	{
		public string McNumber { get; set; }
		public string CompanyName { get; set; }
		public string MainAddress { get; set; }
		public string ZipCode { get; set; }
		public string PrimaryContactName { get; set; }
		public string Fax { get; set; }
		public string DotNumber { get; set; }

        public IEnumerable<Employee> Employees { get; set; }
	}
}
