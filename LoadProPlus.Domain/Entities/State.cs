﻿using LoadProPlus.Domain.Infrastructure;
using System.Collections.Generic;

namespace LoadProPlus.Domain.Entities
{
    public class State : BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}
