﻿namespace LoadProPlus.Domain.Enums
{
	public enum LoadStatusesEnum
	{
		Accepted,
		Booked,
		PickedUp,
		Delivered,
		Closed
	}
}
